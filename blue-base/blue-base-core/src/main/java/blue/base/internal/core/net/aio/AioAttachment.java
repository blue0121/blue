package blue.base.internal.core.net.aio;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * @author Jin Zheng
 * @since 2021-06-28
 */
public class AioAttachment {
	private static final int BUF_CAPACITY = 4096;

    private AsynchronousServerSocketChannel server;
    private AsynchronousSocketChannel client;
    private boolean readMode;
    private ByteBuffer buffer;

	public AioAttachment() {
	}

	public ByteBuffer getBuffer() {
		if (buffer == null) {
			synchronized (this) {
				if (buffer == null) {
					buffer = ByteBuffer.allocate(BUF_CAPACITY);
				}
			}
		}
		return buffer;
	}

	public AsynchronousServerSocketChannel getServer() {
		return server;
	}

	public void setServer(AsynchronousServerSocketChannel server) {
		this.server = server;
	}

	public AsynchronousSocketChannel getClient() {
		return client;
	}

	public void setClient(AsynchronousSocketChannel client) {
		this.client = client;
	}

	public boolean isReadMode() {
		return readMode;
	}

	public void setReadMode(boolean readMode) {
		this.readMode = readMode;
	}
}
