package blue.base.internal.core.net.aio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

/**
 * @author Jin Zheng
 * @since 2021-06-28
 */
public class AioAcceptHandler implements CompletionHandler<AsynchronousSocketChannel, Void> {
    private static Logger logger = LoggerFactory.getLogger(AioAcceptHandler.class);

    private final AsynchronousServerSocketChannel server;

    public AioAcceptHandler(AsynchronousServerSocketChannel server) {
        this.server = server;
    }

    @Override
    public void completed(AsynchronousSocketChannel client, Void att) {

    }

    @Override
    public void failed(Throwable exc, Void attachment) {
        logger.error("Error, ", exc);
    }
}
