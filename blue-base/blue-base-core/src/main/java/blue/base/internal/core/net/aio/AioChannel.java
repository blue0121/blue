package blue.base.internal.core.net.aio;

import blue.base.core.id.IdGenerator;
import blue.base.core.net.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.AsynchronousSocketChannel;

/**
 * @author Jin Zheng
 * @since 2021-06-30
 */
public class AioChannel implements Channel {
    private static Logger logger = LoggerFactory.getLogger(AioChannel.class);

    private final long id;
    private final AsynchronousSocketChannel socketChannel;

	public AioChannel(AsynchronousSocketChannel socketChannel) {
		this.socketChannel = socketChannel;
		this.id = IdGenerator.id();
	}

	@Override
    public long getId() {
        return id;
    }
}
