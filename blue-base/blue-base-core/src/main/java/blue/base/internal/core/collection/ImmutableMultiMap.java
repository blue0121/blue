package blue.base.internal.core.collection;

import blue.base.core.collection.MultiMap;
import blue.base.core.util.AssertUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @author Jin Zheng
 * @since 1.0 2021-02-08
 */
public class ImmutableMultiMap<K, V> extends AbstractMultiMap<K, V> {
	private static Logger logger = LoggerFactory.getLogger(ImmutableMultiMap.class);

	public ImmutableMultiMap(MultiMap<K, V> map) {
		AssertUtil.notNull(map, "MultiMap");
		this.map = switch (map.getMapType().getSimpleName()) {
			case "LinkedHashMap" -> new LinkedHashMap<>();
			default -> new HashMap<>();
		};

		for (var entry : map.entrySet()) {
			this.map.put(entry.getKey(), entry.getValue());
		}
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("clear");
	}

	@Override
	public V put(K key, V value) {
		throw new UnsupportedOperationException("put");
	}

	@Override
	public boolean remove(K key) {
		throw new UnsupportedOperationException("remove");
	}

	@Override
	public boolean remove(K key, V value) {
		throw new UnsupportedOperationException("remove");
	}

	@Override
	public Class<?> getMapType() {
		return map.getClass();
	}


}
