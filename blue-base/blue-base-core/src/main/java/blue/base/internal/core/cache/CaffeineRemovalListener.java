package blue.base.internal.core.cache;

import blue.base.core.cache.RemovalCause;
import blue.base.core.cache.RemovalListener;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-27
 */
public class CaffeineRemovalListener<K, V> implements com.github.benmanes.caffeine.cache.RemovalListener<K, V> {
	private final RemovalListener<K, V> listener;

	public CaffeineRemovalListener(RemovalListener<K, V> listener) {
		this.listener = listener;
	}


	@Override
	public void onRemoval(K key, V value, com.github.benmanes.caffeine.cache.RemovalCause cause) {
		listener.onRemoval(key, value, RemovalCause.from(cause));
	}
}
