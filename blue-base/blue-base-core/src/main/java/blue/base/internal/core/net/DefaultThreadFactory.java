package blue.base.internal.core.net;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Jin Zheng
 * @since 2021-06-28
 */
public class DefaultThreadFactory implements ThreadFactory {
    private static final String PREFIX = "net-thread-";

    private AtomicInteger counter = new AtomicInteger();

	public DefaultThreadFactory() {
	}

    @Override
    public Thread newThread(Runnable r) {
	    int idx = counter.incrementAndGet();
        return new Thread(r, PREFIX + idx);
    }
}
