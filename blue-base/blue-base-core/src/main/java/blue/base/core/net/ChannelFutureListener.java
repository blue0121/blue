package blue.base.core.net;

import java.util.EventListener;

/**
 * @author Jin Zheng
 * @since 2021-06-30
 */
public interface ChannelFutureListener extends EventListener {

	void onSuccess(Channel channel);

	void onFailure(Channel channel, Exception e);

}
