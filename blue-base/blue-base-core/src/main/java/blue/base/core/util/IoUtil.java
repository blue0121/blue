package blue.base.core.util;

import java.io.IOException;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * @author Jin Zheng
 * @since 2021-06-29
 */
public class IoUtil {
	private IoUtil() {
	}

	public static void close(AsynchronousSocketChannel channel) {
		try {
			channel.shutdownInput();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			channel.shutdownOutput();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			channel.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
