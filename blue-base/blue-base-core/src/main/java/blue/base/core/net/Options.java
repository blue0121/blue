package blue.base.core.net;

import blue.base.internal.core.net.DefaultThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.SocketOption;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadFactory;

/**
 * @author Jin Zheng
 * @since 2021-06-28
 */
public class Options {
    private static Logger logger = LoggerFactory.getLogger(Options.class);

    protected Map<SocketOption, Object> optionMap = new HashMap<>();
    protected ThreadFactory threadFactory;
    protected int thread = 2;

	public Options() {
	}

	public void check() {
        if (this.threadFactory == null) {
            this.threadFactory = new DefaultThreadFactory();
            logger.info("threadFactory is null, use DefaultThreadFactory");
        }
        if (this.thread < 1) {
            this.thread = 1;
            logger.warn("min thread number is 1");
        }
    }

    public <T> void addSocketOption(SocketOption<T> option, T value) {
	    this.optionMap.put(option, value);
    }

    public Map<SocketOption, Object> getSocketOptionMap() {
        return optionMap;
    }

    public ThreadFactory getThreadFactory() {
        return threadFactory;
    }

    public void setThreadFactory(ThreadFactory threadFactory) {
        this.threadFactory = threadFactory;
    }

    public int getThread() {
        return thread;
    }

    public void setThread(int thread) {
        this.thread = thread;
    }
}
