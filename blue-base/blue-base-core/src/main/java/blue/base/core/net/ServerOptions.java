package blue.base.core.net;

/**
 * @author Jin Zheng
 * @since 2021-06-28
 */
public class ServerOptions extends Options{
    private int port = 8000;

	public ServerOptions() {
	}

	@Override
	public void check() {
	    super.check();

    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
