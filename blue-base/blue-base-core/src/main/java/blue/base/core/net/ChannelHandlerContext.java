package blue.base.core.net;

/**
 * @author Jin Zheng
 * @since 2021-06-30
 */
public interface ChannelHandlerContext {

	Channel getChannel();

	Pipeline getPipeline();

}
