package blue.base.core.cache;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-27
 */
public interface RemovalListener<K, V> {

	void onRemoval(K key, V value, RemovalCause cause);
}
