package blue.base.core.net;

/**
 * @author Jin Zheng
 * @since 2021-06-28
 */
public interface Server {

	/**
	 * 启动
	 */
	void start();

	/**
	 * 等待
	 */
	void await();

	/**
	 * 关闭
	 */
	void close();

}
