package blue.base.core.net;

import java.util.concurrent.Future;

/**
 * @author Jin Zheng
 * @since 2021-06-30
 */
public interface ChannelFuture extends Future<Void> {

	Channel getChannel();

	ChannelFuture addListener(ChannelFutureListener listener);

	ChannelFuture removeListener(ChannelFutureListener listener);

}
