package blue.base.core.path;

import blue.base.internal.core.path.DefaultFilePath;

import java.util.Collection;

/**
 * @author Jin Zheng
 * @since 1.0 2021-02-19
 */
public interface FilePath {

	String SLASH = "/";
	int SIZE = 128;

	/**
	 * create
	 *
	 * @param path
	 * @return
	 */
	static FilePath create(String path) {
		return new DefaultFilePath(path);
	}

	/**
	 * get original path
	 *
	 * @return
	 */
	String getOriginalPath();

	/**
	 * get current path
	 *
	 * @return
	 */
	String getCurrentPath();

	/**
	 * concat path and trim
	 *
	 * @param paths
	 * @return
	 */
	String concat(Object... paths);

	/**
	 * concat path and trim
	 *
	 * @param paths
	 * @return
	 */
	String concat(Collection<?> paths);

	/**
	 * sub path from left
	 *
	 * @param path
	 * @return
	 */
	String subPath(String path);

	/**
	 * copy
	 *
	 * @return
	 */
	FilePath copy();

}
