package blue.base.core.net;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 2021-06-29
 */
public interface Pipeline {

	void addLast(ChannelHandler handler);

	void addFirst(ChannelHandler handler);

	void addBefore(Class<?> handlerClazz, ChannelHandler handler);

	void addAfter(Class<?> handlerClass, ChannelHandler handler);

	boolean remove(ChannelHandler handler);

	<T extends ChannelHandler> T remove(Class<T> handlerClass);

	<T extends ChannelHandler> T get(Class<T> handlerClass);

	List<ChannelHandler> getAll();

}
