package blue.base.core.net;

import blue.base.core.util.AssertUtil;
import blue.base.internal.core.net.aio.AioServer;

/**
 * @author Jin Zheng
 * @since 2021-06-28
 */
public class NetFactory {
	private NetFactory() {
	}

	public static Server createServer(ServerOptions options) {
        AssertUtil.notNull(options, "ServerOptions");
        return new AioServer(options);
    }

}
