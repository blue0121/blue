package blue.base.core.security;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author Jin Zheng
 * @since 2021-06-16
 */
public interface KeyPair {

	/**
	 * 获取公钥
	 *
	 * @return
	 */
	PublicKey getPublic();

	/**
	 * 获取公钥编码后的字节数组
	 * @return
	 */
	byte[] getPublicEncoded();

	/**
	 * 获取公钥编码后的Base64字符串
	 * @return
	 */
	String getPublicBase64();

	/**
	 * 获取私钥
	 *
	 * @return
	 */
	PrivateKey getPrivate();

	/**
	 * 获取私钥编码后的字节数组
	 * @return
	 */
	byte[] getPrivateEncoded();

	/**
	 * 获取私钥编码后的Base64字符串
	 * @return
	 */
	String getPrivateBase64();
}
