package test.base.core.security;

import blue.base.core.security.KeyPair;
import blue.base.core.security.KeyPairMode;
import blue.base.core.security.SecurityFactory;
import blue.base.core.security.Signature;
import blue.base.core.security.SignatureMode;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.InputStream;

/**
 * @author Jin Zheng
 * @since 2021-06-15
 */
public class SignatureTest {
    private String data = "我的电脑-jin.zheng";

    private static String prefix = "/cert";

    public SignatureTest() {
    }

    @ParameterizedTest
    @CsvSource({"MD5_RSA,RSA,/rsa_pub.pub,/rsa_priv_pkcs8.key",
            "SHA1_RSA,RSA,/rsa_pub.pub,/rsa_priv_pkcs8.key",
            "SHA256_RSA,RSA,/rsa_pub.pub,/rsa_priv_pkcs8.key",
            "SHA384_RSA,RSA,/rsa_pub.pub,/rsa_priv_pkcs8.key",
            "SHA512_RSA,RSA,/rsa_pub.pub,/rsa_priv_pkcs8.key",
            "SHA1_DSA,DSA,/dsa_pub.pub,/dsa_priv_pkcs8.key",
            "SHA224_DSA,DSA,/dsa_pub.pub,/dsa_priv_pkcs8.key",
            "SHA256_DSA,DSA,/dsa_pub.pub,/dsa_priv_pkcs8.key",
            //"SHA384_DSA,DSA,/dsa_pub.pub,/dsa_priv_pkcs8.key",
            //"SHA512_DSA,DSA,/dsa_pub.pub,/dsa_priv_pkcs8.key",
            "SHA1_ECDSA,EC,/ecdsa_pub.pub,/ecdsa_priv_pkcs8.key",
            "SHA224_ECDSA,EC,/ecdsa_pub.pub,/ecdsa_priv_pkcs8.key",
            "SHA256_ECDSA,EC,/ecdsa_pub.pub,/ecdsa_priv_pkcs8.key",
            "SHA384_ECDSA,EC,/ecdsa_pub.pub,/ecdsa_priv_pkcs8.key",
            "SHA512_ECDSA,EC,/ecdsa_pub.pub,/ecdsa_priv_pkcs8.key"})
    public void testSignature(String mode, String keyMode, String pubPath, String privPath) {
        InputStream inPub = LoadOpenSslKeyPairTest.class.getResourceAsStream(prefix + pubPath);
        InputStream inPriv = LoadOpenSslKeyPairTest.class.getResourceAsStream(prefix + privPath);
        KeyPair keyPair = SecurityFactory.loadOpenSslKeyPair(KeyPairMode.valueOf(keyMode), inPub, inPriv);

        SignatureMode signatureMode = SignatureMode.valueOf(mode);
        Signature signature = SecurityFactory.createSignature(signatureMode);
        String sign = signature.signString(keyPair.getPrivate(), data);
        System.out.println(signatureMode.getKey() + ": " + sign);
        Assertions.assertTrue(signature.verifyString(keyPair.getPublic(), data, sign));
    }

}
