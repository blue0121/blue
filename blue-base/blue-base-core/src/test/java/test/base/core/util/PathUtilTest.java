package test.base.core.util;

import blue.base.core.util.PathUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Jin Zheng
 * @since 1.0 2021-07-30
 */
public class PathUtilTest {
	public PathUtilTest() {
	}

	@Test
	public void testCreateTempFile() throws IOException {
		Path path = Files.createTempFile(null, null);
		System.out.println(path);
		String content = "Hello World";
		Files.writeString(path, content);
		Assertions.assertEquals(content, Files.readString(path));
		Files.delete(path);
	}

	@ParameterizedTest
	@CsvSource({"http://www.web.com/abc,www.web.com", "http://abc/,abc", "http://cde,cde"})
	public void testGetHost(String url, String host) {
		Assertions.assertEquals(host, PathUtil.getHost(url));
	}

	@ParameterizedTest
	@CsvSource({"http://www/file.jpg,file.jpg", "/abc/jpg.jpg,jpg.jpg", "jpg.jpg,jpg.jpg", "abc,abc"})
	public void testGetFilename(String url, String filename) {
		Assertions.assertEquals(filename, PathUtil.getFileName(url));
	}

	@ParameterizedTest
	@CsvSource({"http://www.com/file.jpg,.jpg", "/abc/file.txt,.txt", "file.txt,.txt", "file,file"})
	public void testGetFileExt(String url, String ext) {
		Assertions.assertEquals(ext, PathUtil.getFileExt(url));
	}

	@Test
	public void testGetPath() {
		String file = PathUtil.getPath(PathUtilTest.class, "/log4j2.xml");
		Path path = Paths.get(file);
		Assertions.assertTrue(Files.exists(path));
	}

}
