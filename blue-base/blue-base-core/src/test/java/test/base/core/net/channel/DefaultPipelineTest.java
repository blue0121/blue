package test.base.core.net.channel;

import blue.base.core.net.ChannelHandler;
import blue.base.core.net.Pipeline;
import blue.base.internal.core.net.channel.DefaultPipeline;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 2021-07-05
 */
public class DefaultPipelineTest {

	public DefaultPipelineTest() {
	}

	@Test
	public void testAddLast() {
        Pipeline p = new DefaultPipeline();
		ChannelHandler h1 = new TestChannelHandler1();
		ChannelHandler h2 = new TestChannelHandler2();
		ChannelHandler h3 = new TestChannelHandler3();

		p.addLast(h1);
		p.addLast(h2);
		p.addLast(h3);

		Assertions.assertEquals(List.of(h1, h2, h3), p.getAll());
    }

    @Test
    public void testAddFirst() {
	    Pipeline p = new DefaultPipeline();
	    ChannelHandler h1 = new TestChannelHandler1();
	    ChannelHandler h2 = new TestChannelHandler2();
	    ChannelHandler h3 = new TestChannelHandler3();

	    p.addFirst(h1);
	    p.addFirst(h2);
	    p.addFirst(h3);

		Assertions.assertEquals(List.of(h3, h2, h1), p.getAll());
    }

    @Test
    public void testAddBefore() {
	    Pipeline p = new DefaultPipeline();
	    ChannelHandler h1 = new TestChannelHandler1();
	    ChannelHandler h2 = new TestChannelHandler2();
	    ChannelHandler h3 = new TestChannelHandler3();

	    p.addLast(h1);
	    p.addBefore(TestChannelHandler1.class, h2);
	    p.addBefore(TestChannelHandler1.class, h3);

	    Assertions.assertEquals(List.of(h2, h3, h1), p.getAll());
    }

	@Test
	public void testAddAfter() {
		Pipeline p = new DefaultPipeline();
		ChannelHandler h1 = new TestChannelHandler1();
		ChannelHandler h2 = new TestChannelHandler2();
		ChannelHandler h3 = new TestChannelHandler3();

		p.addLast(h1);
		p.addAfter(TestChannelHandler1.class, h2);
		p.addAfter(TestChannelHandler1.class, h3);

		Assertions.assertEquals(List.of(h1, h3, h2), p.getAll());
	}

	@Test
	public void testRemove() {
		Pipeline p = new DefaultPipeline();
		ChannelHandler h1 = new TestChannelHandler1();
		ChannelHandler h2 = new TestChannelHandler2();
		ChannelHandler h3 = new TestChannelHandler3();

		p.addLast(h1);
		p.addLast(h2);
		p.addLast(h3);

		Assertions.assertTrue(p.remove(h2));
		Assertions.assertEquals(List.of(h1, h3), p.getAll());

		Assertions.assertEquals(h3, p.remove(TestChannelHandler3.class));
		Assertions.assertEquals(List.of(h1), p.getAll());
	}

	@Test
	public void testGet() {
		Pipeline p = new DefaultPipeline();
		ChannelHandler h1 = new TestChannelHandler1();
		ChannelHandler h2 = new TestChannelHandler2();
		ChannelHandler h3 = new TestChannelHandler3();

		p.addLast(h1);
		p.addLast(h2);
		p.addLast(h3);

		Assertions.assertEquals(h1, p.get(TestChannelHandler1.class));
		Assertions.assertEquals(h2, p.get(TestChannelHandler2.class));
		Assertions.assertEquals(h3, p.get(TestChannelHandler3.class));
	}

}
