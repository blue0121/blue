package test.base.core.util;

import blue.base.core.util.RandomUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-20
 */
public class RandomUtilTest {
	public RandomUtilTest() {
	}

    @ParameterizedTest
    @CsvSource({"4,NUMBER","10,NUMBER",
				"5,LOWER_CASE","11,LOWER_CASE",
				"4,UPPER_CASE","10,UPPER_CASE",
				"5,UPPER_LOWER_CASE","11,UPPER_LOWER_CASE",
				"4,LOWER_CASE_NUMBER","10,LOWER_CASE_NUMBER",
				"5,UPPER_CASE_NUMBER","11,UPPER_CASE_NUMBER",
				"4,UPPER_LOWER_CASE_NUMBER","10,UPPER_LOWER_CASE_NUMBER"})
    public void testRandom(int len, RandomUtil.RandomType type) {
        String ran = RandomUtil.rand(type, len);
	    System.out.println(ran);
	    Assertions.assertNotNull(ran);
		Assertions.assertEquals(ran.length(), len);
	    Pattern pattern = Pattern.compile(this.getPattern(type, len));
		Matcher matcher = pattern.matcher(ran);
		Assertions.assertTrue(matcher.matches());
    }

	private String getPattern(RandomUtil.RandomType type, int len) {
		String patternType = switch (type) {
			case NUMBER -> "\\d";
			case LOWER_CASE -> "[a-z]";
			case UPPER_CASE -> "[A-Z]";
			case UPPER_LOWER_CASE -> "[a-zA-Z]";
			case LOWER_CASE_NUMBER -> "[a-z0-9]";
			case UPPER_CASE_NUMBER -> "[A-Z0-9]";
			case UPPER_LOWER_CASE_NUMBER -> "[a-zA-Z0-9]";
		};
		StringBuilder pattern = new StringBuilder(16);
		pattern.append("^")
				.append(patternType)
				.append("{")
				.append(len)
				.append("}$");
		return pattern.toString();
	}

}
