package test.base.core.http;

import blue.base.core.http.HttpClient;
import blue.base.core.http.StringResponse;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author Jin Zheng
 * @since 2020-04-25
 */
public class HttpClientTest {
	private static final int PORT = 10000;

	private static WireMockServer server;

	private HttpClient httpClient;

	public HttpClientTest() {
	}

	@BeforeAll
	public static void beforeAll() {
		server = new WireMockServer(PORT);
		server.start();
		WireMock.configureFor(PORT);
	}

	@AfterAll
	public static void afterAll() {
		server.stop();
	}

	@BeforeEach
	public void beforeEach() {
		Map<String, String> headers = Map.of("Content-Type", "application/json");
		httpClient = HttpClient.builder().setBaseUrl("http://localhost:10000")
				.setTimeout(10000)
				.setDefaultHeaders(headers)
				.build();
		WireMock.reset();
	}

	@Test
	public void testGet() throws Exception {
		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo("/test"))
				.withHeader("Content-Type", WireMock.equalTo("application/json"))
				.willReturn(WireMock.aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("test_content")));
		StringResponse response = httpClient.getSync("/test");
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertEquals("test_content", response.getBody());

		CompletableFuture<StringResponse> future = httpClient.getAsync("/test");
		future.thenAccept(r -> System.out.println(r.getCode() + ": " + r.getBody()));
		response = future.get();
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertEquals("test_content", response.getBody());
	}

	@Test
	public void testPost() throws Exception {
		WireMock.stubFor(WireMock.post(WireMock.urlEqualTo("/test"))
				.withHeader("Content-Type", WireMock.equalTo("application/json"))
				.withRequestBody(WireMock.equalTo("test_body"))
				.willReturn(WireMock.aResponse()
						.withHeader("Content-Type", "image/jpeg")
						.withBody("test_content")));
		StringResponse response = httpClient.postSync("/test", "test_body");
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertEquals("test_content", response.getBody());

		CompletableFuture<StringResponse> future = httpClient.postAsync("/test", "test_body");
		future.thenAccept(r -> System.out.println(r.getCode() + ": " + r.getBody()));
		response = future.get();
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertEquals("test_content", response.getBody());
	}

}
