package test.base.core.net.aio;

import blue.base.core.net.NetFactory;
import blue.base.core.net.Server;
import blue.base.core.net.ServerOptions;

/**
 * @author Jin Zheng
 * @since 2021-06-28
 */
public class AioServerMain {
	public AioServerMain() {
	}

    public static void main(String[] args) {
        ServerOptions options = new ServerOptions();
        Server server = NetFactory.createServer(options);
        server.start();
        server.await();
        server.close();
    }
}
