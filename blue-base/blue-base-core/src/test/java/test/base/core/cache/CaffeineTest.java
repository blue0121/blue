package test.base.core.cache;

import blue.base.core.util.WaitUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-27
 */
public class CaffeineTest {
	private static Logger logger = LoggerFactory.getLogger(CaffeineTest.class);

	private String key = "key";
	private String key2 = "key2";

	public CaffeineTest() {
	}

    @Test
    public void testRemove() {
	    Cache<String, String> cache = Caffeine.newBuilder()
			    .expireAfterAccess(Duration.ofMillis(10))
			    .removalListener((k, v, cause) -> {
					logger.info("removalListener, k: {}, v: {}, cause: {}", k, v, cause);
			    }).evictionListener((k, v, cause) -> {
				    logger.info("evictionListener, k: {}, v: {}, cause: {}", k, v, cause);
			    })
			    .build();
		cache.put(key, key);
		cache.put(key2, key2);
		cache.invalidate(key2);
	    WaitUtil.sleep(20);
	    Assertions.assertNull(cache.getIfPresent(key));
    }

}
