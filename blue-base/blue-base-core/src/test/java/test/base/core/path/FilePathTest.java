package test.base.core.path;

import blue.base.core.path.FilePath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * @author Jin Zheng
 * @since 1.0 2021-02-19
 */
public class FilePathTest {
	public FilePathTest() {
	}

	@Test
	public void testConcat() {
		FilePath path = FilePath.create("/a");
		Assertions.assertEquals("/a/b", path.concat("b"));
		Assertions.assertEquals("/a/b", path.getCurrentPath());
		Assertions.assertEquals("/a/b/c/d", path.concat("c", "//d"));
		Assertions.assertEquals("/a/b/c/d", path.getCurrentPath());
		Assertions.assertEquals("/a", path.getOriginalPath());
	}

	@ParameterizedTest
	@CsvSource({"/api/test/,/api,/test","/api,/,/api","/api/test,,/api/test","/api/test1/test2,/api/test2,/api/test1/test2"})
	public void testSubPath(String uri, String contextPath, String url) {
		FilePath path = FilePath.create(uri);
		Assertions.assertEquals(url, path.subPath(contextPath));
	}

	@Test
	public void testTrim() {
		FilePath path = FilePath.create("a//b");
		Assertions.assertEquals("/a/b", path.getCurrentPath());
	}

}
