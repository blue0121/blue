package test.base.core.cache;

import blue.base.core.cache.Cache;
import blue.base.core.cache.RemovalCause;
import blue.base.core.cache.RemovalListener;
import blue.base.core.util.WaitUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-27
 */
public class RemovalListenerTest implements RemovalListener<Integer, Integer> {
	private static Logger logger = LoggerFactory.getLogger(RemovalListenerTest.class);
	private Cache<Integer, Integer> cache;

	private Integer key;
	private Integer value;
	private RemovalCause cause;
	
	public RemovalListenerTest() {
	}

	@BeforeEach
	public void beforeEach() {
		cache = Cache.builder()
				.expireAfterAccess(10, TimeUnit.MILLISECONDS)
				.removalListener(this)
				.build();
	}

	@Test
	public void test() {
		cache.put(1, 1);
		Assertions.assertEquals(1, cache.get(1).intValue());
		WaitUtil.sleep(20);
		Assertions.assertNull(cache.get(1));

	}

	@Override
	public void onRemoval(Integer key, Integer value, RemovalCause cause) {
		this.key = key;
		this.value = value;
		this.cause = cause;
		logger.info("onRemoval, key: {}, value: {}, cause: {}", key, value, cause);
	}
}
