package test.base.core.security;

import blue.base.core.security.KeyPair;
import blue.base.core.security.KeyPairMode;
import blue.base.core.security.SecurityFactory;
import blue.base.internal.core.security.AbstractKeyPair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.InputStream;

/**
 * @author Jin Zheng
 * @since 2021-06-22
 */
public class LoadOpenSslKeyPairTest {
	private static String prefix = "/cert";

	public LoadOpenSslKeyPairTest() {
	}

	@ParameterizedTest
	@CsvSource({"RSA,/rsa_pub.pub,/rsa_priv_pkcs8.key,false",
			"RSA,/public.pem,/private.key,false",
			"RSA,/public.crt,/private.key,true",
			"DSA,/dsa_pub.pub,/dsa_priv_pkcs8.key,false",
			"EC,/ecdsa_pub.pub,/ecdsa_priv_pkcs8.key,false"})
	public void testLoadOpenSsl(String mode, String pubPath, String privPath, boolean cert) {
		InputStream inPub = LoadOpenSslKeyPairTest.class.getResourceAsStream(prefix + pubPath);
		InputStream inPriv = LoadOpenSslKeyPairTest.class.getResourceAsStream(prefix + privPath);
		KeyPairMode keyPairMode = KeyPairMode.valueOf(mode);
		Assertions.assertEquals(0, keyPairMode.getSize());
		Assertions.assertTrue(keyPairMode.isManual());

		KeyPair keyPair = SecurityFactory.loadOpenSslKeyPair(keyPairMode, inPub, inPriv);
		Assertions.assertNotNull(keyPair);
		Assertions.assertNotNull(keyPair.getPublic());
		Assertions.assertNotNull(keyPair.getPrivate());

		AbstractKeyPair abstractKeyPair = (AbstractKeyPair) keyPair;
		if (cert) {
			Assertions.assertNotNull(abstractKeyPair.getCertificate());
		} else {
			Assertions.assertNull(abstractKeyPair.getCertificate());
		}
	}

}
