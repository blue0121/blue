package test.base.core.security;

import blue.base.core.security.Digest;
import blue.base.core.security.DigestType;
import blue.base.core.security.SecurityFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.Provider;
import java.security.Security;

/**
 * @author Jin Zheng
 * @since 2021-06-01
 */
public class DigestTest {
	private String key = "testblue";
	private String path = "/log4j2.xml";

	public DigestTest() {
	}

	@Test
	@Disabled
	public void testAllProviders() {
		Provider[] providers = Security.getProviders();
		for (var provider : providers) {
			System.out.println("==========" + provider + "==========");
			for (var key : provider.keySet()) {
				System.out.println(key);
			}
		}
	}

	@ParameterizedTest
	@CsvSource({"MD5,16",
				"SHA1,20",
				"SHA256,32",
				"SHA384,48",
				"SHA512,64"})
	public void testDigest(String type, int len) {
		Digest digest = SecurityFactory.createDigest(DigestType.valueOf(type));
		int len2 = len * 2;
		Assertions.assertEquals(len, digest.digest(key).length);
		Assertions.assertEquals(len2, digest.digestToHex(key).length());
		Assertions.assertEquals(len2, digest.digestToHex(key.getBytes(StandardCharsets.UTF_8)).length());
		InputStream is = DigestTest.class.getResourceAsStream(path);
		Assertions.assertEquals(len, digest.digest(is).length);
		is = DigestTest.class.getResourceAsStream(path);
		Assertions.assertEquals(len2, digest.digestToHex(is).length());
		Assertions.assertEquals(type, digest.getDigestType().name());
	}

}
