package test.http.core.handler.parameter;

import blue.base.core.common.ErrorCode;
import blue.base.core.common.ErrorCodeException;
import blue.http.core.annotation.HttpHeader;
import blue.http.core.message.HttpRequest;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.parser.RequestParamConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-09
 */
@ExtendWith(MockitoExtension.class)
public class HttpHeaderHandlerTest {
	@Mock
	private HttpRequest request;
	@Mock
	private WebSocketRequest webSocketRequest;

	private ParameterDispatcher dispatcher = new ParameterDispatcher();

	public HttpHeaderHandlerTest() {
	}

	@Test
	public void testEmpty() {
		RequestParamConfig config = this.createConfig(String.class, "name", true, true);
		try {
			dispatcher.handleParam(config, request);
			Assertions.fail();
		} catch (ErrorCodeException e) {
			this.assertException(e, "name 不能为空");
		}

		RequestParamConfig config2 = this.createConfig(String.class, "name", false, true);
		String result = (String) dispatcher.handleParam(config2, request);
		Assertions.assertNull(result);

		this.mockRequest(Map.of("name", ""));
		try {
			dispatcher.handleParam(config, request);
			Assertions.fail();
		} catch (ErrorCodeException e) {
			this.assertException(e, "name 不能为空");
		}

		String result2 = (String) dispatcher.handleParam(config2, request);
		Assertions.assertNull(result2);
	}

	@Test
	public void testString() {
		this.mockRequest(Map.of("name", "blue"));
		RequestParamConfig config = this.createConfig(String.class, "name", true, true);
		String result = (String) dispatcher.handleParam(config, request);
		Assertions.assertEquals("blue", result);
	}

	@Test
	public void testInt() {
		this.mockRequest(Map.of("id", "10"));
		RequestParamConfig config = this.createConfig(Integer.class, "id", true, true);
		Integer result = (Integer) dispatcher.handleParam(config, request);
		Assertions.assertEquals(10, result.intValue());
	}

	@Test
	public void testWebSocket() {
		RequestParamConfig config = this.createConfig(String.class, "name", true, true);
		Assertions.assertNull(dispatcher.handleParam(config, webSocketRequest));
	}

	private void mockRequest(Map<String, String> map) {
		for (var entry : map.entrySet()) {
			Mockito.when(request.getHeader(Mockito.eq(entry.getKey()))).thenReturn(entry.getValue());
		}
	}

	private RequestParamConfig createConfig(Class<?> paramClazz, String name, boolean required, boolean validated) {
		RequestParamConfig config = new RequestParamConfig();
		config.setName(name);
		config.setParamClazz(paramClazz);
		config.setParamAnnotationValue(name);
		config.setParamAnnotationClazz(HttpHeader.class);
		config.setParamAnnotationRequired(required);
		config.setValidated(validated);
		return config;
	}

	private void assertException(ErrorCodeException e, String message) {
		e.printStackTrace();
		Assertions.assertEquals(400, e.getHttpStatus());
		Assertions.assertEquals(ErrorCode.REQUIRED.getCode(), e.getCode());
		Assertions.assertEquals(message, e.getMessage());
	}

}
