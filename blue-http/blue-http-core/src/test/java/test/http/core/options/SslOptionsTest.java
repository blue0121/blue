package test.http.core.options;

import blue.http.core.options.SslOptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-12
 */
public class SslOptionsTest {
	public SslOptionsTest() {
	}

	@Test
	public void testDisable() {
		SslOptions options = new SslOptions();
		options.setEnable(false);
		options.check();
		Assertions.assertNull(options.getSslContext());
	}

	@Test
	public void testEnable() {
		SslOptions options = new SslOptions();
		options.setEnable(true)
				.setKeyCertChainIs(SslOptionsTest.class.getResourceAsStream("/cert/public.crt"))
				.setKeyIs(SslOptionsTest.class.getResourceAsStream("/cert/private.key"));
		options.check();
		Assertions.assertNotNull(options.getSslContext());
	}

}
