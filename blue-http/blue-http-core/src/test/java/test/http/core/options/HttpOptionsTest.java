package test.http.core.options;

import blue.http.core.options.HttpOptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-13
 */
public class HttpOptionsTest {
	public HttpOptionsTest() {
	}

	@Test
	public void test() {
		HttpOptions options = new HttpOptions();
		options.setContextPath("api");
		options.setMaxUploadSize(1_000);
		options.setViewRoot("/view");
		options.putVirtualPath("/upload", "/opt/upload");
		options.check();
		Assertions.assertEquals("/api", options.getContextPath());
		Assertions.assertEquals(1_000, options.getMaxUploadSize());
		Assertions.assertEquals("/view", options.getViewRoot());
		var map = options.getVirtualPathMap();
		Assertions.assertEquals(1, map.size());
		Assertions.assertEquals("/opt/upload", map.get("/upload"));
	}

}
