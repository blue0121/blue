package test.http.core.handler.exception;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
public class Error {
    public int code;
    public String message;
}
