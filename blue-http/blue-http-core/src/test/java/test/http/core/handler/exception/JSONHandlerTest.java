package test.http.core.handler.exception;

import blue.base.core.util.JsonUtil;
import blue.http.internal.core.handler.HandlerResponse;
import blue.http.internal.core.handler.exception.ExceptionDispatcher;
import com.alibaba.fastjson.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
public class JSONHandlerTest {
	private ExceptionDispatcher dispatcher = new ExceptionDispatcher();

	public JSONHandlerTest() {
	}

    @Test
	public void test() {
        HandlerResponse response = this.createResponse(new JSONException());
		dispatcher.handleException(response);
	    System.out.println(response.getResult());
	    Assertions.assertEquals(400, response.getHttpStatus().code());
		Error error = JsonUtil.fromString(response.getResult().toString(), Error.class);
		Assertions.assertEquals(400_001, error.code);
		Assertions.assertEquals("无效JSON格式", error.message);
    }

    private HandlerResponse createResponse(Throwable cause) {
	    HandlerResponse response = new HandlerResponse();
	    response.setCause(cause);
	    return response;
    }

}
