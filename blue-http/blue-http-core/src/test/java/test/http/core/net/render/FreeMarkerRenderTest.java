package test.http.core.net.render;

import blue.http.internal.core.net.render.FreeMarkerRender;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-12
 */
public class FreeMarkerRenderTest {
	public FreeMarkerRenderTest() {
	}

	@Test
	public void testRender() {
		FreeMarkerRender render = new FreeMarkerRender("/view/");
		Map<String, Object> param = Map.of("name", "blue");
		String html = render.render("index", param);
		Assertions.assertEquals("hello, blue", html);
	}

}
