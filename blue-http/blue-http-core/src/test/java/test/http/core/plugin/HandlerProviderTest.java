package test.http.core.plugin;

import blue.http.core.plugin.HandlerProvider;
import blue.http.core.plugin.HandlerRegister;
import blue.http.internal.core.net.DefaultHttpServerBuilder;
import blue.http.internal.core.plugin.ProviderManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author Jin Zheng
 * @date 2021-09-16
 */
@ExtendWith(MockitoExtension.class)
public class HandlerProviderTest {
	@Mock
	private DefaultHttpServerBuilder builder;

	public HandlerProviderTest() {

	}

	@Test
	public void testHandle() {
		ProviderManager manager = new ProviderManager(builder);
		Object object = new Object();
		manager.addProvider(new HandlerProvider() {
			@Override
			public void handle(HandlerRegister register) {
				register.addHandler(object);
			}
		});
		ArgumentCaptor<Object> captor = ArgumentCaptor.forClass(Object.class);
		Mockito.verify(builder).addHandler(captor.capture());
		Assertions.assertTrue(captor.getValue() == object);
	}

	@Test
	public void testError() {
		ProviderManager manager = new ProviderManager(builder);
		try {
			manager.addProvider(new Object());
			Assertions.fail();
		} catch (Exception e) {
			Assertions.assertEquals("Unknown Provider", e.getMessage());
		}
	}

}
