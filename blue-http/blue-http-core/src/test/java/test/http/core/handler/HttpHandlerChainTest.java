package test.http.core.handler;

import blue.http.core.filter.HttpFilter;
import blue.http.core.message.HttpRequest;
import blue.http.core.message.HttpResponse;
import blue.http.internal.core.handler.HttpHandlerChain;
import blue.http.internal.core.parser.HttpMethodResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
@ExtendWith(MockitoExtension.class)
public class HttpHandlerChainTest {
	@Mock
	private HttpMethodResult handler;
	@Mock
	private HttpRequest request;
	@Mock
	private HttpResponse response;
	@Mock
	private HttpFilter logFilter;
	@Mock
	private HttpFilter authFilter;

	public HttpHandlerChainTest() {
	}

	@Test
	public void testPreHandleAndAfterCompletion1() throws Exception {
		Mockito.doReturn(true).when(logFilter).preHandle(Mockito.any(), Mockito.any());
		Mockito.doReturn(true).when(authFilter).preHandle(Mockito.any(), Mockito.any());
		HttpHandlerChain chain = new HttpHandlerChain(handler, List.of(logFilter, authFilter));
		boolean result = chain.applyPreHandle(request, response);
		chain.triggerAfterCompletion(request, response, null);
		Assertions.assertTrue(result);
		Mockito.verify(logFilter).preHandle(Mockito.any(), Mockito.any());
		Mockito.verify(authFilter).preHandle(Mockito.any(), Mockito.any());
		Mockito.verify(logFilter).afterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(authFilter).afterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void testPreHandleAndAfterCompletion2() throws Exception {
		Mockito.doReturn(true).when(logFilter).preHandle(Mockito.any(), Mockito.any());
		Mockito.doReturn(false).when(authFilter).preHandle(Mockito.any(), Mockito.any());
		HttpHandlerChain chain = new HttpHandlerChain(handler, List.of(logFilter, authFilter));
		boolean result = chain.applyPreHandle(request, response);
		chain.triggerAfterCompletion(request, response, null);
		Assertions.assertFalse(result);
		Mockito.verify(logFilter).preHandle(Mockito.any(), Mockito.any());
		Mockito.verify(authFilter).preHandle(Mockito.any(), Mockito.any());
		Mockito.verify(logFilter).afterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(authFilter).afterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void testPreHandleAndAfterCompletion3() throws Exception {
		Mockito.doReturn(false).when(logFilter).preHandle(Mockito.any(), Mockito.any());
		HttpHandlerChain chain = new HttpHandlerChain(handler, List.of(logFilter, authFilter));
		boolean result = chain.applyPreHandle(request, response);
		chain.triggerAfterCompletion(request, response, null);
		Assertions.assertFalse(result);
		Mockito.verify(logFilter).preHandle(Mockito.any(), Mockito.any());
		Mockito.verify(authFilter, Mockito.never()).preHandle(Mockito.any(), Mockito.any());
		Mockito.verify(logFilter).afterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(authFilter, Mockito.never()).afterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
	}

	@Test
	public void testPostHandle() throws Exception {
		HttpHandlerChain chain = new HttpHandlerChain(handler, List.of(logFilter, authFilter));
		chain.applyPostHandle(request, response);
		Mockito.verify(logFilter).postHandle(Mockito.any(), Mockito.any());
		Mockito.verify(authFilter).postHandle(Mockito.any(), Mockito.any());
	}

}
