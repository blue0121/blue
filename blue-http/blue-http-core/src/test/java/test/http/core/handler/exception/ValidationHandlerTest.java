package test.http.core.handler.exception;

import blue.base.core.util.JsonUtil;
import blue.http.internal.core.handler.HandlerResponse;
import blue.http.internal.core.handler.exception.ExceptionDispatcher;
import jakarta.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
public class ValidationHandlerTest {
	private ExceptionDispatcher dispatcher = new ExceptionDispatcher();

	public ValidationHandlerTest() {
	}

    @Test
	public void test() {
        HandlerResponse response = this.createResponse(new ValidationException("用户名不能为空"));
		dispatcher.handleException(response);
	    System.out.println(response.getResult());
	    Assertions.assertEquals(400, response.getHttpStatus().code());
		Error error = JsonUtil.fromString(response.getResult().toString(), Error.class);
		Assertions.assertEquals(400_000, error.code);
		Assertions.assertEquals("无效参数：用户名不能为空", error.message);
    }

    private HandlerResponse createResponse(Throwable cause) {
	    HandlerResponse response = new HandlerResponse();
	    response.setCause(cause);
	    return response;
    }

}
