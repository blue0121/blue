package test.http.core.parser;

import blue.http.core.annotation.BodyJson;
import blue.http.core.annotation.HttpMethod;
import blue.http.core.annotation.Multipart;
import blue.http.core.message.UploadFile;
import blue.http.internal.core.parser.HttpConfigCache;
import blue.http.internal.core.parser.HttpMethodResult;
import blue.http.internal.core.parser.HttpUrlKey;
import blue.http.internal.core.parser.ParserFactory;
import blue.http.internal.core.parser.RequestParamConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test.http.core.controller.EchoController;
import test.http.core.controller.UploadController;
import test.http.core.model.User;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-19
 */
public class HttpParamParserTest {
	private ParserFactory factory = new ParserFactory();
	private HttpConfigCache cache = factory.getHttpConfigCache();

	public HttpParamParserTest() {
	}

	@Test
	public void testMultiPart() {
		factory.parse(new UploadController(), UploadController.class);

		HttpUrlKey key = new HttpUrlKey("/upload/index2", HttpMethod.POST);
		HttpMethodResult result = cache.getConfig(key);
		Assertions.assertNotNull(result);
		List<RequestParamConfig> paramList = result.getParamList();
		Assertions.assertEquals(1, paramList.size());
		RequestParamConfig uploadParam = paramList.get(0);
		Assertions.assertEquals(Multipart.class, uploadParam.getParamAnnotationClazz());
		Assertions.assertEquals(UploadFile.class, uploadParam.getParamClazz());

		Assertions.assertEquals("file", uploadParam.getParamAnnotationValue());
		Assertions.assertTrue(uploadParam.isParamAnnotationRequired());
		Assertions.assertFalse(uploadParam.isValidated());
	}

	@Test
	public void testValidated() {
		factory.parse(new EchoController(), EchoController.class);

		HttpUrlKey key = new HttpUrlKey("/echo/validate", HttpMethod.POST);
		HttpMethodResult result = cache.getConfig(key);
		Assertions.assertNotNull(result);
		List<RequestParamConfig> paramList = result.getParamList();
		Assertions.assertEquals(1, paramList.size());
		RequestParamConfig uploadParam = paramList.get(0);
		Assertions.assertEquals(BodyJson.class, uploadParam.getParamAnnotationClazz());
		Assertions.assertEquals(User.class, uploadParam.getParamClazz());

		Assertions.assertEquals("$.user", uploadParam.getParamAnnotationValue());
		Assertions.assertTrue(uploadParam.isParamAnnotationRequired());

		Assertions.assertTrue(uploadParam.isValidated());
	}

}
