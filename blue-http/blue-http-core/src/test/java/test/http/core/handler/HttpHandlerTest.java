package test.http.core.handler;

import blue.base.core.reflect.BeanMethod;
import blue.base.core.reflect.JavaBean;
import blue.base.core.reflect.JavaBeanCache;
import blue.http.core.annotation.Charset;
import blue.http.core.annotation.ContentType;
import blue.http.core.exception.HttpServerException;
import blue.http.core.message.HttpRequest;
import blue.http.internal.core.handler.HttpHandler;
import blue.http.internal.core.handler.HttpHandlerChain;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.parser.HttpMethodResult;
import blue.http.internal.core.parser.RequestParamConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
@ExtendWith(MockitoExtension.class)
public class HttpHandlerTest {
	@Mock
	private ParameterDispatcher dispatcher;
	@Mock
	private HttpRequest request;
	@Mock
	private HttpMethodResult methodResult;
	@Mock
	private HttpHandlerChain chain;

	private HttpHandler handler;

	public HttpHandlerTest() {
	}

	@BeforeEach
	public void beforeEach() {
		Mockito.when(chain.getHandler()).thenReturn(methodResult);
		Mockito.when(methodResult.getContentType()).thenReturn(ContentType.AUTO);
		Mockito.when(methodResult.getCharset()).thenReturn(Charset.UTF_8);
		handler = new HttpHandler(dispatcher);
	}

	@Test
	public void testHandleFailure() throws Exception {
		var response = handler.handle(request, chain);
		Assertions.assertNotNull(response);
		Mockito.verify(chain).applyPreHandle(Mockito.any(), Mockito.any());
		Mockito.verify(chain).triggerAfterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(dispatcher, Mockito.never()).handleParam(Mockito.any(), Mockito.any(HttpRequest.class));
		Mockito.verify(chain, Mockito.never()).applyPostHandle(Mockito.any(), Mockito.any());
	}

	@Test
	public void testHandleSuccess() throws Exception {
		this.mockMethodResult("test");
		Mockito.when(chain.applyPreHandle(Mockito.any(), Mockito.any())).thenReturn(true);
		var response = handler.handle(request, chain);
		Assertions.assertNotNull(response);
		Assertions.assertNull(response.getCause());
		Assertions.assertEquals("test", response.getResult());

		Mockito.verify(chain).applyPreHandle(Mockito.any(), Mockito.any());
		Mockito.verify(chain).triggerAfterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(dispatcher).handleParam(Mockito.any(), Mockito.any(HttpRequest.class));
		Mockito.verify(chain).applyPostHandle(Mockito.any(), Mockito.any());
	}

	@Test
	public void testHandleException() throws Exception {
		this.mockMethodResult("testException");
		Mockito.when(chain.applyPreHandle(Mockito.any(), Mockito.any())).thenReturn(true);
		var response = handler.handle(request, chain);
		Assertions.assertNotNull(response);
		Assertions.assertNotNull(response.getCause());
		Assertions.assertEquals(HttpServerException.class, response.getCause().getClass());

		Mockito.verify(chain).applyPreHandle(Mockito.any(), Mockito.any());
		Mockito.verify(chain).triggerAfterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(dispatcher).handleParam(Mockito.any(), Mockito.any(HttpRequest.class));
		Mockito.verify(chain).applyPostHandle(Mockito.any(), Mockito.any());
	}

	private void mockMethodResult(String methodName) {
		JavaBean bean = JavaBeanCache.getJavaBean(new TestHandler(), TestHandler.class);
		var methodList = bean.getAllMethods();
		BeanMethod method = null;
		for (var m : methodList) {
			if (m.getName().equals(methodName)) {
				method = m;
				break;
			}
		}
		if (method == null) {
			throw new UnsupportedOperationException("Not Found method: " + methodName);
		}
		Mockito.when(methodResult.getMethod()).thenReturn(method);
		List<RequestParamConfig> paramConfigList = new ArrayList<>();
		for (var param : method.getParamList()) {
			RequestParamConfig config = new RequestParamConfig();
			config.setName(param.getName());
			config.setParamClazz(param.getParamClass());
			paramConfigList.add(config);
		}
		Mockito.when(methodResult.getParamList()).thenReturn(paramConfigList);
		Mockito.when(dispatcher.handleParam(Mockito.any(), Mockito.any(HttpRequest.class))).thenReturn(methodName);
	}

}
