package test.http.core.parser;

import blue.http.core.filter.SessionCookieFilter;
import blue.http.core.filter.UploadFileFilter;
import blue.http.internal.core.parser.FilterConfigCache;
import blue.http.internal.core.parser.ParserFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-06
 */
public class FilterParserTest {
	private ParserFactory factory =new ParserFactory();
	private FilterConfigCache cache = factory.getFilterConfigCache();

	public FilterParserTest() {
	}

	@Test
	public void testParse() {
		factory.parse(new SessionCookieFilter(), SessionCookieFilter.class);
		factory.parse(new UploadFileFilter(), UploadFileFilter.class);

		var list = cache.matchHttpFilter("/hello");
		Assertions.assertEquals(2, list.size());
		Assertions.assertEquals(SessionCookieFilter.class, list.get(0).getClass());
		Assertions.assertEquals(UploadFileFilter.class, list.get(1).getClass());
	}

	@Test
	public void testTreeSet() {
		SortedSet<Integer> set = new TreeSet<>();
		set.add(1);
		set.add(1);
		set.add(2);
		set.add(2);
		System.out.println(set);
		Assertions.assertEquals(2, set.size());
	}

}
