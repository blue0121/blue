package test.http.core.it;

import blue.base.core.util.JsonUtil;
import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-24
 */
public class ItUserTest extends ItBaseTest {
	private static Logger logger = LoggerFactory.getLogger(ItUserTest.class);

	private UserService userService = UserService.INSTANCE;

	public ItUserTest() {
	}

	@BeforeEach
	public void beforeEach() {
		userService.clear();
		System.out.println(">>>>>> clear");
	}

	@ParameterizedTest
	@CsvSource({",,false,400000",
			"blue,,false,400000",
			",pwd,false,400000",
			"blue,pwd,false,200000",
			"blue,pwd,true,400004"})
	public void testReg(String username, String password, boolean duplicate, int code) {
		User user = new User(username, password);
		if (duplicate) {
			userService.reg(user);
		}
		String json = JsonUtil.toString(user);
		var resp = client.postSync("/user/reg", json);
		logger.info("Response: {}", resp.getBody());
		Assertions.assertEquals(code / 1000, resp.getCode());
		var obj = JSON.parseObject(resp.getBody());
		Assertions.assertEquals(code, obj.getIntValue("code"));
	}

	@ParameterizedTest
	@CsvSource({"blue,pwd,false,401000",
			"blue,pwd,true,200000"})
	public void testLogin(String username, String password, boolean reg, int code) {
		User user = new User(username, password);
		if (reg) {
			userService.reg(user);
		}
		String json = JsonUtil.toString(user);
		var resp = client.postSync("/user/login", json);
		logger.info("Response: {}", resp.getBody());
		Assertions.assertEquals(code / 1000, resp.getCode());
		var obj = JSON.parseObject(resp.getBody());
		if (code / 1000 == 200) {
			Assertions.assertNotNull(obj.getString("token"));
		} else {
			Assertions.assertEquals(code, obj.getIntValue("code"));
		}
	}

	@ParameterizedTest
	@CsvSource({"blue,pwd,false,401001",
			"blue,pwd,true,200000"})
	public void testListAll(String username, String password, boolean login, int code) {
		User user = new User(username, password);
		String token = "";
		if (login) {
			userService.reg(user);
			String json = JsonUtil.toString(user);
			var resp = client.postSync("/user/login", json);
			Assertions.assertEquals(200, resp.getCode());
			var obj = JSON.parseObject(resp.getBody());
			token = obj.getString("token");
		}
		var resp = client.getSync("/user/list_all", Map.of("X-Token", token));
		logger.info("Response: {}", resp.getBody());
		Assertions.assertEquals(code / 1000, resp.getCode());
		if (code / 1000 == 200) {
			var arr = JSON.parseArray(resp.getBody(), User.class);
			Assertions.assertEquals(1, arr.size());
		} else {
			var obj = JSON.parseObject(resp.getBody());
			Assertions.assertEquals(code, obj.getIntValue("code"));
		}
	}

}
