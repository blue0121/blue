package test.http.core.it;

import blue.validation.core.group.GetGroup;
import blue.validation.core.group.SaveGroup;
import jakarta.validation.constraints.NotEmpty;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-24
 */
public class User {
	@NotEmpty(groups = {SaveGroup.class, GetGroup.class}, message = "用户名不能为空")
    private String username;
	@NotEmpty(groups = {SaveGroup.class, GetGroup.class}, message = "密码不能为空")
    private String password;

	public User() {
	}

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
