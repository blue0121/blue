package test.http.core.handler.parameter;

import blue.http.core.message.HttpRequest;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.parser.RequestParamConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-09
 */
@ExtendWith(MockitoExtension.class)
public class HttpRequestHandlerTest {
	@Mock
	private HttpRequest request;
	@Mock
	private WebSocketRequest webSocketRequest;

	private ParameterDispatcher dispatcher = new ParameterDispatcher();

	public HttpRequestHandlerTest() {
	}

	@Test
	public void testHttpRequest() {
		RequestParamConfig config = this.createConfig("name", true);
		HttpRequest result = (HttpRequest) dispatcher.handleParam(config, request);
		Assertions.assertNotNull(result);
	}

	@Test
	public void testWebSocket() {
		RequestParamConfig config = this.createConfig("name", true);
		Assertions.assertNull(dispatcher.handleParam(config, webSocketRequest));
	}

	private RequestParamConfig createConfig(String name, boolean required) {
		RequestParamConfig config = new RequestParamConfig();
		config.setName(name);
		config.setParamClazz(HttpRequest.class);
		return config;
	}

}
