package test.http.core.handler.exception;

import blue.base.core.common.ErrorCode;
import blue.base.core.common.ErrorCodeException;
import blue.base.core.util.JsonUtil;
import blue.http.internal.core.handler.HandlerResponse;
import blue.http.internal.core.handler.exception.ExceptionDispatcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
public class ErrorCodeHandlerTest {
    private ExceptionDispatcher dispatcher = new ExceptionDispatcher();

	public ErrorCodeHandlerTest() {
	}

	@Test
	public void test1() {
        ErrorCodeException ex = new ErrorCodeException(ErrorCode.NO_DATA);
	    HandlerResponse response = this.createResponse(ex);
	    dispatcher.handleException(response);
		Assertions.assertEquals(404, response.getHttpStatus().code());
		Error error = JsonUtil.fromString(response.getResult().toString(), Error.class);
		Assertions.assertEquals(404_001, error.code);
		Assertions.assertEquals("没有数据", error.message);
    }

    @Test
    public void test2() {
	    OtherException ex = new OtherException(ErrorCode.NO_PARAM);
	    HandlerResponse response = this.createResponse(ex);
	    dispatcher.handleException(response);
	    Assertions.assertEquals(400, response.getHttpStatus().code());
	    Error error = JsonUtil.fromString(response.getResult().toString(), Error.class);
	    Assertions.assertEquals(400_002, error.code);
	    Assertions.assertEquals("没有参数", error.message);
    }

    private HandlerResponse createResponse(Throwable cause) {
        HandlerResponse response = new HandlerResponse();
        response.setCause(cause);
        return response;
    }

    private static class OtherException extends ErrorCodeException {
	    public OtherException(ErrorCode errorCode, Object... args) {
		    super(errorCode, args);
	    }
    }
}
