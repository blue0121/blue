package test.http.core;

import blue.base.core.http.HttpClient;
import blue.base.core.util.PathUtil;
import blue.base.core.util.WaitUtil;
import blue.http.core.HttpServer;
import blue.http.core.HttpServerBuilder;
import blue.http.core.options.HttpOptions;
import blue.http.core.options.ServerOptions;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import test.http.core.controller.DefaultTestController;
import test.http.core.controller.EchoController;
import test.http.core.controller.FtlController;
import test.http.core.controller.HelloController;
import test.http.core.controller.UploadController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-18
 */
public class HttpServerTest {
	private static final int PORT;
	private static final String HOST;

	private static HttpServer server;
	private static HttpClient client;

	static {
		Random random = new Random();
		PORT = random.nextInt(100) + 40000;
		HOST = "http://localhost:" + PORT;
	}

	public HttpServerTest() {
	}

	@BeforeAll
    public static void beforeAll() {
		ServerOptions serverOptions = new ServerOptions();
		serverOptions.setPort(PORT)
				.setStartup(System.currentTimeMillis())
				.setExecutor(Executors.newFixedThreadPool(10))
				.setIoThread(2);
		HttpOptions options = new HttpOptions();
		options.setContextPath("/")
				.setViewRoot("/view");
		HttpServerBuilder builder = HttpServer.builder();
		builder.setServerOptions(serverOptions)
				.setHttpOptions(options)
				.addHandler(new DefaultTestController(), DefaultTestController.class)
				.addHandler(new HelloController(), HelloController.class)
				.addHandler(new UploadController(), UploadController.class)
				.addHandler(new FtlController(), FtlController.class)
				.addHandler(new EchoController(), EchoController.class);
		server = builder.build();
		CountDownLatch latch = new CountDownLatch(1);
		new Thread(() -> {
			try {
				server.start(latch);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		WaitUtil.await(latch);

		client = HttpClient.builder()
				.setBaseUrl(HOST)
				.setTimeout(5_000)
				.build();
    }

    @AfterAll
    public static void afterAll() {
		if (server != null) {
			server.stop();
		}
    }

    @Test
    public void testEcho() {
		String context = "hello";
		var response = client.requestSync("/echo", "POST", context);
	    Assertions.assertEquals(200, response.getCode());
	    Assertions.assertEquals(context, response.getBody());
    }

    @Test
    public void testTest() {
		String name = "blue";
		var response = client.getSync("/test/" + name);
	    Assertions.assertEquals(200, response.getCode());
	    Assertions.assertEquals("test, " + name, response.getBody());
    }

	@Test
	public void testHello() {
		String name = "blue";
		var response = client.postSync("/hello", name);
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertEquals("hello, " + name, response.getBody());

		response = client.postSync("/hello/hello2", name);
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertEquals("hello2, " + name, response.getBody());

		response = client.getSync("/hello/hello3/" + name);
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertEquals("hello3, " + name, response.getBody());

		response = client.getSync("/hello/hello4?name=" + name);
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertEquals("hello4, " + name, response.getBody());
	}

    @Test
    public void testFtl() {
	    String name = "blue";
	    var response = client.getSync("/ftl");
	    Assertions.assertEquals(200, response.getCode());
	    Assertions.assertEquals("hello, " + name, response.getBody());
    }

    @Test
    public void testUpload() throws IOException {
		var path = PathUtil.getPath(HttpServerTest.class, "/log4j2.xml");
	    System.out.println(path);
	    Path file = Paths.get(path);
	    System.out.println(file);
		var response = client.uploadSync("/upload/index2", "POST", file);
	    Assertions.assertEquals(200, response.getCode());
	    System.out.println(response.getBody());
	    JSONObject json = JSON.parseObject(response.getBody());
	    Assertions.assertEquals(Files.size(file), json.getIntValue("size"));
	    Assertions.assertEquals("application/xml", json.getString("contentType"));
	    Path tmp = Paths.get(json.getString("path"));
	    Assertions.assertTrue(Files.exists(tmp));
    }

}
