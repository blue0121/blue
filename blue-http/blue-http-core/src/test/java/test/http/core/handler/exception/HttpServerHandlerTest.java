package test.http.core.handler.exception;

import blue.base.core.util.JsonUtil;
import blue.http.core.exception.HttpServerException;
import blue.http.internal.core.handler.HandlerResponse;
import blue.http.internal.core.handler.exception.ExceptionDispatcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
public class HttpServerHandlerTest {
	private ExceptionDispatcher dispatcher = new ExceptionDispatcher();

	public HttpServerHandlerTest() {
	}

    @Test
	public void test() {
        HandlerResponse response = this.createResponse(new HttpServerException("无效"));
		dispatcher.handleException(response);
	    System.out.println(response.getResult());
	    Assertions.assertEquals(500, response.getHttpStatus().code());
		Error error = JsonUtil.fromString(response.getResult().toString(), Error.class);
		Assertions.assertEquals(500_001, error.code);
		Assertions.assertEquals("发生错误：无效", error.message);
    }

    private HandlerResponse createResponse(Throwable cause) {
	    HandlerResponse response = new HandlerResponse();
	    response.setCause(cause);
	    return response;
    }

}
