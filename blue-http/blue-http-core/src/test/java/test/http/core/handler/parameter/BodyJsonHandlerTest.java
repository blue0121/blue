package test.http.core.handler.parameter;

import blue.base.core.common.ErrorCode;
import blue.base.core.common.ErrorCodeException;
import blue.http.core.annotation.BodyJson;
import blue.http.core.message.HttpRequest;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.parser.RequestParamConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import test.http.core.model.User;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-09
 */
@ExtendWith(MockitoExtension.class)
public class BodyJsonHandlerTest {
	@Mock
	private HttpRequest request;
	@Mock
	private WebSocketRequest webSocketRequest;

	private ParameterDispatcher dispatcher = new ParameterDispatcher();

	public BodyJsonHandlerTest() {
	}

	@Test
	public void testEmpty() {
		RequestParamConfig config = this.createConfig(String.class, "$.name", true, false);
		try {
			dispatcher.handleParam(config, request);
			Assertions.fail();
		} catch (ErrorCodeException e) {
			this.assertException(e, "$.name 不能为空");
		}
	}

	@Test
	public void testString() {
		this.mockRequest("{\"id\":10, \"name\":\"blue\"}");
		RequestParamConfig config = this.createConfig(String.class, "$.name", true, false);
		String result = (String) dispatcher.handleParam(config, request);
		Assertions.assertEquals("blue", result);

		RequestParamConfig config2 = this.createConfig(String.class, "$.name2", true, false);
		try {
			dispatcher.handleParam(config2, request);
			Assertions.fail();
		} catch (ErrorCodeException e) {
			this.assertException(e, "$.name2 不能为空");
		}
	}

	@Test
	public void testInt() {
		this.mockRequest("{\"id\":10, \"name\":\"blue\"}");
		RequestParamConfig config = this.createConfig(Integer.class, "$.id", true,false);
		Integer result = (Integer) dispatcher.handleParam(config, request);
		Assertions.assertEquals(10, result);
	}

	@Test
	public void testObject() {
		this.mockRequest("{\"id\":10, \"name\":\"blue\"}");
		RequestParamConfig config = this.createConfig(User.class, "", true,false);
		User user = (User) dispatcher.handleParam(config, request);
		Assertions.assertEquals(10, user.getId());
		Assertions.assertEquals("blue", user.getName());
	}

	@Test
	public void testJson() {
		this.mockRequest("{\"id\": 20,\"content\":{\"id\":10, \"name\":\"blue\"}}");
		RequestParamConfig config = this.createConfig(User.class, "$.content", true,false);
		User user = (User) dispatcher.handleParam(config, request);
		Assertions.assertEquals(10, user.getId());
		Assertions.assertEquals("blue", user.getName());
	}

	private void mockRequest(String content) {
		Mockito.when(request.getContent()).thenReturn(content);
	}

	private RequestParamConfig createConfig(Class<?> paramClazz, String jsonPath, boolean required, boolean validated) {
		RequestParamConfig config = new RequestParamConfig();
		config.setName(jsonPath);
		config.setParamClazz(paramClazz);
		config.setParamAnnotationClazz(BodyJson.class);
		config.setParamAnnotationValue(jsonPath);
		config.setParamAnnotationRequired(required);
		config.setValidated(validated);
		return config;
	}

	private void assertException(ErrorCodeException e, String message) {
		e.printStackTrace();
		Assertions.assertEquals(400, e.getHttpStatus());
		Assertions.assertEquals(ErrorCode.REQUIRED.getCode(), e.getCode());
		Assertions.assertEquals(message, e.getMessage());
	}
}
