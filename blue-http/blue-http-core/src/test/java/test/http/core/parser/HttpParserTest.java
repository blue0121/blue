package test.http.core.parser;

import blue.http.core.annotation.Charset;
import blue.http.core.annotation.ContentType;
import blue.http.core.annotation.HttpMethod;
import blue.http.internal.core.parser.HttpConfigCache;
import blue.http.internal.core.parser.HttpMethodResult;
import blue.http.internal.core.parser.HttpUrlKey;
import blue.http.internal.core.parser.ParserFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test.http.core.controller.EchoController;
import test.http.core.controller.HelloController;

/**
 * @author Jin Zheng
 * @since 2019-12-29
 */
public class HttpParserTest {
	private ParserFactory factory = new ParserFactory();
	private HttpConfigCache cache = factory.getHttpConfigCache();

	public HttpParserTest() {
	}

	@Test
	public void test() {
		factory.parse(new EchoController(), EchoController.class);
		factory.parse(new HelloController(), HelloController.class);

		HttpUrlKey key = new HttpUrlKey("/hello", HttpMethod.POST);
		HttpMethodResult result = cache.getConfig(key);
		System.out.println(result);
		Assertions.assertNotNull(result);
		Assertions.assertEquals(Charset.UTF_8, result.getCharset());
		Assertions.assertEquals(ContentType.AUTO, result.getContentType());

		Assertions.assertTrue(cache.contains(new HttpUrlKey("/echo", HttpMethod.GET)));
		Assertions.assertTrue(cache.contains(new HttpUrlKey("/echo", HttpMethod.POST)));
		Assertions.assertTrue(cache.contains(new HttpUrlKey("/echo", HttpMethod.DELETE)));
		Assertions.assertTrue(cache.contains(new HttpUrlKey("/echo", HttpMethod.PUT)));
	}

}
