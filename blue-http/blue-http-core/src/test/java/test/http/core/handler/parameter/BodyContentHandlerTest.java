package test.http.core.handler.parameter;

import blue.http.core.annotation.BodyContent;
import blue.http.core.message.HttpRequest;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.parser.RequestParamConfig;
import blue.validation.core.group.GetGroup;
import jakarta.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import test.http.core.model.User;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-09
 */
@ExtendWith(MockitoExtension.class)
public class BodyContentHandlerTest {
	@Mock
	private HttpRequest request;
	@Mock
	private WebSocketRequest webSocketRequest;

	private ParameterDispatcher dispatcher = new ParameterDispatcher();

	public BodyContentHandlerTest() {
	}

	@Test
	public void testString() {
		String content = "hello";
		this.mockRequest(content);
		RequestParamConfig config = this.createConfig(String.class, false);
		Object result = dispatcher.handleParam(config, request);
		Assertions.assertEquals(content, result);
	}

	@Test
	public void testInt() {
		this.mockRequest("10");
		RequestParamConfig config = this.createConfig(Integer.class, false);
		Integer result = (Integer) dispatcher.handleParam(config, request);
		Assertions.assertEquals(10, result.intValue());
	}

	@Test
	public void testJsonWithoutValidated() {
		this.mockRequest("{\"id\":10, \"name\":\"blue\"}");
		RequestParamConfig config = this.createConfig(User.class, false);
		User user1 = (User) dispatcher.handleParam(config, request);
		Assertions.assertEquals(10, user1.getId().intValue());
		Assertions.assertEquals("blue", user1.getName());

		this.mockRequest("{\"id\":10}");
		User user2 = (User) dispatcher.handleParam(config, request);
		Assertions.assertEquals(10, user2.getId().intValue());
		Assertions.assertNull(user2.getName());
	}

	@Test
	public void testJsonWithValidated() {
		this.mockRequest("{\"id\":10, \"name\":\"blue\"}");
		RequestParamConfig config = this.createConfig(User.class, true);
		config.setValidatedGroups(new Class[]{GetGroup.class});
		User user1 = (User) dispatcher.handleParam(config, request);
		Assertions.assertEquals(10, user1.getId().intValue());
		Assertions.assertEquals("blue", user1.getName());

		this.mockRequest("{\"id\":10}");
		try {
			dispatcher.handleParam(config, request);
			Assertions.fail();
		} catch (ValidationException e) {
			e.printStackTrace();
			Assertions.assertEquals("Must be required", e.getMessage());
		}
	}

	@Test
	public void testWebSocketString() {
		String content = "hello";
		Mockito.when(webSocketRequest.getContent()).thenReturn(content);
		RequestParamConfig config = this.createConfig(String.class, false);
		Object result = dispatcher.handleParam(config, webSocketRequest);
		Assertions.assertEquals(content, result);
	}

	private void mockRequest(String content) {
		Mockito.when(request.getContent()).thenReturn(content);
	}

	private RequestParamConfig createConfig(Class<?> paramClazz, boolean validated) {
		RequestParamConfig config = new RequestParamConfig();
		config.setParamClazz(paramClazz);
		config.setParamAnnotationClazz(BodyContent.class);
		config.setValidated(validated);
		return config;
	}

}
