package test.http.core.util;

import blue.http.internal.core.util.HttpServerUtil;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-19
 */
public class HttpServerUtilTest {
	public HttpServerUtilTest() {
	}

	@Test
	public void testSetContentType() {
		HttpResponse response = Mockito.mock(HttpResponse.class);
		HttpHeaders headers = Mockito.mock(HttpHeaders.class);
		Mockito.when(response.headers()).thenReturn(headers);
		ArgumentCaptor<String> value = ArgumentCaptor.forClass(String.class);
		HttpServerUtil.setContentType(response, "file.jpg");
		Mockito.verify(headers).set(Mockito.any(CharSequence.class), value.capture());
		Assertions.assertEquals("image/jpeg", value.getValue());
	}

}
