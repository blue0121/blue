package test.http.core.controller;

import blue.http.core.annotation.Http;
import blue.http.core.annotation.HttpMethod;
import blue.http.core.annotation.PathVariable;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-27
 */
@Http(url = "/test/{name}", method = HttpMethod.GET)
public interface TestController {

	String test(@PathVariable("name") String name);

}
