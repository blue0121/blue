package test.http.core.message;

import blue.http.core.message.Download;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-02
 */
public class DownloadTest {
	private String text = "hello world";
	private String fiilename = "download.txt";

	public DownloadTest() {
	}

	@Test
	public void testMemory() {
        Download download = Download.builder().setFilename(fiilename).handle(Download.Type.MEMORY, os -> {
	        try {
		        os.write(text.getBytes(StandardCharsets.UTF_8));
	        }
	        catch (IOException e) {
		        e.printStackTrace();
	        }
        }).build();
		Assertions.assertEquals(Download.Type.MEMORY, download.getType());
		Assertions.assertEquals(fiilename, download.getFilename());
		Assertions.assertEquals(text, new String(download.getMemory()));
    }

    @Test
    public void testFile() throws IOException {
		Download download = Download.builder().setFilename(fiilename).handle(Download.Type.FILE, os -> {
			try {
				os.write(text.getBytes(StandardCharsets.UTF_8));
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}).build();
	    Assertions.assertEquals(Download.Type.FILE, download.getType());
	    Assertions.assertEquals(fiilename, download.getFilename());
	    Path path = download.getPath();
	    Assertions.assertEquals(text, Files.readString(path));
	    download.delete();
    }

}
