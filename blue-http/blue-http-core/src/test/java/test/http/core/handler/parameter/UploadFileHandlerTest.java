package test.http.core.handler.parameter;

import blue.base.core.common.ErrorCode;
import blue.base.core.common.ErrorCodeException;
import blue.http.core.message.HttpRequest;
import blue.http.core.message.UploadFile;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.parser.RequestParamConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-09
 */
@ExtendWith(MockitoExtension.class)
public class UploadFileHandlerTest {
	@Mock
	private HttpRequest request;
	@Mock
	private WebSocketRequest webSocketRequest;
	@Mock
	private UploadFile file;

	private ParameterDispatcher dispatcher = new ParameterDispatcher();

	public UploadFileHandlerTest() {
	}

	@Test
	public void testEmpty() {
		RequestParamConfig config = this.createConfig("name", false);
		String result = (String) dispatcher.handleParam(config, request);
		Assertions.assertNull(result);
	}

	@Test
	public void testFile() {
		this.mockRequest();
		RequestParamConfig config = this.createConfig("name", true);
		UploadFile result = (UploadFile) dispatcher.handleParam(config, request);
		Assertions.assertNotNull(result);
	}

	@Test
	public void testWebSocket() {
		RequestParamConfig config = this.createConfig("name", true);
		Assertions.assertNull(dispatcher.handleParam(config, webSocketRequest));
	}

	private void mockRequest() {
		Mockito.when(request.getFile()).thenReturn(file);
	}

	private RequestParamConfig createConfig(String name, boolean required) {
		RequestParamConfig config = new RequestParamConfig();
		config.setName(name);
		config.setParamClazz(UploadFile.class);
		return config;
	}

	private void assertException(ErrorCodeException e, String message) {
		e.printStackTrace();
		Assertions.assertEquals(400, e.getHttpStatus());
		Assertions.assertEquals(ErrorCode.REQUIRED.getCode(), e.getCode());
		Assertions.assertEquals(message, e.getMessage());
	}

}
