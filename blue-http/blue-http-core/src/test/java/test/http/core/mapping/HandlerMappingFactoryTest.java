package test.http.core.mapping;

import blue.http.core.annotation.Charset;
import blue.http.core.annotation.ContentType;
import blue.http.core.annotation.HttpMethod;
import blue.http.core.message.HttpRequest;
import blue.http.internal.core.handler.HandlerChain;
import blue.http.internal.core.mapping.HandlerMappingFactory;
import blue.http.internal.core.parser.HttpMethodResult;
import blue.http.internal.core.parser.ParserFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import test.http.core.controller.EchoController;
import test.http.core.controller.HelloController;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-07
 */
public class HandlerMappingFactoryTest {
	private ParserFactory parserFactory = new ParserFactory();
	private HandlerMappingFactory factory = new HandlerMappingFactory(parserFactory);

	private HttpRequest request;

	public HandlerMappingFactoryTest() {
	}

	@BeforeEach
	public void before() {
		this.request = Mockito.mock(HttpRequest.class);
	}

	@Test
	public void testGetHandlerChain1() {
		parserFactory.parse(new EchoController(), EchoController.class);

		Mockito.when(request.getUrl()).thenReturn("/echo");
		Mockito.when(request.getHttpMethod()).thenReturn(HttpMethod.POST);

		HandlerChain chain = factory.getHandlerChain(request);
		Assertions.assertNotNull(chain);
		HttpMethodResult result = (HttpMethodResult) chain.getHandler();
		Assertions.assertEquals(Charset.UTF_8, result.getCharset());
		Assertions.assertEquals(ContentType.AUTO, result.getContentType());
		Assertions.assertEquals(EchoController.class, result.getJavaBean().getTargetClass());
		Assertions.assertEquals("echo", result.getMethod().getName());
	}

	@Test
	public void testGetHandlerChain2() {
		parserFactory.parse(new HelloController(), HelloController.class);

		Mockito.when(request.getUrl()).thenReturn("/hello/hello2");
		Mockito.when(request.getHttpMethod()).thenReturn(HttpMethod.POST);

		HandlerChain chain = factory.getHandlerChain(request);
		Assertions.assertNotNull(chain);
		HttpMethodResult result = (HttpMethodResult) chain.getHandler();
		Assertions.assertEquals(Charset.UTF_8, result.getCharset());
		Assertions.assertEquals(ContentType.AUTO, result.getContentType());
		Assertions.assertEquals(HelloController.class, result.getJavaBean().getTargetClass());
		Assertions.assertEquals("hello2", result.getMethod().getName());
	}

}
