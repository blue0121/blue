package test.http.core.handler.parameter;

import blue.http.core.message.HttpRequest;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.parser.RequestParamConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-09
 */
@ExtendWith(MockitoExtension.class)
public class WebSocketRequestHandlerTest {
	@Mock
	private HttpRequest httpRequest;
	@Mock
	private WebSocketRequest request;

	private ParameterDispatcher dispatcher = new ParameterDispatcher();

	public WebSocketRequestHandlerTest() {
	}

	@Test
	public void testHttpRequest() {
		RequestParamConfig config = this.createConfig("name", true);
		WebSocketRequest result = (WebSocketRequest) dispatcher.handleParam(config, httpRequest);
		Assertions.assertNull(result);
	}

	@Test
	public void testWebSocketRequest() {
		RequestParamConfig config = this.createConfig("name", true);
		WebSocketRequest result = (WebSocketRequest) dispatcher.handleParam(config, request);
		Assertions.assertNotNull(result);
	}

	private RequestParamConfig createConfig(String name, boolean required) {
		RequestParamConfig config = new RequestParamConfig();
		config.setName(name);
		config.setParamClazz(WebSocketRequest.class);
		return config;
	}

}
