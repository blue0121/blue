package test.http.core.controller;

import blue.base.core.util.JsonUtil;
import blue.http.core.annotation.BodyJson;
import blue.http.core.annotation.Http;
import blue.http.core.annotation.HttpMethod;
import blue.http.core.annotation.Validated;
import jakarta.validation.ValidationException;
import org.springframework.stereotype.Controller;
import test.http.core.model.User;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-02
 */
@Controller
@Http(url = "/echo")
public class EchoController {
	public EchoController() {
	}

	public String echo(String text) {
		if (text == null || text.isEmpty()) {
			throw new ValidationException("text is empty");
		}

		return text;
	}

	@Http(url = "/validate", method = HttpMethod.POST)
	public void validate(@BodyJson(jsonPath = "$.user") @Validated User user) {
		System.out.println(JsonUtil.output(user));
		System.out.println("OK");
	}

}
