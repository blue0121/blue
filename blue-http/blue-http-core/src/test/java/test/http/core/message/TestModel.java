package test.http.core.message;

/**
 * @author Jin Zheng
 * @since 2020-07-25
 */
public class TestModel {
	private String name;
	private int password;

	public TestModel() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPassword() {
		return password;
	}

	public void setPassword(int password) {
		this.password = password;
	}
}
