package test.http.core.controller;

import blue.http.core.annotation.Http;
import blue.http.core.annotation.HttpMethod;
import blue.http.core.message.View;
import org.springframework.stereotype.Controller;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-19
 */
@Controller
@Http(url = "/ftl", method = HttpMethod.GET)
public class FtlController {
	public FtlController() {
	}

	public View index() {
		return View.builder().setView("index").put("name", "blue").build();
	}

}
