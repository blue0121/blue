package test.http.core;

import blue.http.core.HttpServer;
import blue.http.core.options.HttpOptions;
import blue.http.core.options.ServerOptions;
import test.http.core.controller.DefaultTestController;
import test.http.core.controller.EchoController;
import test.http.core.controller.HelloController;
import test.http.core.controller.UploadController;

import java.util.concurrent.Executors;

/**
 * @author Jin Zheng
 * @since 2021-08-18
 */
public class HttpServerMain {
	public HttpServerMain() {
	}

    public static void main(String[] args) throws Exception {
	    ServerOptions serverOptions = new ServerOptions();
		serverOptions.setPort(10000)
				.setStartup(System.currentTimeMillis())
				.setExecutor(Executors.newFixedThreadPool(10))
				.setIoThread(2);
        HttpOptions httpOptions = new HttpOptions();
        httpOptions.setContextPath("/api")
                .setViewRoot("/view");
        HttpServer server = HttpServer.builder()
		        .setServerOptions(serverOptions)
                .setHttpOptions(httpOptions)
		        .addHandler(new EchoController(), EchoController.class)
		        .addHandler(new DefaultTestController(), DefaultTestController.class)
		        .addHandler(new HelloController(), HelloController.class)
		        .addHandler(new UploadController(), UploadController.class)
		        .build();
        server.start();
    }
}
