package test.http.core.handler;

import blue.base.core.reflect.BeanMethod;
import blue.base.core.reflect.JavaBean;
import blue.base.core.reflect.JavaBeanCache;
import blue.http.core.exception.HttpServerException;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.handler.WebSocketHandler;
import blue.http.internal.core.handler.WebSocketHandlerChain;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.parser.RequestParamConfig;
import blue.http.internal.core.parser.WebSocketMethodResult;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
@ExtendWith(MockitoExtension.class)
public class WebSocketHandlerTest {
	@Mock
	private ParameterDispatcher dispatcher;
	@Mock
	private WebSocketRequest request;
	@Mock
	private WebSocketMethodResult methodResult;
	@Mock
	private WebSocketHandlerChain chain;

	private WebSocketHandler handler;

	public WebSocketHandlerTest() {
	}

	@BeforeEach
	public void beforeEach() {
		Mockito.when(chain.getHandler()).thenReturn(methodResult);
		handler = new WebSocketHandler(dispatcher);
	}

	@Test
	public void testHandleFailure() throws Exception {
		var response = handler.handle(request, chain);
		Assertions.assertNotNull(response);
		Mockito.verify(chain).applyPreHandle(Mockito.any(), Mockito.any());
		Mockito.verify(chain).triggerAfterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(dispatcher, Mockito.never()).handleParam(Mockito.any(), Mockito.any(WebSocketRequest.class));
		Mockito.verify(chain, Mockito.never()).applyPostHandle(Mockito.any(), Mockito.any());
	}

	@Test
	public void testHandleSuccess() throws Exception {
		this.mockMethodResult("test");
		Mockito.when(chain.applyPreHandle(Mockito.any(), Mockito.any())).thenReturn(true);
		var response = handler.handle(request, chain);
		Assertions.assertNotNull(response);
		Assertions.assertNull(response.getCause());
		Assertions.assertEquals("test", response.getResult());

		Mockito.verify(chain).applyPreHandle(Mockito.any(), Mockito.any());
		Mockito.verify(chain).triggerAfterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(dispatcher).handleParam(Mockito.any(), Mockito.any(WebSocketRequest.class));
		Mockito.verify(chain).applyPostHandle(Mockito.any(), Mockito.any());
	}

	@Test
	public void testHandleException() throws Exception {
		this.mockMethodResult("testException");
		Mockito.when(chain.applyPreHandle(Mockito.any(), Mockito.any())).thenReturn(true);
		var response = handler.handle(request, chain);
		Assertions.assertNotNull(response);
		Assertions.assertNotNull(response.getCause());
		Assertions.assertEquals(HttpServerException.class, response.getCause().getClass());

		Mockito.verify(chain).applyPreHandle(Mockito.any(), Mockito.any());
		Mockito.verify(chain).triggerAfterCompletion(Mockito.any(), Mockito.any(), Mockito.any());
		Mockito.verify(dispatcher).handleParam(Mockito.any(), Mockito.any(WebSocketRequest.class));
		Mockito.verify(chain).applyPostHandle(Mockito.any(), Mockito.any());
	}

	private void mockMethodResult(String methodName) {
		JavaBean bean = JavaBeanCache.getJavaBean(new TestHandler(), TestHandler.class);
		var methodList = bean.getAllMethods();
		BeanMethod method = null;
		for (var m : methodList) {
			if (m.getName().equals(methodName)) {
				method = m;
				break;
			}
		}
		if (method == null) {
			throw new UnsupportedOperationException("Not Found method: " + methodName);
		}

		Mockito.when(methodResult.getMethod()).thenReturn(method);
		List<RequestParamConfig> paramConfigList = new ArrayList<>();
		for (var param : method.getParamList()) {
			RequestParamConfig config = new RequestParamConfig();
			config.setName(param.getName());
			config.setParamClazz(param.getParamClass());
			paramConfigList.add(config);
		}
		Mockito.when(methodResult.getParamList()).thenReturn(paramConfigList);
		Mockito.when(dispatcher.handleParam(Mockito.any(), Mockito.any(WebSocketRequest.class))).thenReturn(methodName);
	}

}
