package test.http.core.message;

import blue.http.core.message.View;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-02
 */
public class ViewTest {
	private Map<String, Object> map = Map.of("name", "blue", "redirect", "http://www.baidu.com");

	public ViewTest() {
	}

	@Test
	public void testGetView() {
		View view = View.builder().setView("view.tpl").putAll(map).build();
		Assertions.assertNotNull(view);
		Assertions.assertFalse(view.isRedirect());
		Assertions.assertEquals("view.tpl", view.getView());
		var model = view.getModel();
		Assertions.assertEquals(2, model.size());
		Assertions.assertEquals("blue", model.get("name"));
		Assertions.assertEquals("http://www.baidu.com", model.get("redirect"));
	}

	@Test
	public void testGetRedirect() {
		View view = View.builder().setRedirect("http://www.web.com").putAll(map).build();
		Assertions.assertNotNull(view);
		Assertions.assertTrue(view.isRedirect());
		var model = view.getModel();
		Assertions.assertEquals(2, model.size());
		Assertions.assertEquals("blue", model.get("name"));
		Assertions.assertEquals("http://www.baidu.com", model.get("redirect"));
		String redirct = view.getRedirect();
		URI uri = URI.create(redirct);
		Assertions.assertEquals("www.web.com", uri.getHost());
		String query = uri.getRawQuery();
		Assertions.assertTrue(query.contains("redirect=http%3A%2F%2Fwww.baidu.com"));
		Assertions.assertTrue(query.contains("name=blue"));
	}

}
