package test.http.core.it;

import blue.http.core.annotation.BodyContent;
import blue.http.core.annotation.Http;
import blue.http.core.annotation.HttpMethod;
import blue.http.core.annotation.Validated;
import blue.validation.core.group.GetGroup;
import blue.validation.core.group.SaveGroup;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-24
 */
@Http(url = "/user")
public class UserController {
	private static Logger logger = LoggerFactory.getLogger(UserController.class);

	private UserService userService = UserService.INSTANCE;

	public UserController() {
	}

	@Http(url = "/reg", method = HttpMethod.POST)
	public void reg(@BodyContent @Validated(SaveGroup.class) User user) {
		userService.reg(user);
	}

	@Http(url = "/login", method = HttpMethod.POST)
	public Object login(@BodyContent @Validated(GetGroup.class) User user) {
		String token = userService.login(user);
		JSONObject json = new JSONObject();
		json.put("token", token);
		return json;
	}

	@Http(url = "/list_all", method = HttpMethod.GET)
	public Object listAll() {
		var list = userService.listAll();
		return list;
	}

}
