package test.http.core.message;

import blue.http.core.annotation.HttpMethod;
import blue.http.core.plugin.SessionProvider;
import blue.http.internal.core.message.DefaultHttpRequest;
import blue.http.internal.core.plugin.MemorySessionProvider;
import io.netty.channel.ChannelId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-04
 */
public class HttpRequestTest {
	private String ip = "localhost";

	public HttpRequestTest() {
	}

	private DefaultHttpRequest getRequest() {
		ChannelId id = Mockito.mock(ChannelId.class);
		SessionProvider sessionProvider = new MemorySessionProvider();
		DefaultHttpRequest request = new DefaultHttpRequest(HttpMethod.GET, ip, id, sessionProvider);
		return request;
	}

	@Test
	public void testQueryString() {
		DefaultHttpRequest request = this.getRequest();
		String uri = "/api/test?name=blue&password=123456";
		String path = "/api";
		request.parseUri(uri, path);
		Assertions.assertEquals("localhost", request.getIp());
		Assertions.assertEquals("/api", request.getContextPath());
		Assertions.assertEquals("name=blue&password=123456", request.getQueryString());
		Assertions.assertEquals("blue", request.getQueryString("name"));
		Assertions.assertEquals("123456", request.getQueryString("password"));
		Assertions.assertEquals(123456, request.getQueryInt("password", 1));
		Assertions.assertEquals(1, request.getQueryInt("name", 1));
		TestModel model = request.getQueryObject(TestModel.class);
		Assertions.assertNotNull(model);
		Assertions.assertEquals("blue", model.getName());
		Assertions.assertEquals(123456, model.getPassword());
	}

	@Test
	public void testPost() {
		DefaultHttpRequest request = this.getRequest();
		request.putPost("name", "blue");
		request.putPost("password", "123456");
		Assertions.assertEquals("blue", request.getPost("name"));
		Assertions.assertEquals("123456", request.getPost("password"));
		Assertions.assertEquals(123456, request.getPostInt("password", 1));
		Assertions.assertEquals(1, request.getPostInt("name", 1));
		TestModel model = request.getPostObject(TestModel.class);
		Assertions.assertNotNull(model);
		Assertions.assertEquals("blue", model.getName());
		Assertions.assertEquals(123456, model.getPassword());
	}

	@Test
	public void testBody() {
		DefaultHttpRequest request = this.getRequest();
		request.setContent("{\"name\":\"blue\",\"password\":123456}");
		TestModel model = request.getContentJson(TestModel.class);
		Assertions.assertNotNull(model);
		Assertions.assertEquals("blue", model.getName());
		Assertions.assertEquals(123456, model.getPassword());
	}

}
