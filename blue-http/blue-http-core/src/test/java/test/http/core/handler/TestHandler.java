package test.http.core.handler;

import blue.http.core.exception.HttpServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-12
 */
public class TestHandler {
	private static Logger logger = LoggerFactory.getLogger(TestHandler.class);

	public TestHandler() {
	}


	public String test(String name) {
		logger.info("name: {}", name);
		return name;
	}

	public void testException(String name) {
		logger.info("name: {}", name);
		throw new HttpServerException(name);
	}

}
