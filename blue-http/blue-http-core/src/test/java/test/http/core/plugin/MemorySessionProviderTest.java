package test.http.core.plugin;

import blue.base.core.util.WaitUtil;
import blue.http.core.message.Session;
import blue.http.internal.core.plugin.MemorySessionProvider;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;

/**
 * @author Jin Zheng
 * @since 1.0 2021-09-03
 */
public class MemorySessionProviderTest {
	private String token = "token123";
	private String key = "key123";

	public MemorySessionProviderTest() {
	}

	@Test
	public void testCreateSession() {
		MemorySessionProvider provider = new MemorySessionProvider(Duration.ofMillis(10));
		Session session = provider.getSession(token);
		session.setKey(key);
		WaitUtil.sleep(20);
		session = provider.getSession(token);
		Assertions.assertNull(session.getKey());
	}

	@Test
	public void testDeleteSession() {
		MemorySessionProvider provider = new MemorySessionProvider(Duration.ofMillis(10));
		Session session = provider.getSession(token);
		session.setKey(key);
		session.delete();
		session = provider.getSession(token);
		Assertions.assertNull(session.getKey());
	}

}
