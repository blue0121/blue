package blue.http.internal.core.handler.exception;

import blue.base.core.common.ErrorCodeException;
import blue.http.internal.core.handler.HandlerResponse;
import io.netty.handler.codec.http.HttpResponseStatus;

/**
 * @author Jin Zheng
 * @since 2020-01-27
 */
public class ErrorCodeExceptionHandler implements ExceptionHandler {
	@Override
	public boolean accepted(Throwable cause) {
		return cause instanceof ErrorCodeException;
	}

	@Override
	public void handle(HandlerResponse response) {
		ErrorCodeException ex = (ErrorCodeException) response.getCause();
		response.setResult(ex.toJsonString());
		response.setHttpStatus(HttpResponseStatus.valueOf(ex.getHttpStatus()));
	}
}
