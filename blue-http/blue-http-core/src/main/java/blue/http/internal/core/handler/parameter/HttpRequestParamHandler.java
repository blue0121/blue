package blue.http.internal.core.handler.parameter;

import blue.http.core.message.HttpRequest;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-22
 */
public class HttpRequestParamHandler implements ParamHandler {
	public HttpRequestParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		if (!(request instanceof HttpRequest))
			return null;

		return request;
	}
}
