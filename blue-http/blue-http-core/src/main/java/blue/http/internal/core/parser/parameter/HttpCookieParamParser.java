package blue.http.internal.core.parser.parameter;

import blue.http.core.annotation.HttpCookie;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-27
 */
public class HttpCookieParamParser implements ParamParser<HttpCookie> {
	public HttpCookieParamParser() {
	}

	@Override
	public void parse(RequestParamConfig config, HttpCookie annotation) {
		config.setParamAnnotationClazz(annotation.annotationType());
		config.setParamAnnotationValue(annotation.value());
		config.setParamAnnotationRequired(annotation.required());
	}
}
