package blue.http.internal.core.handler.parameter;

import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-22
 */
public class WebSocketRequestParamHandler implements ParamHandler {
	public WebSocketRequestParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		if (!(request instanceof WebSocketRequest))
			return null;

		return request;
	}
}
