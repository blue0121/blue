package blue.http.internal.core.handler.parameter;

import blue.base.core.common.ErrorCode;
import blue.http.core.message.HttpRequest;
import blue.http.core.message.UploadFile;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 2021-01-24
 */
public class MultipartParamHandler implements ParamHandler {
	public MultipartParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		if (!(request instanceof HttpRequest))
			return null;

		UploadFile param = ((HttpRequest)request).getFile(config.getParamAnnotationValue());
		if (param == null && config.isParamAnnotationRequired()) {
			throw ErrorCode.REQUIRED.newException(config.getName());
		}

		return param;
	}
}
