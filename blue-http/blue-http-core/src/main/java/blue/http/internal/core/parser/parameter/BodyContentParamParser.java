package blue.http.internal.core.parser.parameter;

import blue.http.core.annotation.BodyContent;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-27
 */
public class BodyContentParamParser implements ParamParser<BodyContent> {
	public BodyContentParamParser() {
	}

	@Override
	public void parse(RequestParamConfig config, BodyContent annotation) {
		config.setParamAnnotationClazz(annotation.annotationType());
	}
}
