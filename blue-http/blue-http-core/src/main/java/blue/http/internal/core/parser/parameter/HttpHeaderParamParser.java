package blue.http.internal.core.parser.parameter;

import blue.http.core.annotation.HttpHeader;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-27
 */
public class HttpHeaderParamParser implements ParamParser<HttpHeader> {
	public HttpHeaderParamParser() {
	}

	@Override
	public void parse(RequestParamConfig config, HttpHeader annotation) {
		config.setParamAnnotationClazz(annotation.annotationType());
		config.setParamAnnotationValue(annotation.value());
		config.setParamAnnotationRequired(annotation.required());
	}
}
