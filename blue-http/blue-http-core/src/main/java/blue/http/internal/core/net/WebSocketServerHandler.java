package blue.http.internal.core.net;

import blue.base.core.common.ErrorCode;
import blue.http.core.message.WebSocketRequest;
import blue.http.core.message.WebSocketResponse;
import blue.http.core.options.WebSocketOptions;
import blue.http.internal.core.handler.HandlerChain;
import blue.http.internal.core.handler.HandlerFactory;
import blue.http.internal.core.mapping.HandlerMappingFactory;
import blue.http.internal.core.message.DefaultWebSocketRequest;
import blue.http.internal.core.message.WebSocketMessage;
import blue.http.internal.core.util.WebSocketServerUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;

/**
 * @author Jin Zheng
 * @since 2020-02-03
 */
public class WebSocketServerHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {
	private static Logger logger = LoggerFactory.getLogger(WebSocketServerHandler.class);

	private final ExecutorService executor;
	private final WebSocketOptions webSocketOptions;
	private final HandlerMappingFactory handlerMappingFactory;
	private final HandlerFactory handlerFactory;

	public WebSocketServerHandler(DefaultHttpServerBuilder builder) {
		this.executor = builder.getServerOptions().getExecutor();
		this.webSocketOptions = builder.getWebSocketOptions();
		this.handlerMappingFactory = builder.getHandlerMappingFactory();
		this.handlerFactory = builder.getHandlerFactory();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame frame) throws Exception {
		Channel ch = ctx.channel();
		WebSocketMessage message = WebSocketServerUtil.getWebSocketMessage(ch, frame.text());
		if (message == null) {
			return;
		}

		WebSocketRequest request = DefaultWebSocketRequest.from(message);
		HandlerChain chain = handlerMappingFactory.getHandlerChain(request);
		if (chain == null) {
			WebSocketServerUtil.sendText(ch, message, null, ErrorCode.NOT_FOUND);
			return;
		}
		executor.execute(() ->
		{
			WebSocketResponse response = handlerFactory.handle(request, chain);
			WebSocketServerUtil.sendText(ch, message, response);
		});

	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		super.channelInactive(ctx);
		Channel ch = ctx.channel();
		//router.remove(ch);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		super.exceptionCaught(ctx, cause);
		Channel ch = ctx.channel();
		ChannelId id = ch.id();
		logger.error("WebSocket client raised exception，disconnected: " + ch.remoteAddress() + "，id=" + id, cause);
		ch.close();
	}

}
