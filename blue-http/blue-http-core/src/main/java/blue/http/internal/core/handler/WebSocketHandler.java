package blue.http.internal.core.handler;

import blue.base.core.reflect.BeanMethod;
import blue.http.core.message.WebSocketRequest;
import blue.http.core.message.WebSocketResponse;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.message.DefaultWebSocketResponse;
import blue.http.internal.core.parser.RequestParamConfig;
import blue.http.internal.core.parser.WebSocketMethodResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-07
 */
public class WebSocketHandler implements Handler<WebSocketRequest, WebSocketResponse> {
	private static Logger logger = LoggerFactory.getLogger(WebSocketHandler.class);

	private ParameterDispatcher parameterDispatcher;

	public WebSocketHandler(ParameterDispatcher parameterDispatcher) {
		this.parameterDispatcher = parameterDispatcher;
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public HandlerResponse handle(WebSocketRequest request, HandlerChain chain) {
		WebSocketMethodResult result = (WebSocketMethodResult) chain.getHandler();
		DefaultWebSocketResponse response = new DefaultWebSocketResponse();
		Exception ex = null;
		try {
			if (!chain.applyPreHandle(request, response)) {
				return HandlerResponse.from(response);
			}
			this.invoke(request, response, result);
			chain.applyPostHandle(request, response);
		} catch (Exception e) {
			ex = e;
		} finally {
			chain.triggerAfterCompletion(request, response, ex);
		}
		return HandlerResponse.from(response);
	}

	@Override
	public WebSocketResponse response(HandlerResponse response) {
		WebSocketResponse resp = (WebSocketResponse) response.getResponse();
		resp.setResult(response.getResult());

		return resp;
	}

	private void invoke(WebSocketRequest request, DefaultWebSocketResponse response, WebSocketMethodResult result) {
		List<RequestParamConfig> configList = result.getParamList();
		Object[] params = new Object[configList.size()];
		int i = 0;
		for (var config : configList) {
			params[i] = parameterDispatcher.handleParam(config, request);
			i++;
		}

		BeanMethod method = result.getMethod();
		try {
			Object rs = method.invoke(null, params);
			response.setResult(rs);
		} catch (Throwable e) {
			e.printStackTrace();
			if (e instanceof InvocationTargetException) {
				e = e.getCause();
			}

			response.setCause(e);
		}
	}

}
