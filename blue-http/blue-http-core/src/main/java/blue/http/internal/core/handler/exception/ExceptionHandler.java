package blue.http.internal.core.handler.exception;

import blue.http.internal.core.handler.HandlerResponse;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-07
 */
public interface ExceptionHandler {

	/**
	 * 是否接受
	 *
	 * @param request
	 * @return
	 */
	boolean accepted(Throwable cause);

	/**
	 * 处理响应对象
	 *
	 * @param response
	 */
	void handle(HandlerResponse response);
}
