package blue.http.internal.core.handler.parameter;

import blue.base.core.common.ErrorCode;
import blue.http.core.message.HttpRequest;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 2021-01-24
 */
public class HttpHeaderParamHandler implements ParamHandler {
	public HttpHeaderParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		if (!(request instanceof HttpRequest))
			return null;

		String param = ((HttpRequest)request).getHeader(config.getParamAnnotationValue());
		if (param == null || param.isEmpty()) {
			if (config.isParamAnnotationRequired()) {
				throw ErrorCode.REQUIRED.newException(config.getName());
			}

			return null;
		}

		return ParamHandlerUtil.convert(config, param);
	}
}
