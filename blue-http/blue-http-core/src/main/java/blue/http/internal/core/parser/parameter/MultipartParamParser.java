package blue.http.internal.core.parser.parameter;

import blue.http.core.annotation.Multipart;
import blue.http.core.message.UploadFile;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-27
 */
public class MultipartParamParser implements ParamParser<Multipart> {
	public MultipartParamParser() {
	}

	@Override
	public void parse(RequestParamConfig config, Multipart annotation) {
		if (!UploadFile.class.isAssignableFrom(config.getParamClazz())) {
			throw new IllegalArgumentException("@Multipart param type must be UploadFile class, name: " + config.getName());
		}

		config.setParamAnnotationClazz(annotation.annotationType());
		config.setParamAnnotationValue(annotation.value());
		config.setParamAnnotationRequired(annotation.required());
	}
}
