package blue.http.internal.core.net.render;

import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-12
 */
public interface Render {

	/**
	 * 页面渲染
	 *
	 * @param view
	 * @param param
	 * @return
	 */
	String render(String view, Map<String, Object> param);

}
