package blue.http.internal.core.message;

import blue.http.core.message.View;

import java.util.Map;

/**
 * @author zhengjin
 * @since 1.0 2018年03月16日
 */
public class DefaultView implements View {
	private String redirect;
	private String view;
	private Map<String, Object> model;

	public DefaultView() {
	}

	@Override
	public boolean isRedirect() {
		return redirect != null && !redirect.isEmpty();
	}

	@Override
	public String getRedirect() {
		return redirect;
	}

	@Override
	public String getView() {
		return view;
	}

	@Override
	public Map<String, Object> getModel() {
		return model;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public void setView(String view) {
		this.view = view;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}
}
