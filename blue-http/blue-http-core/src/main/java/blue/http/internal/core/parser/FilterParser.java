package blue.http.internal.core.parser;

import blue.base.core.reflect.JavaBean;
import blue.http.core.annotation.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-06
 */
public class FilterParser implements Parser{
	private static Logger logger = LoggerFactory.getLogger(AbstractParser.class);

	private Set<Class<?>> parserClassSet = new HashSet<>();
	private final FilterConfigCache configCache;

	public FilterParser(FilterConfigCache filterConfigCache) {
		this.configCache = filterConfigCache;
	}

	@Override
	public void parse(JavaBean bean) {
		if (parserClassSet.contains(bean.getTargetClass())) {
			logger.warn("{} has bean parsed", bean.getTargetClass().getName());
			return;
		}

		Filter annotation = bean.getAnnotation(Filter.class);
		DefaultFilterConfig config = new DefaultFilterConfig();
		config.setFilters(annotation.filters());
		config.setExcludes(annotation.excludes());
		config.setOrder(annotation.order());
		config.setTarget(bean.getTarget());
		configCache.addFilterConfig(config);
		logger.info("Found Filter, name: {}, order: {}, filters: {}, excludes: {}", bean.getName(),
				config.getOrder(), config.getFilters(), config.getExcludes());

		parserClassSet.add(bean.getTargetClass());
	}
}
