package blue.http.internal.core.parser.parameter;

import blue.base.core.reflect.BeanMethod;
import blue.http.core.annotation.BodyContent;
import blue.http.core.annotation.BodyJson;
import blue.http.core.annotation.BodyParam;
import blue.http.core.annotation.HttpCookie;
import blue.http.core.annotation.HttpHeader;
import blue.http.core.annotation.Multipart;
import blue.http.core.annotation.PathVariable;
import blue.http.core.annotation.QueryParam;
import blue.http.core.annotation.Validated;
import blue.http.internal.core.parser.RequestParamConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Jin Zheng
 * @since 2021-01-26
 */
public class ParamParserFactory {
	private static Logger logger = LoggerFactory.getLogger(ParamParserFactory.class);

	private static final Set<Class<? extends Annotation>> paramAnnotationSet = Set.of(BodyContent.class, BodyJson.class,
			BodyParam.class, HttpCookie.class, HttpHeader.class, Multipart.class, PathVariable.class, QueryParam.class);

	private Map<Class<? extends Annotation>, ParamParser> parserMap = new HashMap<>();

	public ParamParserFactory() {
		parserMap.put(BodyContent.class, new BodyContentParamParser());
		parserMap.put(BodyJson.class, new BodyJsonParamParser());
		parserMap.put(BodyParam.class, new BodyParamParamParser());
		parserMap.put(HttpCookie.class, new HttpCookieParamParser());
		parserMap.put(HttpHeader.class, new HttpHeaderParamParser());
		parserMap.put(Multipart.class, new MultipartParamParser());
		parserMap.put(PathVariable.class, new PathVariableParamParser());
		parserMap.put(QueryParam.class, new QueryParamParamParser());
		parserMap.put(Validated.class, new ValidatedParamParser());
	}

	public List<RequestParamConfig> parse(BeanMethod method) {
		List<RequestParamConfig> configList = new ArrayList<>();
		for (var param : method.getParamList()) {
			RequestParamConfig config = new RequestParamConfig();
			configList.add(config);
			config.setName(param.getName());
			config.setParamClazz(param.getParamClass());
			for (var annotation : param.getAnnotations()) {
				if (annotation instanceof Validated) {
					this.parse(config, annotation);
					continue;
				}

				if (!paramAnnotationSet.contains(annotation.annotationType())) {
					continue;
				}

				if (config.hasParamAnnotation()) {
					throw new IllegalArgumentException("Annotation is conflict at " + param.getName());
				}

				this.parse(config, annotation);
			}
		}

		return configList;
	}

	@SuppressWarnings("unchecked")
	public void parse(RequestParamConfig config, Annotation annotation) {
		ParamParser parser = parserMap.get(annotation.annotationType());
		if (parser == null) {
			logger.warn("ParamParser is null, annotation: {}", annotation.annotationType());
			return;
		}
		parser.parse(config, annotation);
	}

}
