package blue.http.internal.core.handler.exception;

import blue.base.core.common.ErrorCode;
import blue.http.internal.core.handler.HandlerResponse;
import com.alibaba.fastjson.JSONException;
import io.netty.handler.codec.http.HttpResponseStatus;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-07
 */
public class JSONExceptionHandler implements ExceptionHandler {
	public JSONExceptionHandler() {
	}

	@Override
	public boolean accepted(Throwable cause) {
		return cause  instanceof JSONException;
	}

	@Override
	public void handle(HandlerResponse response) {
		ErrorCode errorCode = ErrorCode.INVALID_JSON;
		response.setResult(errorCode.toJsonString());
		response.setHttpStatus(HttpResponseStatus.valueOf(errorCode.getHttpStatus()));
	}
}
