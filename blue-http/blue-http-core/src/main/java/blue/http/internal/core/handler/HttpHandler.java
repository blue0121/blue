package blue.http.internal.core.handler;

import blue.base.core.reflect.BeanMethod;
import blue.http.core.message.HttpRequest;
import blue.http.core.message.HttpResponse;
import blue.http.internal.core.handler.parameter.ParameterDispatcher;
import blue.http.internal.core.message.DefaultHttpResponse;
import blue.http.internal.core.parser.HttpMethodResult;
import blue.http.internal.core.parser.RequestParamConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-07
 */
public class HttpHandler implements Handler<HttpRequest, HttpResponse> {
	private static Logger logger = LoggerFactory.getLogger(HttpHandler.class);

	private ParameterDispatcher parameterDispatcher;

	public HttpHandler(ParameterDispatcher parameterDispatcher) {
		this.parameterDispatcher = parameterDispatcher;
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public HandlerResponse handle(HttpRequest request, HandlerChain chain) {
		HttpMethodResult result = (HttpMethodResult) chain.getHandler();
		DefaultHttpResponse response = new DefaultHttpResponse();
		response.setCharset(result.getCharset());
		response.setContentType(result.getContentType());
		Exception ex = null;
		try {
			if (!chain.applyPreHandle(request, response)) {
				return HandlerResponse.from(response);
			}
			this.invoke(request, response, result);
			chain.applyPostHandle(request, response);
		}
		catch (Exception e) {
			ex = e;
		}
		finally {
			chain.triggerAfterCompletion(request, response, ex);
		}
		return HandlerResponse.from(response);
	}

	@Override
	public HttpResponse response(HandlerResponse response) {
		HttpResponse resp = (HttpResponse) response.getResponse();
		resp.setResult(response.getResult());
		resp.setHttpStatus(response.getHttpStatus());
		return resp;
	}

	private void invoke(HttpRequest request, DefaultHttpResponse response, HttpMethodResult result) {
		List<RequestParamConfig> configList = result.getParamList();
		Object[] params = new Object[configList.size()];
		int i = 0;
		for (var config : configList) {
			try {
				params[i] = parameterDispatcher.handleParam(config, request);
			} catch (Exception e) {
				response.setCause(e);
				return;
			}
			i++;
		}

		BeanMethod method = result.getMethod();
		try {
			Object rs = method.invoke(null, params);
			response.setResult(rs);
		} catch (Throwable e) {
			logger.error("Invoke error, ", e);
			if (e instanceof InvocationTargetException) {
				e = e.getCause();
			}

			response.setCause(e);
		}
	}

}
