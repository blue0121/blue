package blue.http.internal.core.handler.exception;

import blue.base.core.common.ErrorCode;
import blue.http.internal.core.handler.HandlerResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import jakarta.validation.ValidationException;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-07
 */
public class ValidationExceptionHandler implements ExceptionHandler {
	public ValidationExceptionHandler() {
	}

	@Override
	public boolean accepted(Throwable cause) {
		return cause instanceof ValidationException;
	}

	@Override
	public void handle(HandlerResponse response) {
		ValidationException ex = (ValidationException) response.getCause();
		ErrorCode errorCode = ErrorCode.INVALID_PARAM;
		response.setResult(errorCode.toJsonString(ex.getMessage()));
		response.setHttpStatus(HttpResponseStatus.valueOf(errorCode.getHttpStatus()));
	}
}
