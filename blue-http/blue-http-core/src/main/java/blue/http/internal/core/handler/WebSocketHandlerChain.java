package blue.http.internal.core.handler;

import blue.base.core.util.AssertUtil;
import blue.http.core.filter.WebSocketFilter;
import blue.http.core.message.WebSocketRequest;
import blue.http.core.message.WebSocketResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 2020-01-05
 */
public class WebSocketHandlerChain implements HandlerChain<WebSocketRequest, WebSocketResponse> {
	private static Logger logger = LoggerFactory.getLogger(WebSocketHandlerChain.class);

	private Object handler;
	private List<WebSocketFilter> filterList = new ArrayList<>();
	private int filterIndex = -1;

	public WebSocketHandlerChain(Object handler) {
		this(handler, null);
	}

	public WebSocketHandlerChain(Object handler, List<WebSocketFilter> filterList) {
		AssertUtil.notNull(handler, "Handler");
		this.handler = handler;
		if (filterList != null && !filterList.isEmpty()) {
			this.filterList.addAll(filterList);
		}
	}

	@Override
	public boolean applyPreHandle(WebSocketRequest request, WebSocketResponse response) throws Exception {
		for (int i = 0; i < filterList.size(); i++) {
			this.filterIndex = i;
			var filter = filterList.get(i);
			if (!filter.preHandle(request, response)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void applyPostHandle(WebSocketRequest request, WebSocketResponse response) throws Exception {
		for (int i = filterList.size() - 1; i >= 0; i--) {
			var filter = filterList.get(i);
			filter.postHandle(request, response);
		}
	}

	@Override
	public void triggerAfterCompletion(WebSocketRequest request, WebSocketResponse response, Exception ex) {
		for (int i = filterIndex; i >= 0; i--) {
			var filter = filterList.get(i);
			try {
				filter.afterCompletion(request, response, ex);
			} catch (Exception e) {
				logger.error("WebSocketFilter.afterCompletion throw exception,", e);
			}
		}
	}

	@Override
	public Object getHandler() {
		return handler;
	}

}
