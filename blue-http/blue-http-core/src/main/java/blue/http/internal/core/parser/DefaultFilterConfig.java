package blue.http.internal.core.parser;

import blue.base.core.util.AssertUtil;
import blue.http.core.parser.FilterConfig;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-05
 */
public class DefaultFilterConfig implements FilterConfig {

	private Object target;
	private List<String> filterList;
	private List<String> excludeList;
	private int order;

	public DefaultFilterConfig() {
	}

	public void setTarget(Object target) {
		AssertUtil.notNull(target, "Object");
		this.target = target;
	}

	public void setFilters(String[] filters) {
		AssertUtil.notEmpty(filters, "Filter url");
		this.filterList = List.of(filters);
	}

	public void setExcludes(String[] excludes) {
		this.excludeList = List.of(excludes);
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public List<String> getFilters() {
		return filterList;
	}

	@Override
	public List<String> getExcludes() {
		return excludeList;
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Override
	public Object getTarget() {
		AssertUtil.notNull(target, "Object");
		return target;
	}

	@Override
	public String toString() {
		return String.format("%s{filters: {}, excludes: {}, order: %d}", target, filterList, excludeList, order);
	}

}
