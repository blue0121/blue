package blue.http.internal.core.mapping;

import blue.base.core.util.AssertUtil;
import blue.http.internal.core.handler.HandlerChain;
import blue.http.internal.core.parser.ParserFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-07
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public class HandlerMappingFactory {

	private List<HandlerMapping> mappingList = new ArrayList<>();

	public HandlerMappingFactory(ParserFactory parserFactory) {
		AssertUtil.notNull(parserFactory, "ParserFactory");
		mappingList.add(new HttpHandlerMapping(parserFactory));
		mappingList.add(new WebSocketHandlerMapping(parserFactory));
	}

	public HandlerChain getHandlerChain(Object request) {
		if (request == null) {
			return null;
		}

		for (HandlerMapping mapping : mappingList) {
			if (mapping.accepted(request)) {
				HandlerChain chain = mapping.getHandlerChain(request);
				return chain;
			}
		}
		return null;
	}

}
