package blue.http.internal.core.handler.parameter;

import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-22
 */
public interface ParamHandler {
	/**
	 * 处理注解参数
	 *
	 * @param config
	 * @param request
	 * @return
	 */
	Object handle(RequestParamConfig config, Object request);

}
