package blue.http.internal.core.handler.parameter;

import blue.base.core.common.ErrorCode;
import blue.http.core.message.HttpRequest;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 2021-01-22
 */
public class QueryParamParamHandler implements ParamHandler {
	public QueryParamParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		if (!(request instanceof HttpRequest))
			return null;

		String param = ((HttpRequest)request).getQueryString(config.getParamAnnotationValue());
		if (param == null || param.isEmpty()) {
			if (config.isParamAnnotationRequired()) {
				throw ErrorCode.REQUIRED.newException(config.getName());
			}

			return null;
		}
		return ParamHandlerUtil.convert(config, param);
	}
}
