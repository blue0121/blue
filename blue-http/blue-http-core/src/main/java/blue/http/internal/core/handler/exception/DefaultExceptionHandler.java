package blue.http.internal.core.handler.exception;

import blue.base.core.common.ErrorCode;
import blue.http.internal.core.handler.HandlerResponse;
import io.netty.handler.codec.http.HttpResponseStatus;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-11
 */
public class DefaultExceptionHandler implements ExceptionHandler{
	public DefaultExceptionHandler() {
	}

	@Override
	public boolean accepted(Throwable cause) {
		return true;
	}

	@Override
    public void handle(HandlerResponse response) {
        response.setResult(ErrorCode.ERROR.toJsonString());
        response.setHttpStatus(HttpResponseStatus.INTERNAL_SERVER_ERROR);
    }
}
