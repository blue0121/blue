package blue.http.internal.core.parser.parameter;

import blue.http.core.annotation.Validated;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-27
 */
public class ValidatedParamParser implements ParamParser<Validated> {
	public ValidatedParamParser() {
	}

	@Override
	public void parse(RequestParamConfig config, Validated annotation) {
		config.setValidated(true);
		config.setValidatedGroups(annotation.value());
	}
}
