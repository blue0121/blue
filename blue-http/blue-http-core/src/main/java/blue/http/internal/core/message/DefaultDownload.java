package blue.http.internal.core.message;

import blue.http.core.message.Download;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author zhengjin
 * @since 1.0 2018年03月26日
 */
public class DefaultDownload implements Download {
	private static Logger logger = LoggerFactory.getLogger(DefaultDownload.class);

	private Type type;
	private byte[] memory;
	private Path path;
	private String filename;
	private boolean download;

	public DefaultDownload() {
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public byte[] getMemory() {
		return memory;
	}

	@Override
	public String getFilename() {
		return filename;
	}

	@Override
	public Path getPath() {
		return path;
	}

	@Override
	public boolean isDownload() {
		return download;
	}

	@Override
	public void delete() {
		if (path == null)
			return;

		try {
			Files.deleteIfExists(path);
			if (logger.isDebugEnabled()) {
				logger.debug("Delete file: {}", path);
			}
		} catch (IOException e) {
			logger.error("Delete file error, ", e);
		}
	}

	public void setType(Type type) {
		this.type = type;
	}

	public void setMemory(byte[] memory) {
		this.memory = memory;
	}

	public void setPath(Path path) {
		this.path = path;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void setDownload(boolean download) {
		this.download = download;
	}
}
