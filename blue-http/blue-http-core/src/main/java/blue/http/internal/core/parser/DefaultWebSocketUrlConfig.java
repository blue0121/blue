package blue.http.internal.core.parser;

import blue.base.core.reflect.BeanMethod;
import blue.base.core.reflect.JavaBean;
import blue.http.core.parser.WebSocketUrlConfig;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2020-08-28
 */
public class DefaultWebSocketUrlConfig implements WebSocketUrlConfig {
	private String name;
	private String url;
	private JavaBean javaBean;
	private BeanMethod method;
	private List<RequestParamConfig> paramList;

	public DefaultWebSocketUrlConfig() {
	}

	public WebSocketUrlKey buildKey() {
		return new WebSocketUrlKey(url);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setJavaBean(JavaBean javaBean) {
		this.javaBean = javaBean;
	}

	public void setMethod(BeanMethod method) {
		this.method = method;
	}

	public List<RequestParamConfig> getParamList() {
		return paramList;
	}

	public void setParamList(List<RequestParamConfig> paramList) {
		this.paramList = List.copyOf(paramList);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getUrl() {
		return url;
	}

	@Override
	public BeanMethod getMethod() {
		return method;
	}

	@Override
	public JavaBean getJavaBean() {
		return javaBean;
	}
}
