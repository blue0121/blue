package blue.http.internal.core.handler;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-06
 */
public interface HandlerChain<T, V> {

	boolean applyPreHandle(T request, V response) throws Exception;

	void applyPostHandle(T request, V response) throws Exception;

	void triggerAfterCompletion(T request, V response, Exception ex);

	Object getHandler();

}
