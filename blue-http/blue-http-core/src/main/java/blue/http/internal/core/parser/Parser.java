package blue.http.internal.core.parser;

import blue.base.core.reflect.JavaBean;

/**
 * 控制器解析
 *
 * @author Jin Zheng
 * @since 1.0 2021-08-05
 */
public interface Parser {

	/**
	 * 解析
	 *
	 * @param bean
	 */
	void parse(JavaBean bean);

}
