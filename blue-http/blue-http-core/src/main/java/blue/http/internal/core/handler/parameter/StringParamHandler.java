package blue.http.internal.core.handler.parameter;

import blue.http.core.message.HttpRequest;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-22
 */
public class StringParamHandler implements ParamHandler {
	public StringParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		if (request instanceof HttpRequest) {
			return ((HttpRequest) request).getContent();
		} else if (request instanceof WebSocketRequest) {
			return ((WebSocketRequest) request).getContent();
		}

		return null;
	}
}
