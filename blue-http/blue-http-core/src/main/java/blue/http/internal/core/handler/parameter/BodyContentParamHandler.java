package blue.http.internal.core.handler.parameter;


import blue.http.core.message.HttpRequest;
import blue.http.core.message.WebSocketRequest;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-22
 */
public class BodyContentParamHandler implements ParamHandler {
	public BodyContentParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		String body = this.getBody(request);
		if (body == null || body.isEmpty()) {
			return null;
		}

		return ParamHandlerUtil.convert(config, body);
	}

	private String getBody(Object request) {
		if (request instanceof HttpRequest) {
			return ((HttpRequest) request).getContent();
		} else if (request instanceof WebSocketRequest) {
			return ((WebSocketRequest) request).getContent();
		}
		return null;
	}
}
