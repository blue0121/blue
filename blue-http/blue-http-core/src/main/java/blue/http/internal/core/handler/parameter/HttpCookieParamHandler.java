package blue.http.internal.core.handler.parameter;

import blue.base.core.common.ErrorCode;
import blue.http.core.message.HttpRequest;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 2021-01-24
 */
public class HttpCookieParamHandler implements ParamHandler {
	public HttpCookieParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		if (!(request instanceof HttpRequest))
			return null;

		String param = ((HttpRequest)request).getCookie(config.getParamAnnotationValue());
		if (param == null || param.isEmpty()) {
			if (config.isParamAnnotationRequired()) {
				throw ErrorCode.REQUIRED.newException(config.getName());
			}

			return null;
		}

		return ParamHandlerUtil.convert(config, param);
	}
}
