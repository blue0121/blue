package blue.http.internal.core.handler.exception;

import blue.base.core.common.ErrorCode;
import blue.http.core.exception.HttpServerException;
import blue.http.internal.core.handler.HandlerResponse;
import io.netty.handler.codec.http.HttpResponseStatus;

/**
 * @author Jin Zheng
 * @since 1.0 2020-01-07
 */
public class HttpServerExceptionHandler implements ExceptionHandler {
	public HttpServerExceptionHandler() {
	}

	@Override
	public boolean accepted(Throwable cause) {
		return cause instanceof HttpServerException;
	}

	@Override
	public void handle(HandlerResponse response) {
		HttpServerException ex = (HttpServerException) response.getCause();
		ErrorCode errorCode = ErrorCode.SYS_ERROR;
		response.setResult(errorCode.toJsonString(ex.getMessage()));
		response.setHttpStatus(HttpResponseStatus.valueOf(errorCode.getHttpStatus()));
	}

}
