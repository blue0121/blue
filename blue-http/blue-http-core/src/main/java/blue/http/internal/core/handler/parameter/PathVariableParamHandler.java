package blue.http.internal.core.handler.parameter;

import blue.base.core.common.ErrorCode;
import blue.http.core.message.HttpRequest;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 2021-01-24
 */
public class PathVariableParamHandler implements ParamHandler {
	public PathVariableParamHandler() {
	}

	@Override
	public Object handle(RequestParamConfig config, Object request) {
		if (!(request instanceof HttpRequest))
			return null;

		String param = ((HttpRequest)request).getPathVariable(config.getParamAnnotationValue());
		if (param == null || param.isEmpty()) {
			throw ErrorCode.REQUIRED.newException(config.getName());
		}

		return ParamHandlerUtil.convert(config, param);
	}
}
