package blue.http.internal.core.plugin;

import blue.http.core.plugin.HandlerRegister;
import blue.http.internal.core.net.DefaultHttpServerBuilder;

/**
 * @author Jin Zheng
 * @date 2021-09-16
 */
public class DefaultHandlerRegistry implements HandlerRegister {
	private final DefaultHttpServerBuilder builder;

	public DefaultHandlerRegistry(DefaultHttpServerBuilder builder) {
		this.builder = builder;
	}

	@Override
	public void addHandler(Object handler) {
		builder.addHandler(handler);
	}

	@Override
	public void addHandler(Object handler, Class<?> clazz) {
		builder.addHandler(handler, clazz);
	}
}
