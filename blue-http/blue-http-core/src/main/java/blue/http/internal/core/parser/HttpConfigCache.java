package blue.http.internal.core.parser;

import blue.http.core.annotation.HttpMethod;
import blue.http.core.parser.HttpUrlConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2020-08-28
 */
public class HttpConfigCache {
	public static final String BRACE_START = "{";
	public static final String BRACE_END = "}";

	private Map<HttpUrlKey, DefaultHttpUrlConfig> cache = new HashMap<>();
	private Map<HttpUrlKey, DefaultHttpUrlConfig> pathCache = new HashMap<>();

	private AntPathMatcher pathMatcher = new AntPathMatcher();

	public HttpConfigCache() {
	}

	public void put(HttpUrlKey key, DefaultHttpUrlConfig config) {
		cache.put(key, config);
		if (config.getUrl().contains(BRACE_START) && config.getUrl().contains(BRACE_END)) {
			pathCache.put(key, config);
		}
	}

	public boolean contains(HttpUrlKey key) {
		return cache.containsKey(key);
	}

	public HttpMethodResult getConfig(String url, HttpMethod method) {
		return this.getConfig(new HttpUrlKey(url, method));
	}

	public HttpMethodResult getConfig(HttpUrlKey key) {
		DefaultHttpUrlConfig config = cache.get(key);
		if (config != null) {
			return new HttpMethodResult(config, null);
		}

		for (var entry : pathCache.entrySet()) {
			String url = entry.getKey().getUrl();
			if (pathMatcher.match(url, key.getUrl())) {
				var map = pathMatcher.extractUriTemplateVariables(url, key.getUrl());
				return new HttpMethodResult(entry.getValue(), map);
			}
		}
		return null;
	}

	public List<HttpUrlConfig> all() {
		return List.copyOf(cache.values());
	}

}
