package blue.http.internal.core.parser.parameter;

import blue.http.core.annotation.PathVariable;
import blue.http.internal.core.parser.RequestParamConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-27
 */
public class PathVariableParamParser implements ParamParser<PathVariable> {
	public PathVariableParamParser() {
	}

	@Override
	public void parse(RequestParamConfig config, PathVariable annotation) {
		config.setParamAnnotationClazz(annotation.annotationType());
		config.setParamAnnotationValue(annotation.value());
		config.setParamAnnotationRequired(true);
	}
}
