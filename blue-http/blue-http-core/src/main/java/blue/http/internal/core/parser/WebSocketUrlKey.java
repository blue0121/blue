package blue.http.internal.core.parser;

import java.util.Objects;

/**
 * @author Jin Zheng
 * @since 1.0 2020-08-28
 */
public class WebSocketUrlKey {
	private String url;

	public WebSocketUrlKey() {

	}

	public WebSocketUrlKey(String url) {
		this.url = url;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		WebSocketUrlKey that = (WebSocketUrlKey) o;
		return url.equals(that.url);
	}

	@Override
	public int hashCode() {
		return Objects.hash(url);
	}

	@Override
	public String toString() {
		return String.format("{url: %s}", url);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
