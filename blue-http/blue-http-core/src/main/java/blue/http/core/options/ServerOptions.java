package blue.http.core.options;

import blue.base.core.util.AssertUtil;

import java.util.concurrent.ExecutorService;

/**
 * @author Jin Zheng
 * @since 2021-08-21
 */
public class ServerOptions {
    private int port = 8080;
    private int ioThread = Runtime.getRuntime().availableProcessors();
    private ExecutorService executor;
    private long startup = 0;

	public ServerOptions() {
	}

	public void check() {
        AssertUtil.notNull(executor, "ExecutorService");
        AssertUtil.nonNegative(ioThread, "I/O thread");
    }

    public int getPort() {
        return port;
    }

    public ServerOptions setPort(int port) {
        this.port = port;
        return this;
    }

    public int getIoThread() {
        return ioThread;
    }

    public ServerOptions setIoThread(int ioThread) {
        this.ioThread = ioThread;
        return this;
    }

    public ExecutorService getExecutor() {
        return executor;
    }

    public ServerOptions setExecutor(ExecutorService executor) {
        this.executor = executor;
        return this;
    }

    public long getStartup() {
        return startup;
    }

    public ServerOptions setStartup(long startup) {
        this.startup = startup;
        return this;
    }
}
