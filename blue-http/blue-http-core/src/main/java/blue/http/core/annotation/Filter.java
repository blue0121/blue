package blue.http.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-05
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Filter {

	/**
	 * 拦截URL
	 *
	 * @return
	 */
	String[] filters();

	/**
	 * 排除URL
	 *
	 * @return
	 */
	String[] excludes() default {};

	/**
	 * 升序排列
	 *
	 * @return
	 */
	int order() default 100;

}
