package blue.http.core.parser;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-05
 */
public interface FilterConfig extends Comparable<FilterConfig> {

	List<String> getFilters();

	List<String> getExcludes();

	int getOrder();

	Object getTarget();

	@Override
	default int compareTo(FilterConfig o) {
		return this.getOrder() - o.getOrder();
	}
}
