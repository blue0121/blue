package blue.http.core.options;

import blue.base.core.util.AssertUtil;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-20
 */
public class WebSocketOptions {
	private String root = "/ws";

	public WebSocketOptions() {
	}

	public void check() {
		AssertUtil.notNull(root, "WebSocket root path");
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}
}
