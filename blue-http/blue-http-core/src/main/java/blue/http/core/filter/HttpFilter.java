package blue.http.core.filter;

import blue.http.core.message.HttpRequest;
import blue.http.core.message.HttpResponse;

/**
 * HTTP 调用过滤器
 *
 * @author Jin Zheng
 * @since 2020-02-03
 */
public interface HttpFilter {

	/**
	 * 预/前处理
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	default boolean preHandle(HttpRequest request, HttpResponse response) throws Exception {
		return true;
	}

	/**
	 * 后处理
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	default void postHandle(HttpRequest request, HttpResponse response) throws Exception {
	}

	/**
	 * 完成后处理
	 *
	 * @param request
	 * @param response
	 * @param ex
	 * @throws Exception
	 */
	default void afterCompletion(HttpRequest request, HttpResponse response, Exception ex) throws Exception {
	}

}
