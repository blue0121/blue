package blue.http.core.plugin;

/**
 * @author Jin Zheng
 * @date 2021-09-16
 */
public interface HandlerProvider {

	/**
	 * HandlerRegistry#addHandler
	 *
	 * @param register
	 */
	void handle(HandlerRegister register);

}
