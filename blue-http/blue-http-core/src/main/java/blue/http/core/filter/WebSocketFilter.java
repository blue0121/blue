package blue.http.core.filter;

import blue.http.core.message.WebSocketRequest;
import blue.http.core.message.WebSocketResponse;

/**
 * WebSocket 调用过滤器
 *
 * @author Jin Zheng
 * @since 2020-02-03
 */
public interface WebSocketFilter {

	/**
	 * 预/前处理
	 *
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	default boolean preHandle(WebSocketRequest request, WebSocketResponse response) throws Exception {
		return true;
	}

	/**
	 * 后处理
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	default void postHandle(WebSocketRequest request, WebSocketResponse response) throws Exception {
	}

	/**
	 * 完成后处理
	 *
	 * @param request
	 * @param response
	 * @param ex
	 * @throws Exception
	 */
	default void afterCompletion(WebSocketRequest request, WebSocketResponse response, Exception ex) throws Exception {
	}

}
