package blue.http.core.message;

import blue.http.internal.core.message.DefaultViewBuilder;

import java.util.Map;

/**
 * 页面渲染
 *
 * @author zhengjin
 * @since 1.0 2017年11月20日
 */
public interface View {

	static ViewBuilder builder() {
		return new DefaultViewBuilder();
	}

	/**
	 * 是否页面跳转
	 *
	 * @return
	 */
	boolean isRedirect();

	/**
	 * 获取客户端跳转URL
	 *
	 * @return
	 */
	String getRedirect();

	/**
	 * 获取服务端渲染页面
	 *
	 * @return
	 */
	String getView();

	/**
	 * 获取服务端页面渲染数据
	 *
	 * @return
	 */
	Map<String, Object> getModel();

}
