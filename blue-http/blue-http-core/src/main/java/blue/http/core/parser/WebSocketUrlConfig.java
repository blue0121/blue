package blue.http.core.parser;

import blue.base.core.reflect.BeanMethod;
import blue.base.core.reflect.JavaBean;

/**
 * @author Jin Zheng
 * @since 1.0 2020-08-28
 */
public interface WebSocketUrlConfig {

	/**
	 * web socket url name
	 *
	 * @return
	 */
	String getName();

	/**
	 * web socket url
	 *
	 * @return
	 */
	String getUrl();

	/**
	 * method
	 *
	 * @return
	 */
	BeanMethod getMethod();

	/**
	 * class instance
	 *
	 * @return
	 */
	JavaBean getJavaBean();
}
