package blue.http.core.parser;

import blue.base.core.reflect.BeanMethod;
import blue.base.core.reflect.JavaBean;
import blue.http.core.annotation.Charset;
import blue.http.core.annotation.ContentType;
import blue.http.core.annotation.HttpMethod;

/**
 * @author Jin Zheng
 * @since 1.0 2020-08-28
 */
public interface HttpUrlConfig {

	/**
	 * http url name
	 *
	 * @return
	 */
	String getName();

	/**
	 * http url
	 *
	 * @return
	 */
	String getUrl();

	/**
	 * http method
	 *
	 * @return
	 */
	HttpMethod getHttpMethod();

	/**
	 * http charset
	 *
	 * @return
	 */
	Charset getCharset();

	/**
	 * http content-type
	 *
	 * @return
	 */
	ContentType getContentType();

	/**
	 * method
	 *
	 * @return
	 */
	BeanMethod getMethod();

	/**
	 * class instance
	 *
	 * @return
	 */
	JavaBean getJavaBean();
}
