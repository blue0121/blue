package blue.http.core.message;

import java.io.OutputStream;
import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * @author zhengjin
 * @since 1.0 2018年03月26日
 */
public interface DownloadBuilder
{
	Download build();

	DownloadBuilder handle(Download.Type type, Consumer<OutputStream> f);

	DownloadBuilder setPath(Path path);

	DownloadBuilder setFilename(String filename);

	DownloadBuilder setDownload(boolean download);
}
