package blue.http.core.message;

import java.util.Map;

/**
 * 创建页面渲染对象
 *
 * @author zhengjin
 * @since 1.0 2018年03月16日
 */
public interface ViewBuilder {

	/**
	 * 创建View
	 *
	 * @return
	 */
	View build();

	/**
	 * 设置301跳转路径
	 *
	 * @param redirect
	 * @return
	 */
	ViewBuilder setRedirect(String redirect);

	/**
	 * 设置页面视图路径
	 *
	 * @param view
	 * @return
	 */
	ViewBuilder setView(String view);

	/**
	 * 设置页面视图参数
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	ViewBuilder put(String key, Object value);

	/**
	 * 设置页面视图参数
	 *
	 * @param map
	 * @return
	 */
	ViewBuilder putAll(Map<String, Object> map);

	/**
	 * 移除页面视图参数
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	ViewBuilder remove(String key, Object value);

}
