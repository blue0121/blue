package blue.http.core.plugin;

import blue.http.core.message.Session;

import java.time.Duration;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-26
 */
public interface SessionProvider {
	/**
	 * default expire duration
	 */
	Duration DEFAULT_EXPIRE = Duration.ofMinutes(30);

	/**
	 * get or create session with token
	 *
	 * @param token
	 * @return
	 */
	Session getSession(String token);

}
