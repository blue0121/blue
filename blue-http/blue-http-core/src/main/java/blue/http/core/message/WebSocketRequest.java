package blue.http.core.message;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-20
 */
public interface WebSocketRequest {
	/**
	 * 获取令牌
	 *
	 * @return
	 */
	String getToken();

	/**
	 * 获取时间戳
	 *
	 * @return
	 */
	long getTimestamp();

	/**
	 * 获取URL
	 *
	 * @return
	 */
	String getUrl();

	/**
	 * 获取内容
	 *
	 * @return
	 */
	String getContent();

	/**
	 * 获取对象并验证
	 *
	 * @param clazz
	 * @param groups
	 * @param <T>
	 * @return
	 */
	<T> T getObject(Class<T> clazz, Class<?>... groups);

	/**
	 * 获取对象列表并验证
	 *
	 * @param clazz
	 * @param groups
	 * @param <T>
	 * @return
	 */
	<T> List<T> getObjectList(Class<T> clazz, Class<?>... groups);

	/**
	 * 设置用户ID
	 *
	 * @param userId
	 */
	void setUserId(int userId);

	/**
	 * 获取用户ID
	 *
	 * @return
	 */
	int getUserId();
}
