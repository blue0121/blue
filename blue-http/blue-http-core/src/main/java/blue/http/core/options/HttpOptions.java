package blue.http.core.options;

import blue.base.core.path.FilePath;
import blue.base.core.util.NumberUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-13
 */
public class HttpOptions {
	private static Logger logger = LoggerFactory.getLogger(HttpOptions.class);

	private String contextPath = "/";
	private Map<String, String> virtualPathMap = new HashMap<>();
	private long maxUploadSize;
	private String viewRoot;

	public HttpOptions() {
	}

	public void check() {
		logger.info("Max upload file size：{}", maxUploadSize > 0 ? NumberUtil.byteFormat(maxUploadSize) : "Unlimited");
		logger.info("App context path：{}", contextPath);
		for (var entry : virtualPathMap.entrySet()) {
			logger.info("Virtual folder：{} ==> {}", entry.getKey(), entry.getValue());
		}
		this.virtualPathMap = Map.copyOf(virtualPathMap);
	}

	public String getContextPath() {
		return contextPath;
	}

	public HttpOptions setContextPath(String contextPath) {
		if (contextPath == null || contextPath.isEmpty()) {
			return this;
		}

		if (contextPath.startsWith(FilePath.SLASH)) {
			this.contextPath = contextPath;
		} else {
			this.contextPath = FilePath.SLASH + contextPath;
		}
		return this;
	}

	public Map<String, String> getVirtualPathMap() {
		return virtualPathMap;
	}

	public HttpOptions putVirtualPath(String path, String folder) {
		virtualPathMap.put(path, folder);
		return this;
	}

	public HttpOptions putVirtualPathAll(Map<String, String> map) {
		if (map != null && !map.isEmpty()) {
			virtualPathMap.putAll(map);
		}
		return this;
	}

	public long getMaxUploadSize() {
		return maxUploadSize;
	}

	public HttpOptions setMaxUploadSize(long maxUploadSize) {
		this.maxUploadSize = maxUploadSize;
		return this;
	}

	public String getViewRoot() {
		return viewRoot;
	}

	public HttpOptions setViewRoot(String viewRoot) {
		this.viewRoot = viewRoot;
		return this;
	}
}
