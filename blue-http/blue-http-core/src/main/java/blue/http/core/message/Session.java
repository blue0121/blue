package blue.http.core.message;

import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-25
 */
public interface Session {

	/**
	 * get token
	 *
	 * @return
	 */
	String getToken();

	/**
	 * set session key
	 *
	 * @param key
	 */
	void setKey(String key);

	/**
	 * get session key
	 *
	 * @return
	 */
	String getKey();

	/**
	 * delete this session
	 */
	void delete();

	/**
	 * all session attribute
	 *
	 * @return
	 */
	Map<String, Object> getAttrs();

	/**
	 * get session attribute
	 *
	 * @param key
	 * @return
	 */
	Object getAttr(String key);

	/**
	 * set session attribute
	 *
	 * @param key
	 * @param value
	 */
	void setAttr(String key, Object value);

	/**
	 * remove session attribute by key
	 *
	 * @param key
	 */
	void removeAttr(String key);

	/**
	 * clear session attribute
	 */
	void clearAttr();

}
