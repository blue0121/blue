package blue.http.core.message;

import blue.http.internal.core.message.DefaultDownloadBuilder;

import java.nio.file.Path;

/**
 * @author zhengjin
 * @since 1.0 2018年03月26日
 */
public interface Download {
	enum Type {
		/**
		 * 内存
		 */
		MEMORY,

		/**
		 * 文件
		 */
		FILE
	}

	static DownloadBuilder builder() {
		return new DefaultDownloadBuilder();
	}

	/**
	 * 获取输出流类型
	 */
	Type getType();

	/**
	 * 获取内存字节数组
	 */
	byte[] getMemory();

	/**
	 * 获取下载文件名称
	 */
	String getFilename();

	/**
	 * 获取文件
	 */
	Path getPath();

	/**
	 * 是否为文件下载
	 */
	boolean isDownload();

	/**
	 * 删除文件
	 */
	void delete();
}
