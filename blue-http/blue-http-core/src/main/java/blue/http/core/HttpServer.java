package blue.http.core;

import blue.http.internal.core.net.DefaultHttpServerBuilder;

import java.util.concurrent.CountDownLatch;

/**
 * @author Jin Zheng
 * @since 1.0 2021-07-27
 */
public interface HttpServer {

	static HttpServerBuilder builder() {
		return new DefaultHttpServerBuilder();
	}

	/**
	 * start http server
	 *
	 * @throws Exception
	 */
	void start() throws Exception;

	/**
	 * start http server with CountDownLatch
	 *
	 * @param latch
	 * @throws Exception
	 */
	void start(CountDownLatch latch) throws Exception;

	/**
	 * stop http server
	 */
	void stop();
}
