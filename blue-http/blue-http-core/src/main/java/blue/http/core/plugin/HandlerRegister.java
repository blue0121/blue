package blue.http.core.plugin;

/**
 * @author Jin Zheng
 * @date 2021-09-16
 */
public interface HandlerRegister {
	/**
	 * add handler, such as Controller, Filter
	 *
	 * @param handler
	 */
	void addHandler(Object handler);

	/**
	 * add handler, such as Controller, Filter
	 *
	 * @param handler
	 * @param clazz
	 */
	void addHandler(Object handler, Class<?> clazz);

}
