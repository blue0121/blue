package blue.http.core;

import blue.http.core.options.HttpOptions;
import blue.http.core.options.ServerOptions;
import blue.http.core.options.SslOptions;
import blue.http.core.options.WebSocketOptions;
import blue.http.core.plugin.SessionProvider;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-16
 */
public interface HttpServerBuilder {

	/**
	 * build http server instance
	 *
	 * @return
	 */
	HttpServer build();

	/**
	 * set server options
	 *
	 * @param options
	 * @return
	 */
	HttpServerBuilder setServerOptions(ServerOptions options);

	/**
	 * set http options
	 *
	 * @param options
	 */
	HttpServerBuilder setHttpOptions(HttpOptions options);

	/**
	 * set ssl/tls options
	 *
	 * @param options
	 */
	HttpServerBuilder setSslOptions(SslOptions options);

	/**
	 * set websocket options
	 *
	 * @param options
	 * @return
	 */
	HttpServerBuilder setWebSocketOptions(WebSocketOptions options);

	/**
	 * set session provider
	 *
	 * @param provider
	 * @return
	 */
	HttpServerBuilder setSessionProvider(SessionProvider provider);

	/**
	 * add HandlerProvider,
	 *
	 * @param provider
	 * @return
	 */
	HttpServerBuilder addProvider(Object provider);

	/**
	 * add handler, such as Controller, Filter
	 *
	 * @param handler
	 * @return
	 */
	HttpServerBuilder addHandler(Object handler);

	/**
	 * add handler, such as Controller, Filter
	 *
	 * @param handler
	 * @param clazz
	 */
	HttpServerBuilder addHandler(Object handler, Class<?> clazz);

}
