module blue.http.core {
	requires java.xml;
	requires blue.base.core;
	requires blue.validation.core;
	requires io.netty.all;
	requires fastjson;
	requires freemarker;

	exports blue.http.core;
	exports blue.http.core.annotation;
	exports blue.http.core.exception;
	exports blue.http.core.filter;
	exports blue.http.core.message;
	exports blue.http.core.options;
	exports blue.http.core.parser;
	exports blue.http.core.plugin;

	exports blue.http.internal.core.parser.parameter to blue.base.core;
}