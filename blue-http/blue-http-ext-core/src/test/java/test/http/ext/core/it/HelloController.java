package test.http.ext.core.it;

import blue.http.core.annotation.Http;
import blue.http.core.annotation.HttpMethod;

/**
 * @author Jin Zheng
 * @since 2019-12-29
 */
@Http(url = "/hello", method = HttpMethod.POST)
public class HelloController
{
	public HelloController()
	{
	}

	public String index(String name)
	{
		return "hello, " + name;
	}

}
