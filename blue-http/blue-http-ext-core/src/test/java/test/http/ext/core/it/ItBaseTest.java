package test.http.ext.core.it;

import blue.base.core.http.HttpClient;
import blue.base.core.util.WaitUtil;
import blue.http.core.HttpServer;
import blue.http.core.HttpServerBuilder;
import blue.http.core.options.HttpOptions;
import blue.http.core.options.ServerOptions;
import blue.http.ext.core.plugin.HealthProvider;
import blue.http.ext.core.plugin.MonitorProvider;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;

/**
 * @author Jin Zheng
 * @since 1.0 2021-08-24
 */
public class ItBaseTest {
	protected static final int PORT;
	protected static final String HOST;

	protected static HttpServer server;
	protected static HttpClient client;

	static {
		Random random = new Random();
		PORT = random.nextInt(100) + 40000;
		HOST = "http://localhost:" + PORT;
	}

	public ItBaseTest() {
	}

	private static void addProvider(HttpServerBuilder builder) {
		builder.addProvider(new HealthProvider());
		builder.addProvider(new MonitorProvider());
	}

	private static void addHandler(HttpServerBuilder builder) {
		builder.addHandler(new HelloController());
	}

	@BeforeAll
	public static void beforeAll() {
		ServerOptions serverOptions = new ServerOptions();
		serverOptions.setPort(PORT)
				.setStartup(System.currentTimeMillis())
				.setExecutor(Executors.newFixedThreadPool(10))
				.setIoThread(2);
		HttpOptions options = new HttpOptions();
		options.setContextPath("/")
				.setViewRoot("/view");
		HttpServerBuilder builder = HttpServer.builder();
		builder.setServerOptions(serverOptions)
				.setHttpOptions(options);
		addProvider(builder);
		addHandler(builder);
		server = builder.build();
		CountDownLatch latch = new CountDownLatch(1);
		new Thread(() -> {
			try {
				server.start(latch);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		WaitUtil.await(latch);

		client = HttpClient.builder()
				.setBaseUrl(HOST)
				.setTimeout(5_000)
				.build();
	}

	@AfterAll
	public static void afterAll() {
		if (server != null) {
			server.stop();
		}
	}
}
