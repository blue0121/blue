package test.http.ext.core.it;

import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author Jin Zheng
 * @since 1.0 2021-09-17
 */
public class HandlerProviderTest extends ItBaseTest {
	public HandlerProviderTest() {
	}

    @Test
	public void testHealth() {
		var response = client.getSync("/management/health");
	    String json = response.getBody();
	    System.out.println(json);
	    Assertions.assertEquals(200, response.getCode());
	    Assertions.assertNotNull(json);
	    var object = JSON.parseObject(json);
		Assertions.assertEquals("up", object.getString("health"));
	}

	@Test
	public void testMonitor() {
		var response = client.getSync("/management/monitor");
		String metric = response.getBody();
		System.out.println(metric);
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertNotNull(metric);
		Assertions.assertFalse(metric.contains("summary_http_request_bucket{"));

		System.out.println("========================");
		client.postSync("/hello", "blue");
		response = client.getSync("/management/monitor");
		metric = response.getBody();
		System.out.println(metric);
		Assertions.assertEquals(200, response.getCode());
		Assertions.assertNotNull(metric);
		Assertions.assertTrue(metric.contains("summary_http_request_bucket{"));
	}

}
