package blue.http.ext.core.plugin;

import blue.http.core.plugin.HandlerProvider;
import blue.http.core.plugin.HandlerRegister;
import blue.http.ext.internal.core.controller.HealthController;

/**
 * @author Jin Zheng
 * @since 1.0 2021-09-17
 */
public class HealthProvider implements HandlerProvider {
	public HealthProvider() {
	}

	@Override
	public void handle(HandlerRegister register) {
		register.addHandler(new HealthController());
	}
}
