package blue.http.ext.core.plugin;

import blue.http.core.plugin.HandlerProvider;
import blue.http.core.plugin.HandlerRegister;
import blue.http.ext.internal.core.controller.MonitorController;
import blue.http.ext.internal.core.filter.MonitorFilter;

/**
 * @author Jin Zheng
 * @since 1.0 2021-09-17
 */
public class MonitorProvider implements HandlerProvider {
	private MonitorFilter filter;

	public MonitorProvider() {
		this.filter = new MonitorFilter();
	}

	public MonitorProvider(String buckets) {
		this.filter = new MonitorFilter(buckets);
	}

	public MonitorProvider(double...buckets) {
		this.filter = new MonitorFilter(buckets);
	}

	@Override
	public void handle(HandlerRegister register) {
		register.addHandler(filter);
		register.addHandler(new MonitorController());
	}
}
