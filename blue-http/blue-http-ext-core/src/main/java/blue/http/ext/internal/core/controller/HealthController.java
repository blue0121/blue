package blue.http.ext.internal.core.controller;


import blue.http.core.annotation.ContentType;
import blue.http.core.annotation.Http;
import blue.http.core.annotation.HttpMethod;

/**
 * @author Jin Zheng
 * @since 1.0 2021-01-11
 */
@Http(url = "/management/health", method = HttpMethod.GET, contentType = ContentType.JSON)
public class HealthController {
	private static final String UP = """
			{"health":"up"}""";

	public HealthController() {
	}

	public String index() {
		return UP;
	}

}
