module blue.http.ext.core {
	requires blue.base.core;
	requires blue.http.core;
	requires simpleclient;
	requires simpleclient.common;
	requires simpleclient.hotspot;

	exports blue.http.ext.core.plugin;

	exports blue.http.ext.internal.core.controller to blue.base.core;
}