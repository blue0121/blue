package test.jdbc.core.model;

import blue.jdbc.core.annotation.Entity;
import blue.jdbc.core.annotation.Id;
import blue.jdbc.core.annotation.Transient;
import blue.jdbc.core.annotation.Version;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Assertions;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-27
 */
@Getter
@Setter
@Entity(table = "usr_user")
public class UserEntity {
	@Id
	private Integer id;
	@Version
	private Integer version;
	private Integer groupId;
	private String name;
	private String password;
	private Integer state;
	@Transient
	private String groupName;

	public UserEntity() {
	}

	public static UserEntity create(Integer groupId, String name, String password, Integer state) {
		UserEntity user = new UserEntity();
		user.setGroupId(groupId);
		user.setName(name);
		user.setPassword(password);
		user.setState(state);
		return user;
	}

	public static void verify(UserEntity user, Integer version, Integer groupId, String name, String password, Integer state) {
		Assertions.assertNotNull(user);
		Assertions.assertEquals(version, user.getVersion());
		Assertions.assertEquals(groupId, user.getGroupId());
		Assertions.assertEquals(name, user.getName());
		Assertions.assertEquals(password, user.getPassword());
		Assertions.assertEquals(state, user.getState());
	}

}
