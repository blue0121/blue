package test.jdbc.core.model;

import blue.jdbc.core.annotation.Column;
import blue.jdbc.core.annotation.Mapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-27
 */
@Getter
@Setter
@NoArgsConstructor
@Mapper
public class ResultMapper {
	private String userId;
	private String name;
	@Column(name = "message")
	private String msg;

}
