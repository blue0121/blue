package test.jdbc.core.model;

import blue.jdbc.core.annotation.Mapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-22
 */
@Getter
@Setter
@NoArgsConstructor
@Mapper
public class UserGroupMapper {
	private Integer id;
	private Integer groupId;
	private String name;
	private String password;
	private Integer state;
	private String groupName;

}
