package test.jdbc.core.it;

import blue.jdbc.core.exception.VersionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.jdbc.core.model.GroupEntity;
import test.jdbc.core.model.UserEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-15
 */
public class JdbcDynamicSQLItTest extends ItBaseTest {
	public JdbcDynamicSQLItTest() {
	}

	@BeforeEach
	public void beforeEach() {
		super.beforeEach();
	}

	@AfterEach
	public void afterEach() {
		super.afterEach();
	}

	@Test
	public void testCount() {
		Map<String, Object> param = Map.of("name", "blue");
		int count = jdbcOperation.count(GroupEntity.class, param);
		Assertions.assertEquals(1, count);

		param = Map.of("name", "blue", "count", 2);
		count = jdbcOperation.count(GroupEntity.class, param);
		Assertions.assertEquals(0, count);
	}

	@Test
	public void testDeleteBy() {
		Map<String, Object> param = Map.of("name", "blue");
		int count = jdbcOperation.deleteBy(GroupEntity.class, param);
		Assertions.assertEquals(1, count);

		param = Map.of("name", "green", "count", 1);
		count = jdbcOperation.deleteBy(GroupEntity.class, param);
		Assertions.assertEquals(1, count);

		var map = jdbcOperation.getList(GroupEntity.class, List.of(1, 2));
		Assertions.assertTrue(map.isEmpty());
	}

	@Test
	public void testExist() {
		GroupEntity group = new GroupEntity();
		group.setName("blue");
		Assertions.assertTrue(jdbcOperation.exist(group, "name"));

		group.setId(1);
		Assertions.assertFalse(jdbcOperation.exist(group, "name"));

		group.setId(2);
		Assertions.assertTrue(jdbcOperation.exist(group, "name"));
	}

	@Test
	public void testGetField() {
		Map<String, Object> param = new HashMap<>();
		param.put("id", 1);
		Assertions.assertEquals("blue", jdbcOperation.getField(GroupEntity.class, String.class, "name", param));

		param.put("count", 1);
		Assertions.assertEquals("blue", jdbcOperation.getField(GroupEntity.class, String.class, "name", param));

		param.put("id", 10);
		Assertions.assertNull(jdbcOperation.getField(GroupEntity.class, String.class, "name", param));
	}

	@Test
	public void testGet() {
		Map<String, Object> param = new HashMap<>();
		param.put("id", 1);
		var group = jdbcOperation.getObject(GroupEntity.class, param);
		GroupEntity.verify(group, 1, "blue", 1);

		param.put("name", "blue");
		group = jdbcOperation.getObject(GroupEntity.class, param);
		GroupEntity.verify(group, 1, "blue", 1);

		param.put("id", 10);
		Assertions.assertNull(jdbcOperation.getObject(GroupEntity.class, param));
	}

	@Test
	public void testInc1() {
		Map<String, Integer> param = new HashMap<>();
		param.put("count", 10);
		int n = jdbcOperation.inc(GroupEntity.class, 1, param);
		Assertions.assertEquals(1, n);
		var group = jdbcOperation.get(GroupEntity.class, 1);
		GroupEntity.verify(group, 1, "blue", 11);


		n = jdbcOperation.inc(GroupEntity.class, 10, param);
		Assertions.assertEquals(0, n);
	}

	@Test
	public void testInc2() {
		var user = UserEntity.create(1, "blue", "blue", 1);
		int n = jdbcOperation.save(user, false);
		Assertions.assertEquals(1, n);

		Map<String, Integer> param = new HashMap<>();
		param.put("groupId", 10);
		param.put("state", 10);
		n = jdbcOperation.inc(UserEntity.class, user.getId(), param);
		Assertions.assertEquals(1, n);

		user = jdbcOperation.get(UserEntity.class, user.getId());
		UserEntity.verify(user, 1, 11, "blue", "blue", 11);
	}

	@Test
	public void testSaveObject() {
		Map<String, Object> map = Map.of("id", 3, "name", "red", "count", 1);
		int n = jdbcOperation.saveObject(GroupEntity.class, map);
		Assertions.assertEquals(1, n);
		var group = jdbcOperation.get(GroupEntity.class, 3);
		GroupEntity.verify(group, 3, "red", 1);

		map = Map.of("groupId", 1, "name", "blue", "password", "blue", "state", 1);
		n = jdbcOperation.saveObject(UserEntity.class, map);
		Assertions.assertEquals(1, n);
		var user = jdbcOperation.getObject(UserEntity.class, Map.of("name", "blue"));
		System.out.println("userId: " + user.getId());
		UserEntity.verify(user, 1, 1, "blue", "blue", 1);
	}

	@Test
	public void testSave() {
		UserEntity user = UserEntity.create(1, "blue", "blue", 1);
		int n = jdbcOperation.save(user, true);
		Assertions.assertEquals(1, n);
		Assertions.assertNotNull(user.getId());
		user = jdbcOperation.get(UserEntity.class, user.getId());
		UserEntity.verify(user, 1, 1, "blue", "blue", 1);
	}

	@Test
	public void testUpdateObject() {
		Map<String, Object> map = Map.of("name", "blueblue");
		int n = jdbcOperation.updateObject(GroupEntity.class, 1, map);
		Assertions.assertEquals(1, n);
		var group = jdbcOperation.get(GroupEntity.class, 1);
		GroupEntity.verify(group, 1, "blueblue", 1);

		var user = UserEntity.create(1, "blue", "blue", 1);
		n = jdbcOperation.save(user);
		Assertions.assertEquals(1, n);
		map = Map.of("name", "blueblue", "version", 1);
		n = jdbcOperation.updateObject(UserEntity.class, user.getId(), map);
		Assertions.assertEquals(1, n);
		user = jdbcOperation.get(UserEntity.class, user.getId());
		UserEntity.verify(user, 2, 1, "blueblue", "blue", 1);

		map = Map.of("name", "blueblue", "version", 1);
		try {
			jdbcOperation.updateObject(UserEntity.class, user.getId(), map);
			Assertions.fail();
		} catch (VersionException e) {
			Assertions.assertEquals("test.jdbc.core.model.UserEntity conflict", e.getMessage());
		}
	}

	@Test
	public void testUpdate() {
		var group = GroupEntity.create(1, "blueblue", null);
		int n = jdbcOperation.update(group);
		Assertions.assertEquals(1, n);
		group = jdbcOperation.get(GroupEntity.class, 1);
		GroupEntity.verify(group, 1, "blueblue", 1);

		var user = UserEntity.create(1, "blue", "blue", 1);
		n = jdbcOperation.save(user);
		Assertions.assertEquals(1, n);
		user = jdbcOperation.get(UserEntity.class, user.getId());
		user.setName("blueblue");
		n = jdbcOperation.update(user);
		Assertions.assertEquals(1, n);
		user = jdbcOperation.get(UserEntity.class, user.getId());
		UserEntity.verify(user, 2, 1, "blueblue", "blue", 1);

		user.setVersion(1);
		try {
			jdbcOperation.update(user);
			Assertions.fail();
		} catch (VersionException e) {
			Assertions.assertEquals("test.jdbc.core.model.UserEntity conflict", e.getMessage());
		}
	}

}
