package test.jdbc.core.parser;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-01
 */
public class MapTest {
	private static final int COUNT = 100;

	public MapTest() {
	}

	@Test
	public void testLinkedHashMap() {
		this.assertMap(new LinkedHashMap<>());
	}

	@Test
	public void testTreeMap() {
		this.assertMap(new TreeMap<>());
	}

	private void assertMap(Map<Integer, Integer> map) {
		for (int i = 0; i < COUNT; i++) {
			map.put(i, i);
		}
		int j = 0;
		for (var entry : map.entrySet()) {
			Assertions.assertEquals(j, entry.getKey().intValue());
			Assertions.assertEquals(j, entry.getValue().intValue());
			j++;
		}

		var map2 = Collections.unmodifiableMap(map);
		j = 0;
		for (var entry : map2.entrySet()) {
			Assertions.assertEquals(j, entry.getKey().intValue());
			Assertions.assertEquals(j, entry.getValue().intValue());
			j++;
		}
	}

}
