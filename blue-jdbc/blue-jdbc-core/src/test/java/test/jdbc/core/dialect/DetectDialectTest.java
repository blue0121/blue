package test.jdbc.core.dialect;

import blue.jdbc.internal.core.dialect.DetectDialect;
import blue.jdbc.internal.core.dialect.Dialect;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-22
 */
public class DetectDialectTest {
	public DetectDialectTest() {
	}

	@ParameterizedTest
	@CsvSource({"jdbc:mysql://localhost:3306/test,blue.jdbc.internal.core.dialect.MySQLDialect",
				"jdbc:oracle:thin:@localhost:1521@test,blue.jdbc.internal.core.dialect.OracleDialect",
				"jdbc:postgresql://localhost:1534/test,blue.jdbc.internal.core.dialect.PostgreSQLDialect",
				"jdbc:hsqldb:memory:test,blue.jdbc.internal.core.dialect.HyperSQLDialect",
				"jdbc:test://localhost,"})
    public void testDialect(String url, Class<?> clazz) throws Exception {
		DataSource ds = Mockito.mock(DataSource.class);
		Connection conn = Mockito.mock(Connection.class);
		DatabaseMetaData meta = Mockito.mock(DatabaseMetaData.class);
		Mockito.when(ds.getConnection()).thenReturn(conn);
		Mockito.when(conn.getMetaData()).thenReturn(meta);
		Mockito.when(meta.getURL()).thenReturn(url);

		Dialect dialect = DetectDialect.dialect(ds);
		if (clazz == null) {
			Assertions.assertNull(dialect);
		} else {
			Assertions.assertNotNull(dialect);
			Assertions.assertEquals(clazz, dialect.getClass());
		}
    }

}
