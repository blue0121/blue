package test.jdbc.core.it;

import blue.jdbc.core.exception.VersionException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.jdbc.core.model.GroupEntity;
import test.jdbc.core.model.GroupMapper;
import test.jdbc.core.model.UserEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-12
 */
public class JdbcStaticSQLItTest extends ItBaseTest {
	public JdbcStaticSQLItTest() {
	}

	@BeforeEach
	public void beforeEach() {
		super.beforeEach();
	}

	@AfterEach
	public void afterEach() {
		super.afterEach();
	}

	@Test
	public void testGet() {
		var group = jdbcOperation.get(GroupEntity.class, 1);
		GroupEntity.verify(group, 1, "blue", 1);
	}

	@Test
	public void testGetList() {
		List<Integer> idList = List.of(1, 2);
		Map<Integer, GroupEntity> map = jdbcOperation.getList(GroupEntity.class, idList);
		Assertions.assertEquals(2, map.size());
		var g1 = map.get(1);
		GroupEntity.verify(g1, 1, "blue", 1);
		GroupEntity g2 = map.get(2);
		GroupEntity.verify(g2, 2, "green", 1);
	}

	@Test
	public void testDeleteId() {
		Assertions.assertEquals(1, jdbcOperation.deleteId(GroupEntity.class, 1));
		Assertions.assertEquals(1, jdbcOperation.deleteId(GroupEntity.class, 2));
		Assertions.assertNull(jdbcOperation.get(GroupEntity.class, 1));
		Assertions.assertNull(jdbcOperation.get(GroupEntity.class, 2));
	}

	@Test
	public void testDeleteIdList() {
		Assertions.assertEquals(2, jdbcOperation.deleteIdList(GroupEntity.class, List.of(1, 2)));
		Assertions.assertNull(jdbcOperation.get(GroupEntity.class, 1));
		Assertions.assertNull(jdbcOperation.get(GroupEntity.class, 2));
	}

	@Test
	public void testDeleteIdList2() {
		Assertions.assertEquals(2, jdbcOperation.deleteIdList(GroupEntity.class, List.of(1, 2, 3)));
		Assertions.assertNull(jdbcOperation.get(GroupEntity.class, 1));
		Assertions.assertNull(jdbcOperation.get(GroupEntity.class, 2));
	}

	@Test
	public void testSave() {
		var group = GroupEntity.create(3, "red", 3);
		jdbcOperation.save(group, false);
		var group2 = jdbcOperation.get(GroupEntity.class, 3);
		GroupEntity.verify(group2, 3, "red", 3);

		var user = UserEntity.create(1, "group1", "pass1", 1);
		int c = jdbcOperation.save(user, false);
		Assertions.assertTrue(c > 0);
		Assertions.assertNotNull(user.getId());
		System.out.println("userId: " + user.getId());
		var user2 = jdbcOperation.get(UserEntity.class, user.getId());
		UserEntity.verify(user2, 1, 1, "group1", "pass1", 1);
	}

	@Test
	public void testSaveAndUpdateList1() {
		String name = "update_name_";
		int count = 5;
		int[] expectRs = new int[count];
		List<GroupEntity> groupList = new ArrayList<>();
		List<Integer> idList = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			expectRs[i] = 1;
			int id = i + 3;
			idList.add(id);
			groupList.add(GroupEntity.create(id, "red_" + id, id));
		}
		int[] groupRs = jdbcOperation.saveList(groupList);
		Assertions.assertArrayEquals(expectRs, groupRs);
		int j = 1;
		for (var group : groupList) {
			group.setName(name + j);
			j++;
		}
		groupRs = jdbcOperation.updateList(groupList);
		Assertions.assertArrayEquals(expectRs, groupRs);
		var groupMap = jdbcOperation.getList(GroupEntity.class, idList);
		for (j = 1; j <= 5; j++) {
			int id = j + 2;
			var g = groupMap.get(id);
			GroupEntity.verify(g, id, name + j, id);
		}
	}

	@Test
	public void testSaveAndUpdateList2() {
		String name = "update_name_";
		int count = 5;
		int[] expectRs = new int[count];
		List<UserEntity> userList = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			expectRs[i] = 1;
			int id = i + 1;
			userList.add(UserEntity.create(id, "red_" + id, "pwd_" + id, id));
		}
		int[] userRs = jdbcOperation.saveList(userList);
		Assertions.assertArrayEquals(expectRs, userRs);
		List<Integer> idList = new ArrayList<>();
		for (var user : userList) {
			Assertions.assertNotNull(user.getId());
			idList.add(user.getId());
		}
		System.out.println("userIds: " + idList);

		int j = 1;
		for (var user : userList) {
			user.setName(name + j);
			user.setPassword(name + j);
			j++;
		}
		userRs = jdbcOperation.updateList(userList);
		Assertions.assertArrayEquals(expectRs, userRs);
		var userMap = jdbcOperation.getList(UserEntity.class, idList);
		System.out.println(userMap);
		j = 1;
		for (var id : idList) {
			UserEntity.verify(userMap.get(id), 2, j, name + j, name + j, j);
			j++;
		}

		userList.get(1).setVersion(100);
		Assertions.assertThrows(VersionException.class, () -> jdbcOperation.updateList(userList));
	}

	@Test
	public void testUpdate() {
		String name = "update_name";
		var group1 = jdbcOperation.get(GroupEntity.class, 1);
		GroupEntity.verify(group1, 1, "blue", 1);
		group1.setName(name);
		Assertions.assertEquals(1, jdbcOperation.update(group1, false));
		group1 = jdbcOperation.get(GroupEntity.class, 1);
		GroupEntity.verify(group1, 1, name, 1);

		var user1 = UserEntity.create(1, "blue", "pwd", 1);
		Assertions.assertEquals(1, jdbcOperation.save(user1, false));
		user1 = jdbcOperation.get(UserEntity.class, user1.getId());
		UserEntity.verify(user1, 1, 1, "blue", "pwd", 1);
		user1.setName(name);
		user1.setPassword(name);
		Assertions.assertEquals(1, jdbcOperation.update(user1, false));
		user1 = jdbcOperation.get(UserEntity.class, user1.getId());
		UserEntity.verify(user1, 2, 1, name, name, 1);

		var user2 = UserEntity.create(1, "blue", "blue", 1);
		user2.setId(user1.getId());
		Assertions.assertThrows(VersionException.class, () -> jdbcOperation.update(user2, false));
	}

	@Test
	public void testGetObject() {
		String sql = "select count(*) from \"group\"";
		Integer n = jdbcOperation.getObject(Integer.class, sql);
		Assertions.assertEquals(2, n.intValue());

		sql += " where \"name\"=?";
		n = jdbcOperation.getObject(Integer.class, sql, List.of("blue"));
		Assertions.assertEquals(1, n.intValue());

		sql = "select * from \"group\" where \"name\"=?";
		GroupMapper group = jdbcOperation.getObject(GroupMapper.class, sql, List.of("blue"));
		GroupMapper.verify(group, 1, "blue", 1);

		sql = "select \"id\" from \"group\" where \"name\"=?";
		GroupMapper group2 = jdbcOperation.getObject(GroupMapper.class, sql, List.of("blue"));
		Assertions.assertNotNull(group2);
		Assertions.assertEquals(1, group2.getId().intValue());
		Assertions.assertNull(group2.getName());
		Assertions.assertNull(group2.getCount());
	}

	@Test
	public void testList() {
		String sql = "select \"id\" from \"group\"";
		List<Integer> list = jdbcOperation.list(Integer.class, sql);
		Assertions.assertTrue(list != null && list.size() == 2);
		Assertions.assertTrue(list.contains(1));
		Assertions.assertTrue(list.contains(2));

		sql = "select * from \"group\" order by \"id\"";
		List<GroupMapper> groupList = jdbcOperation.list(GroupMapper.class, sql);
		Assertions.assertTrue(groupList != null && groupList.size() == 2);
		GroupMapper.verify(groupList.get(0), 1, "blue", 1);
		GroupMapper.verify(groupList.get(1), 2, "green", 1);
	}

}
