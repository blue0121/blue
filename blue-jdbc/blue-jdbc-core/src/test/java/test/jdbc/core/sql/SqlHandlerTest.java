package test.jdbc.core.sql;

import blue.jdbc.internal.core.dialect.Dialect;
import blue.jdbc.internal.core.dialect.MySQLDialect;
import blue.jdbc.internal.core.parser.EntityConfigCache;
import blue.jdbc.internal.core.parser.MapperConfigCache;
import blue.jdbc.internal.core.parser.ParserFactory;
import blue.jdbc.internal.core.sql.SqlHandlerFactory;
import test.jdbc.core.model.GroupEntity;
import test.jdbc.core.model.GroupMapper;
import test.jdbc.core.model.ResultMapper;
import test.jdbc.core.model.UserEntity;

/**
 * @author Jin Zheng
 * @since 1.0 2019-12-05
 */
public abstract class SqlHandlerTest {
	protected final Dialect dialect;
	protected final ParserFactory parseFactory;
	protected final EntityConfigCache entityConfigCache;
	protected final MapperConfigCache mapperConfigCache;
	protected final SqlHandlerFactory factory;

	public SqlHandlerTest() {
		this.dialect = new MySQLDialect();
		this.parseFactory = new ParserFactory(dialect);
		this.entityConfigCache = parseFactory.getEntityConfigCache();
		this.mapperConfigCache = parseFactory.getMapperConfigCache();
		this.factory = new SqlHandlerFactory();

		parseFactory.parse(GroupEntity.class);
		parseFactory.parse(GroupMapper.class);
		parseFactory.parse(ResultMapper.class);
		parseFactory.parse(UserEntity.class);
	}

}
