package test.jdbc.core.model;

import blue.jdbc.core.annotation.Entity;
import blue.jdbc.core.annotation.GeneratorType;
import blue.jdbc.core.annotation.Id;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Assertions;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-05
 */
@Getter
@Setter
@Entity(table = "group")
public class GroupEntity {
	@Id(generator = GeneratorType.ASSIGNED)
	private Integer id;
	private String name;
	private Integer count;

	public GroupEntity() {
	}

	public static GroupEntity create(Integer id, String name, Integer count) {
		GroupEntity group = new GroupEntity();
		group.setId(id);
		group.setName(name);
		group.setCount(count);
		return group;
	}

	public static void verify(GroupEntity group, Integer id, String name, Integer count) {
		Assertions.assertNotNull(group);
		Assertions.assertEquals(id, group.getId());
		Assertions.assertEquals(name, group.getName());
		Assertions.assertEquals(count, group.getCount());
	}

}
