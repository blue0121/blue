package test.jdbc.core.sql;

import blue.jdbc.core.exception.JdbcException;
import blue.jdbc.internal.core.sql.SqlType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test.jdbc.core.model.GroupEntity;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-15
 */
public class GetSqlHandlerTest extends SqlHandlerTest {

	public GetSqlHandlerTest() {
	}

    @Test
    public void testSql1() {
	    Map<String, Object> map = new HashMap<>();
		map.put("id", 1);
		var config = entityConfigCache.get(GroupEntity.class);
		var sqlItem = factory.handle(SqlType.GET, config, map);
	    System.out.println(sqlItem.getSql());
	    Assertions.assertEquals("select * from `group` where `id`=?",
			    sqlItem.getSql());
		Assertions.assertEquals(List.of("id"), sqlItem.getParamNameList());
    }

	@Test
	public void testSql2() {
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("id", 1);
		map.put("count", 1);
		var config = entityConfigCache.get(GroupEntity.class);
		var sqlItem = factory.handle(SqlType.GET, config, map);
		System.out.println(sqlItem.getSql());
		Assertions.assertEquals("select * from `group` where `id`=? and `count`=?",
				sqlItem.getSql());
		Assertions.assertEquals(List.of("id", "count"), sqlItem.getParamNameList());
	}

	@Test
	public void testSqlFailure1() {
		Map<String, Object> map = new HashMap<>();
		var config = entityConfigCache.get(GroupEntity.class);
		try {
			factory.handle(SqlType.GET, config, map);
			Assertions.fail();
		} catch (JdbcException e) {
			Assertions.assertEquals("Parameter is mandatory", e.getMessage());
		}
	}

	@Test
	public void testSqlFailure2() {
		Map<String, Object> map = new HashMap<>();
		map.put("aaa", "aaa");
		var config = entityConfigCache.get(GroupEntity.class);
		try {
			factory.handle(SqlType.GET, config, map);
			Assertions.fail();
		} catch (JdbcException e) {
			Assertions.assertEquals("Field [aaa] not exist", e.getMessage());
		}
	}

}
