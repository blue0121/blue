package test.jdbc.core.model;

import blue.jdbc.core.annotation.Dao;
import blue.jdbc.core.engine.BaseDao;
import blue.jdbc.core.engine.Expression;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-21
 */
@Dao
public class GroupDao extends BaseDao<GroupEntity> {
	public GroupDao() {
	}

	@Override
	protected void query(Expression exp, GroupEntity object, List<Object> paramList) {
		if (object.getName() != null && !object.getName().isEmpty()) {
			exp.add("""
					a."name" like ?""");
			paramList.add("%" + object.getName() + "%");
		}
		if (object.getCount() != null) {
			exp.add("""
					a."count"=?""");
			paramList.add(object.getCount());
		}
	}
}
