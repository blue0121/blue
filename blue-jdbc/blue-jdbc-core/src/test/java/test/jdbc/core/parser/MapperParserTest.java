package test.jdbc.core.parser;

import blue.jdbc.internal.core.dialect.MySQLDialect;
import blue.jdbc.internal.core.parser.ParserFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import test.jdbc.core.model.ResultMapper;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-27
 */
public class MapperParserTest {
	private ParserFactory factory;

	public MapperParserTest() {
		factory = new ParserFactory(new MySQLDialect());
	}

	@Test
	public void testParse() {
		factory.parse(ResultMapper.class);
		var cache = factory.getMapperConfigCache();
		var config = cache.get(ResultMapper.class);

		Assertions.assertEquals(ResultMapper.class, config.getClazz());
		var map = config.getColumnMap();
		Assertions.assertEquals(3, map.size());

		var userIdColumn = map.get("userId");
		Assertions.assertNotNull(userIdColumn);
		Assertions.assertEquals("userId", userIdColumn.getFieldName());
		Assertions.assertEquals("user_id", userIdColumn.getColumnName());
		Assertions.assertEquals("`user_id`", userIdColumn.getEscapeColumnName());

		var nameColumn = map.get("name");
		Assertions.assertNotNull(nameColumn);
		Assertions.assertEquals("name", nameColumn.getFieldName());
		Assertions.assertEquals("name", nameColumn.getColumnName());
		Assertions.assertEquals("`name`", nameColumn.getEscapeColumnName());

		var msgColumn = map.get("msg");
		Assertions.assertNotNull(msgColumn);
		Assertions.assertEquals("msg", msgColumn.getFieldName());
		Assertions.assertEquals("message", msgColumn.getColumnName());
		Assertions.assertEquals("`message`", msgColumn.getEscapeColumnName());
	}

}
