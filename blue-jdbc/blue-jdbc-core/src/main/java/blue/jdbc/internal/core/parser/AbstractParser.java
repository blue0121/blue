package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.BeanField;
import blue.base.core.reflect.JavaBean;
import blue.jdbc.core.annotation.Column;
import blue.jdbc.core.exception.JdbcException;
import blue.jdbc.internal.core.dialect.Dialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-26
 */
public abstract class AbstractParser implements Parser {
    private static Logger logger = LoggerFactory.getLogger(AbstractParser.class);

    protected final Dialect dialect;
    protected final Set<Class<?>> parserClassSet = new HashSet<>();

    public AbstractParser(Dialect dialect) {
        this.dialect = dialect;
    }

    @Override
    public final void parse(JavaBean bean) {
        if (parserClassSet.contains(bean.getTargetClass())) {
            logger.warn("{} has bean parsed", bean.getTargetClass().getName());
            return;
        }
        this.parseInternal(bean);
        parserClassSet.add(bean.getTargetClass());
    }

    protected abstract void parseInternal(JavaBean bean);


    protected String getColumnName(BeanField field) {
        if (field.getGetterMethod() == null) {
            throw new JdbcException("Not found Getter Method: " + field.getField().getName());
        }
        if (field.getSetterMethod() == null) {
            throw new JdbcException("Not found Setter Method: " + field.getField().getName());
        }

        Column annotation = field.getDeclaredAnnotation(Column.class);
        if (annotation != null) {
            return annotation.name();
        }
        return field.getColumnName();
    }

    protected void setFieldConfig(BeanField field, DefaultFieldConfig config) {
        String columnName = this.getColumnName(field);
        String escapeColumnName = dialect.escape(columnName);
        config.setColumnName(columnName);
        config.setEscapeColumnName(escapeColumnName);
        config.setBeanField(field);
    }

}
