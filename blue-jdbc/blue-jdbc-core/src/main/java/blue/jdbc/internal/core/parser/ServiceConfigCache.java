package blue.jdbc.internal.core.parser;

import blue.jdbc.core.parser.ServiceConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-21
 */
public class ServiceConfigCache {
	private final Map<String, ServiceConfig> configMap = new HashMap<>();
	private final Map<Class<?>, ServiceConfig> clazzConfigMap = new HashMap<>();

	public ServiceConfigCache() {
	}

	public void put(ServiceConfig config) {
		configMap.put(config.getName(), config);
		clazzConfigMap.put(config.getClazz(), config);
	}

	public boolean exist(Class<?> clazz) {
		return clazzConfigMap.containsKey(clazz);
	}

	public ServiceConfig get(String name) {
		return configMap.get(name);
	}

	public ServiceConfig get(Class<?> clazz) {
		return clazzConfigMap.get(clazz);
	}

	public Map<Class<?>, ServiceConfig> all() {
		return Map.copyOf(clazzConfigMap);
	}

}
