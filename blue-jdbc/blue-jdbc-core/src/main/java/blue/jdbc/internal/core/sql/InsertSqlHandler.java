package blue.jdbc.internal.core.sql;

import blue.base.core.util.StringUtil;
import blue.jdbc.core.parser.SqlItem;
import blue.jdbc.internal.core.parser.DefaultSqlItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-16
 */
public class InsertSqlHandler implements SqlHandler {
	public InsertSqlHandler() {
	}

    @Override
    public SqlItem sql(SqlParam param) {
        var config = param.getEntityConfig();
        var map = param.getMap();
		this.checkMap(map);

		var idMap = config.getIdMap();
		var columnMap = config.getColumnMap();
		var version = config.getVersionConfig();
	    List<String> columnList = new ArrayList<>();
		List<String> fieldList = new ArrayList<>();
		for (var entry : map.entrySet()) {
			var column = this.getColumnString(entry.getKey(), idMap, columnMap, version);
			columnList.add(column);
			fieldList.add(entry.getKey());
		}
		var sql = String.format(INSERT_TPL, config.getEscapeTableName(),
				StringUtil.join(columnList, SEPARATOR),
				StringUtil.repeat(PLACEHOLDER, columnList.size(), SEPARATOR));
        return new DefaultSqlItem(sql, fieldList);
    }
}
