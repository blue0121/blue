package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.JavaBean;
import blue.jdbc.core.annotation.Dao;
import blue.jdbc.core.exception.JdbcException;
import blue.jdbc.core.parser.DaoConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-21
 */
public class DaoConfigParser implements Parser {
	private static Logger logger = LoggerFactory.getLogger(DaoConfigParser.class);

	private final DaoConfigCache cache;

	public DaoConfigParser(DaoConfigCache cache) {
		this.cache = cache;
	}

	@Override
	public void parse(JavaBean bean) {
		if (cache.exist(bean.getTargetClass())) {
			logger.warn("{} has bean parsed", bean.getTargetClass().getName());
			return;
		}

		var field = bean.getField(DaoConfig.FIELD_JDBC);
		if (field == null) {
			throw new JdbcException("@Dao not exists field: " + DaoConfig.FIELD_JDBC);
		}

		Object object = bean.newInstanceQuietly();
		String name = this.getName(bean);
		DefaultDaoConfig config = new DefaultDaoConfig();
		config.setName(name);
		config.setJavaBean(bean);
		config.setObject(object);
		config.check();
		cache.put(config);
		logger.info("Found @Dao [{}]: {}", name, config.getClazz().getName());
	}

	private String getName(JavaBean bean) {
		Dao anno = bean.getAnnotation(Dao.class);
		String name = anno.name();
		if (!name.isEmpty()) {
			return name;
		}

		String clazzName = bean.getTargetClass().getSimpleName();
		char first = clazzName.charAt(0);
		return Character.toLowerCase(first) + clazzName.substring(1);
	}
}
