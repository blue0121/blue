package blue.jdbc.internal.core.util;

import blue.base.core.id.IdGenerator;
import blue.base.core.id.SnowflakeId;
import blue.jdbc.core.annotation.GeneratorType;
import blue.jdbc.core.exception.JdbcException;
import blue.jdbc.core.parser.EntityConfig;
import blue.jdbc.core.parser.IdConfig;
import blue.jdbc.core.parser.IdType;
import blue.jdbc.internal.core.executor.KeyHolder;

import java.util.Collection;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-29
 */
public class IdUtil {
	private IdUtil() {
	}

	/**
	 * 产生主键
	 *
	 * @return
	 */
	public static void generateId(Map<String, Object> map, Map<String, IdConfig> idMap) {
		generateId(map, idMap, null);
	}

	public static void generateId(Map<String, Object> map, Map<String, IdConfig> idMap, SnowflakeId snowflakeId) {
		for (var entry : idMap.entrySet()) {
			var id = entry.getValue();
			if (id.getGeneratorType() == GeneratorType.AUTO) {
				if (id.getIdType() == IdType.STRING) {
					map.put(id.getFieldName(), IdGenerator.uuid32bit());
				}
			} else if (id.getGeneratorType() == GeneratorType.UUID) {
				if (id.getIdType() == IdType.STRING) {
					map.put(id.getFieldName(), IdGenerator.uuid32bit());
				} else if (id.getIdType() == IdType.LONG) {
					if (snowflakeId == null) {
						map.put(id.getFieldName(), IdGenerator.id());
					} else {
						map.put(id.getFieldName(), snowflakeId.nextId());
					}
				}
			}
		}
	}

	public static void setId(KeyHolder holder, Object target, EntityConfig config) {
		if (holder == null || target == null || config == null) {
			return;
		}
		var id = config.getIdConfig();
		if (id == null) {
			return;
		}
		if (target instanceof Collection<?> list) {
			int i = 0;
			var numList = holder.getKeyList();
			if (numList.isEmpty()) {
				return;
			}
			for (var t : list) {
				setId(numList.get(i++), t, id);
			}
		} else {
			setId(holder.getKey(), target, id);
		}
	}

	private static void setId(Number num, Object target, IdConfig id) {
		if (num == null) {
			return;
		}
		var field = id.getBeanField();
		if (id.getIdType() == IdType.INT) {
			field.setFieldValue(target, num.intValue());
		} else if (id.getIdType() == IdType.LONG) {
			field.setFieldValue(target, num.longValue());
		}
	}

	public static IdConfig checkSingleId(EntityConfig entityConfig) {
		var id = entityConfig.getIdConfig();
		if (id == null) {
			throw new JdbcException(entityConfig.getClazz().getName() + " @Id主键个数大于1，不支持该操作");
		}

		return id;
	}

}
