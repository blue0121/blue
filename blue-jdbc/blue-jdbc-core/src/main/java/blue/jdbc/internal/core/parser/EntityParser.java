package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.BeanField;
import blue.base.core.reflect.JavaBean;
import blue.jdbc.core.annotation.Entity;
import blue.jdbc.core.annotation.GeneratorType;
import blue.jdbc.core.annotation.Id;
import blue.jdbc.core.annotation.Must;
import blue.jdbc.core.annotation.Transient;
import blue.jdbc.core.annotation.Version;
import blue.jdbc.core.exception.JdbcException;
import blue.jdbc.core.parser.ColumnConfig;
import blue.jdbc.core.parser.IdConfig;
import blue.jdbc.core.parser.IdType;
import blue.jdbc.core.parser.VersionConfig;
import blue.jdbc.internal.core.dialect.Dialect;
import blue.jdbc.internal.core.sql.DefaultSqlGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-26
 */
public class EntityParser extends AbstractParser {
	private static Logger logger = LoggerFactory.getLogger(EntityParser.class);
	private static final Set<Class<?>> INT_TYPE_SET = Set.of(int.class, long.class, Integer.class, Long.class);

	private final EntityConfigCache cache;

	public EntityParser(Dialect dialect, EntityConfigCache cache) {
		super(dialect);
		this.cache = cache;
	}

	@Override
	protected void parseInternal(JavaBean bean) {
		Entity annotationEntity = bean.getDeclaredAnnotation(Entity.class);
		String tableName = annotationEntity.table();
		if (tableName.isEmpty()) {
			tableName = bean.getColumnName();
		}
		String escapeTableName = dialect.escape(tableName);

		logger.info("Mapped entity class: {} <==> {}", bean.getTargetClass().getName(), tableName);
		DefaultEntityConfig config = new DefaultEntityConfig();
		config.setTableName(tableName);
		config.setEscapeTableName(escapeTableName);
		config.setJavaBean(bean);
		Map<String, IdConfig> idMap = new LinkedHashMap<>();
		Map<String, ColumnConfig> columnMap = new LinkedHashMap<>();
		Map<String, ColumnConfig> extraMap = new LinkedHashMap<>();

		var fieldMap = bean.getAllFields();
		for (var entry : fieldMap.entrySet()) {
			BeanField field = entry.getValue();
			Id annotationId = field.getDeclaredAnnotation(Id.class);
			if (annotationId != null) {
				IdConfig id = this.parseFieldId(field, annotationId);
				idMap.put(id.getFieldName(), id);
				logger.debug("Id: {} <==> {}", id.getFieldName(), id.getColumnName());
				continue;
			}
			Version annotationVersion = field.getDeclaredAnnotation(Version.class);
			if (annotationVersion != null) {
				if (config.getVersionConfig() != null) {
					throw new JdbcException("Only one @Version: " + bean.getTargetClass().getName());
				}
				VersionConfig version = this.parseFieldVersion(field, annotationVersion);
				config.setVersionConfig(version);
				logger.debug("Version: {} <==> {}", version.getFieldName(), version.getColumnName());
				continue;
			}
			Transient annotationTransient = field.getDeclaredAnnotation(Transient.class);
			ColumnConfig column = this.parseFieldColumn(entry.getValue());
			if (annotationTransient != null) {
				extraMap.put(column.getFieldName(), column);
				logger.debug("Extra: {} <==> {}", column.getFieldName(), column.getColumnName());
			} else {
				columnMap.put(column.getFieldName(), column);
				logger.debug("Column: {} <==> {}", column.getFieldName(), column.getColumnName());
			}
		}

		config.setIdMap(idMap);
		config.setColumnMap(columnMap);
		config.setExtraMap(extraMap);

		var generator = new DefaultSqlGenerator(config);
		generator.generate();

		config.check();
		cache.put(config);
	}

	private IdConfig parseFieldId(BeanField field, Id annotation) {
		DefaultIdConfig config = new DefaultIdConfig();

		Class<?> fieldType = field.getField().getType();
		IdType idType = switch (fieldType.getSimpleName()) {
			case "String" -> IdType.STRING;
			case "int", "Integer" -> IdType.INT;
			case "long", "Long" -> IdType.LONG;
			default -> throw new JdbcException("Unsupported type: " + fieldType.getName());
		};
		config.setIdType(idType);

		GeneratorType generatorType = annotation.generator();
		if (generatorType == GeneratorType.AUTO) {
			generatorType = switch (idType) {
				case STRING -> GeneratorType.UUID;
				default -> GeneratorType.INCREMENT;
			};
		}
		config.setGeneratorType(generatorType);

		this.setFieldConfig(field, config);
		return config;
	}

	private VersionConfig parseFieldVersion(BeanField field, Version annotation) {
		var type = field.getField().getType();
		if (!INT_TYPE_SET.contains(type)) {
			throw new JdbcException("@Version type must be: " + INT_TYPE_SET);
		}

		DefaultVersionConfig config = new DefaultVersionConfig();
		config.setForce(annotation.force());
		config.setDefaultValue(annotation.defaultValue());
		this.setFieldConfig(field, config);
		return config;
	}

	private ColumnConfig parseFieldColumn(BeanField field) {
		DefaultColumnConfig config = new DefaultColumnConfig();
		this.setFieldConfig(field, config);

		Must annotation = field.getDeclaredAnnotation(Must.class);
		if (annotation != null) {
			config.setMustInsert(annotation.insert());
			config.setMustInsert(annotation.update());
		}

		return config;
	}

}
