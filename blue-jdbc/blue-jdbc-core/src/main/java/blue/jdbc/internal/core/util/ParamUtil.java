package blue.jdbc.internal.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-02
 */
public class ParamUtil {
	private ParamUtil() {
	}

	public static List<Object> toParamList(Map<String, Object> map, List<String> nameList, boolean dynamic) {
		List<Object> valueList = new ArrayList<>();
		for (var name : nameList) {
			var value = map.get(name);
			if (dynamic && ObjectUtil.isEmpty(value)) {
				continue;
			}
			valueList.add(value);
		}
		return valueList;
	}

}
