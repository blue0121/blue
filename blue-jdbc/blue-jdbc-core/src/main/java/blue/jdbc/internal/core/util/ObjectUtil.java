package blue.jdbc.internal.core.util;

import blue.base.core.dict.DictParser;
import blue.base.core.dict.Dictionary;
import blue.jdbc.core.parser.ColumnConfig;
import blue.jdbc.core.parser.EntityConfig;
import blue.jdbc.core.parser.IdConfig;
import blue.jdbc.core.parser.VersionConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-02
 */
public class ObjectUtil {
	private static final String EMPTY_STR = "";

	private ObjectUtil() {
	}

	public static Map<String, Object> generateMap(Map<String, Object> param, EntityConfig config) {
		Map<String, Object> map = new HashMap<>();
		if (param != null && !param.isEmpty()) {
			map.putAll(param);
		}
		IdUtil.generateId(map, config.getIdMap());
		VersionUtil.generate(map, config.getVersionConfig());
		return map;
	}

	public static Map<String, Object> toMap(Object target, EntityConfig config, boolean dynamic) {
		Map<String, Object> map = new HashMap<>();
		toIdMap(target, map, config.getIdMap(), dynamic);
		toVersionMap(target, map, config.getVersionConfig());
		toColumnMap(target, map, config.getColumnMap(), dynamic);
		return map;
	}

	private static void toIdMap(Object target, Map<String, Object> map, Map<String, IdConfig> idMap, boolean dynamic) {
		for (var entry : idMap.entrySet()) {
			var id = entry.getValue();
			Object value = id.getBeanField().getFieldValue(target);
			value = convert(value);
			if (!dynamic || value != null) {
				map.put(id.getFieldName(), value);
			}
		}
	}

	private static void toVersionMap(Object target, Map<String, Object> map, VersionConfig config) {
		if (config == null) {
			return;
		}

		Object value = config.getBeanField().getFieldValue(target);
		if (value == null) {
			value = config.getDefaultValue();
		}
		map.put(config.getFieldName(), value);
	}

	private static void toColumnMap(Object target, Map<String, Object> map, Map<String, ColumnConfig> columnMap, boolean dynamic) {
		for (var entry : columnMap.entrySet()) {
			var column = entry.getValue();
			Object value = column.getBeanField().getFieldValue(target);
			value = convert(value);
			if (!dynamic || value != null) {
				map.put(column.getFieldName(), value);
			}
		}
	}

	private static Object convert(Object val) {
		if (isEmpty(val)) {
			return null;
		}

		if (val instanceof Dictionary) {
			Dictionary dict = (Dictionary) val;
			return DictParser.getInstance().getFromObject(dict);
		}

		return val;
	}

	public static boolean isEmpty(Object object) {
		return object == null || EMPTY_STR.equals(object);
	}

}
