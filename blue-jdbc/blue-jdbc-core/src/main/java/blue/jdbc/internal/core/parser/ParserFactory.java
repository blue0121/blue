package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.JavaBean;
import blue.base.core.reflect.JavaBeanCache;
import blue.jdbc.core.annotation.Dao;
import blue.jdbc.core.annotation.Entity;
import blue.jdbc.core.annotation.Mapper;
import blue.jdbc.core.annotation.Service;
import blue.jdbc.internal.core.dialect.Dialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-26
 */
public class ParserFactory {
	private static Logger logger = LoggerFactory.getLogger(ParserFactory.class);
	private static Set<Class<? extends Annotation>> annotationSet = Set.of(
			Entity.class, Mapper.class, Dao.class, Service.class);

	private final Dialect dialect;
	private final Map<Class<? extends Annotation>, Parser> parserMap = new HashMap<>();
	private final EntityConfigCache entityConfigCache = new EntityConfigCache();
	private final MapperConfigCache mapperConfigCache = new MapperConfigCache();
	private final DaoConfigCache daoConfigCache = new DaoConfigCache();
	private final ServiceConfigCache serviceConfigCache = new ServiceConfigCache();

	public ParserFactory(Dialect dialect) {
		this.dialect = dialect;
		parserMap.put(Entity.class, new EntityParser(dialect, entityConfigCache));
		parserMap.put(Mapper.class, new MapperParser(dialect, mapperConfigCache));
		parserMap.put(Dao.class, new DaoConfigParser(daoConfigCache));
		parserMap.put(Service.class, new ServiceConfigParser(serviceConfigCache));
	}

	public void parse(Class<?> clazz) {
		JavaBean javaBean = JavaBeanCache.getJavaBean(clazz);
		var annotationList = javaBean.getAnnotations();
		for (var annotation : annotationList) {
			if (!annotationSet.contains(annotation.annotationType()))
				continue;

			Parser parser = parserMap.get(annotation.annotationType());
			if (parser != null) {
				logger.debug("Found {} Handler: {}", annotation.annotationType().getSimpleName(), parser);
				parser.parse(javaBean);
				break;
			}
		}
	}

	public Dialect getDialect() {
		return dialect;
	}

	public EntityConfigCache getEntityConfigCache() {
		return entityConfigCache;
	}

	public MapperConfigCache getMapperConfigCache() {
		return mapperConfigCache;
	}

	public DaoConfigCache getDaoConfigCache() {
		return daoConfigCache;
	}

	public ServiceConfigCache getServiceConfigCache() {
		return serviceConfigCache;
	}
}
