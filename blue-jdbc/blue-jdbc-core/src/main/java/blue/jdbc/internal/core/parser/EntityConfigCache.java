package blue.jdbc.internal.core.parser;

import blue.base.core.util.AssertUtil;
import blue.jdbc.core.exception.JdbcException;
import blue.jdbc.core.parser.EntityConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public class EntityConfigCache {
	private Map<Class<?>, EntityConfig> entityCache = new HashMap<>();

	public EntityConfigCache() {
	}

	public void put(EntityConfig config) {
		AssertUtil.notNull(config, "EntityConfig");
		entityCache.put(config.getClazz(), config);
	}

	public boolean exist(Class<?> clazz) {
		AssertUtil.notNull(clazz, "Class");
		return entityCache.containsKey(clazz);
	}

	public EntityConfig get(Class<?> clazz) {
		if (!this.exist(clazz)) {
			throw new JdbcException("Not found @Entity at " + clazz.getName());
		}
		return entityCache.get(clazz);
	}

	public Map<Class<?>, EntityConfig> all() {
		return Map.copyOf(entityCache);
	}

}
