package blue.jdbc.internal.core.sql;

/**
 * @author Jin Zheng
 * @since 2019-11-24
 */
public enum SqlType {

	/**
	 * 动态 insert SQL语句
	 */
	INSERT,

	/**
	 * 动态 update SQL语句
	 */
	UPDATE,

	/**
	 * 动态 delete SQL 语句
	 */
	DELETE,

	/**
	 * 动态增长SQL语句
	 */
	INC,

	/**
	 * 判断是否存在 SQL 语句
	 */
	EXIST,

	/**
	 * 动态 select ... limit 1 单字段 SQL 语句
	 */
	GET_FIELD,

	/**
	 * 动态 select ... limit SQL语句
	 */
	GET,

	/**
	 * 动态 select count(*) SQL 语句
	 */
	COUNT,

}
