package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.JavaBean;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-26
 */
public interface Parser {
	/**
	 * parse class with @Entity/@Mapper
	 */
	void parse(JavaBean bean);
}
