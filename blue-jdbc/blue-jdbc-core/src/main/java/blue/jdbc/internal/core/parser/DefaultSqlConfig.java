package blue.jdbc.internal.core.parser;

import blue.base.core.util.AssertUtil;
import blue.jdbc.core.parser.SqlConfig;
import blue.jdbc.core.parser.SqlItem;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public class DefaultSqlConfig implements SqlConfig {
	private SqlItem selectById;
	private SqlItem selectByIdList;
	private SqlItem insert;
	private SqlItem updateById;
	private SqlItem updateByIdAndVersion;
	private SqlItem deleteById;
	private SqlItem deleteByIdList;

	public DefaultSqlConfig() {
	}

	@Override
	public void check() {
		AssertUtil.notNull(selectById, "SelectById SQL");
		selectById.check();
		AssertUtil.notNull(selectByIdList, "SelectByIdList SQL");
		selectByIdList.check();
		AssertUtil.notNull(insert, "Insert SQL");
		insert.check();
		AssertUtil.notNull(updateById, "UpdateById SQL");
		updateById.check();
		AssertUtil.notNull(deleteById, "DeleteById SQL");
		deleteById.check();
		AssertUtil.notNull(deleteByIdList, "DeleteByIdList SQL");
		deleteByIdList.check();
	}

	@Override
	public SqlItem getSelectById() {
		return selectById;
	}

	@Override
	public SqlItem getSelectByIdList() {
		return selectByIdList;
	}

	@Override
	public SqlItem getInsert() {
		return insert;
	}

	@Override
	public SqlItem getUpdateById() {
		return updateById;
	}

	@Override
	public SqlItem getUpdateByIdAndVersion() {
		return updateByIdAndVersion;
	}

	@Override
	public SqlItem getDeleteById() {
		return deleteById;
	}

	@Override
	public SqlItem getDeleteByIdList() {
		return deleteByIdList;
	}

	public void setSelectById(SqlItem selectById) {
		this.selectById = selectById;
	}

	public void setSelectByIdList(SqlItem selectByIdList) {
		this.selectByIdList = selectByIdList;
	}

	public void setInsert(SqlItem insert) {
		this.insert = insert;
	}

	public void setUpdateById(SqlItem updateById) {
		this.updateById = updateById;
	}

	public void setUpdateByIdAndVersion(SqlItem updateByIdAndVersion) {
		this.updateByIdAndVersion = updateByIdAndVersion;
	}

	public void setDeleteById(SqlItem deleteById) {
		this.deleteById = deleteById;
	}

	public void setDeleteByIdList(SqlItem deleteByIdList) {
		this.deleteByIdList = deleteByIdList;
	}
}
