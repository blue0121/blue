package blue.jdbc.internal.core.engine;

import blue.jdbc.core.engine.Jdbc;
import blue.jdbc.core.engine.JdbcOperation;
import blue.jdbc.internal.core.executor.DataSourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-02
 */
public class DefaultJdbc implements Jdbc, AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(DefaultJdbc.class);

    private final DataSourceFactory dataSourceFactory;
	private final JdbcOperation jdbcOperation;
	private final ComponentFactory componentFactory;

	public DefaultJdbc(DefaultJdbcBuilder builder) {
		this.dataSourceFactory = builder.getDataSourceFactory();
		this.jdbcOperation = builder.getJdbcOperation();
		this.componentFactory = builder.getComponentFactory();
	}

	@Override
    public JdbcOperation getJdbcOperation() {
        return jdbcOperation;
    }

	@Override
	public <T> T getDao(Class<T> clazz) {
		return componentFactory.getDao(clazz);
	}

	@Override
    public void close() {
		this.dataSourceFactory.close();
    }
}
