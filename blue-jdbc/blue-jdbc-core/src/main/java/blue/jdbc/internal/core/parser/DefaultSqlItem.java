package blue.jdbc.internal.core.parser;

import blue.base.core.util.AssertUtil;
import blue.jdbc.core.parser.SqlItem;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-29
 */
public class DefaultSqlItem implements SqlItem {
	private String sql;
	private List<String> paramNameList;

	public DefaultSqlItem() {
	}

	public DefaultSqlItem(String sql) {
		this(sql, null);
	}

	public DefaultSqlItem(String sql, List<String> paramNameList) {
		this.sql = sql;
		this.setParamNameList(paramNameList);
	}

	@Override
	public String toString() {
		return String.format("SQL: %s, params: %s", sql, paramNameList);
	}

	@Override
	public void check() {
		AssertUtil.notEmpty(sql, "SQL");
		AssertUtil.notNull(paramNameList, "ParameterNameList");
	}

	@Override
	public String getSql() {
		return sql;
	}

	@Override
	public List<String> getParamNameList() {
		return paramNameList;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public void setParamNameList(List<String> paramNameList) {
		if (paramNameList == null || paramNameList.isEmpty()) {
			this.paramNameList = List.of();
		} else {
			this.paramNameList = List.copyOf(paramNameList);
		}
	}
}
