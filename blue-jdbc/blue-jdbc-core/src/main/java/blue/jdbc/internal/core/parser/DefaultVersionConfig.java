package blue.jdbc.internal.core.parser;

import blue.jdbc.core.parser.VersionConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public class DefaultVersionConfig extends DefaultFieldConfig implements VersionConfig {
	private boolean force;
	private int defaultValue;

	public DefaultVersionConfig() {
	}

	@Override
	public boolean isForce() {
		return force;
	}

	@Override
	public int getDefaultValue() {
		return defaultValue;
	}

	public void setForce(boolean force) {
		this.force = force;
	}

	public void setDefaultValue(int defaultValue) {
		this.defaultValue = defaultValue;
	}
}
