package blue.jdbc.internal.core.executor;

import blue.base.core.util.AssertUtil;
import blue.jdbc.core.options.DataSourceOptions;
import com.alibaba.druid.pool.DruidDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-02
 */
public class DataSourceFactory implements AutoCloseable {
    private static Logger logger = LoggerFactory.getLogger(DataSourceFactory.class);

	private final DataSourceOptions options;
    private final DruidDataSource dataSource;

	public DataSourceFactory(DataSourceOptions options) {
		AssertUtil.notNull(options, "DataSourceOptions");
		this.options = options;
		this.dataSource = this.initDataSource();
	}

	private DruidDataSource initDataSource() {
		var dataSource = new DruidDataSource();
		dataSource.setUrl(options.getUrl());
		dataSource.setUsername(options.getUsername());
		dataSource.setPassword(options.getPassword());

		dataSource.setInitialSize(options.getInitialSize());
		dataSource.setMinIdle(options.getMinIdle());
		dataSource.setMaxActive(options.getMaxActive());
		dataSource.setMaxWait(options.getMaxWait());

		dataSource.setTimeBetweenEvictionRunsMillis(options.getTimeBetweenEvictionRunsMillis());
		dataSource.setMinEvictableIdleTimeMillis(options.getMinEvictableIdleTimeMillis());
		dataSource.setMaxEvictableIdleTimeMillis(options.getMaxEvictableIdleTimeMillis());

		dataSource.setValidationQuery(options.getValidationQuery());
		dataSource.setTestWhileIdle(options.isTestWhileIdle());
		dataSource.setTestOnBorrow(options.isTestOnBorrow());
		dataSource.setTestOnReturn(options.isTestOnReturn());
		dataSource.setKeepAlive(options.isKeepAlive());
		dataSource.setPoolPreparedStatements(options.isPoolPreparedStatements());
		dataSource.setAsyncInit(options.isAsyncInit());

		try {
			dataSource.setFilters(options.getFilters());
			dataSource.init();
		} catch (SQLException e) {
			logger.error("Error, ", e);
		}
		logger.info("DruidDataSource initialize, url: {}", options.getUrl());
		return dataSource;
	}

	public DataSource getDateSource() {
		return dataSource;
	}

	@Override
	public void close() {
		if (dataSource != null) {
			dataSource.close();
			logger.info("DruidDataSource closed.");
		}
	}
}
