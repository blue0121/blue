package blue.jdbc.internal.core.executor.metadata;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jin Zheng
 * @since 2021-12-18
 */
@Getter
@Setter
@NoArgsConstructor
public class IdMetadata extends ColumnMetadata {
    protected boolean inc = false;
}
