package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.JavaBean;
import blue.base.core.util.AssertUtil;
import blue.jdbc.core.parser.ColumnConfig;
import blue.jdbc.core.parser.MapperConfig;

import java.util.Collections;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public class DefaultMapperConfig implements MapperConfig {
	private Class<?> clazz;
	private JavaBean javaBean;
	private Map<String, ColumnConfig> columnMap;

	public DefaultMapperConfig() {
	}

	@Override
	public void check() {
		AssertUtil.notNull(clazz, "Class");
		AssertUtil.notNull(javaBean, "JavaBean");
		AssertUtil.notNull(columnMap, "ColumnConfigMap");
		for (var entry : columnMap.entrySet()) {
			entry.getValue().check();
		}
	}

	@Override
	public Class<?> getClazz() {
		return clazz;
	}

	@Override
	public JavaBean getJavaBean() {
		return javaBean;
	}

	@Override
	public Map<String, ColumnConfig> getColumnMap() {
		return columnMap;
	}

	public void setJavaBean(JavaBean javaBean) {
		AssertUtil.notNull(javaBean, "JavaBean");
		this.clazz = javaBean.getTargetClass();
		this.javaBean = javaBean;
	}

	public void setColumnMap(Map<String, ColumnConfig> columnMap) {
		if (columnMap == null || columnMap.isEmpty()) {
			this.columnMap = Map.of();
		} else {
			this.columnMap = Collections.unmodifiableMap(columnMap);
		}
	}
}
