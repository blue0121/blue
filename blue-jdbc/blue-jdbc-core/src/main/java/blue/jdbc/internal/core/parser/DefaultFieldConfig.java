package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.BeanField;
import blue.base.core.util.AssertUtil;
import blue.jdbc.core.parser.FieldConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public class DefaultFieldConfig implements FieldConfig {
	protected String fieldName;
	protected String columnName;
	protected String escapeColumnName;
	protected BeanField beanField;

	public DefaultFieldConfig() {
	}

	@Override
	public void check() {
		AssertUtil.notEmpty(fieldName, "FieldName");
		AssertUtil.notEmpty(columnName, "ColumnName");
		AssertUtil.notEmpty(escapeColumnName, "Escape ColumnName");
		AssertUtil.notNull(beanField, "BeanField");
	}

	@Override
	public String getFieldName() {
		return fieldName;
	}

	@Override
	public String getColumnName() {
		return columnName;
	}

	@Override
	public String getEscapeColumnName() {
		return escapeColumnName;
	}

	@Override
	public BeanField getBeanField() {
		return beanField;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public void setEscapeColumnName(String escapeColumnName) {
		this.escapeColumnName = escapeColumnName;
	}

	public void setBeanField(BeanField beanField) {
		AssertUtil.notNull(beanField, "BeanField");
		this.fieldName = beanField.getField().getName();
		this.beanField = beanField;
	}
}
