package blue.jdbc.internal.core.parser;

import blue.jdbc.core.parser.ColumnConfig;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public class DefaultColumnConfig extends DefaultFieldConfig implements ColumnConfig {
	private boolean mustInsert;
	private boolean mustUpdate;

	public DefaultColumnConfig() {
	}

	@Override
	public boolean isMustInsert() {
		return mustInsert;
	}

	@Override
	public boolean isMustUpdate() {
		return mustUpdate;
	}

	public void setMustInsert(boolean mustInsert) {
		this.mustInsert = mustInsert;
	}

	public void setMustUpdate(boolean mustUpdate) {
		this.mustUpdate = mustUpdate;
	}
}
