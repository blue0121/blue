package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.BeanField;
import blue.base.core.reflect.JavaBean;
import blue.base.core.util.AssertUtil;
import blue.jdbc.core.parser.ServiceConfig;
import lombok.Setter;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-22
 */
@Setter
public class DefaultServiceConfig implements ServiceConfig {
	private String name;
	private Class<?> clazz;
	private Object object;
	private JavaBean javaBean;
	private List<BeanField> daoFieldList;

	public DefaultServiceConfig() {
	}

	@Override
	public void check() {
		AssertUtil.notEmpty(name, "DaoName");
		AssertUtil.notNull(object, "DaoObject");
		AssertUtil.notNull(javaBean, "JavaBean");
		AssertUtil.notEmpty(daoFieldList, "DaoFieldList");
		this.clazz = object.getClass();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Class<?> getClazz() {
		return clazz;
	}

	@Override
	public <T> T getObject() {
		return (T) object;
	}

	@Override
	public JavaBean getJavaBean() {
		return javaBean;
	}

	@Override
	public List<BeanField> getDaoFieldList() {
		return daoFieldList;
	}

	public void setDaoFieldList(List<BeanField> daoFieldList) {
		if (daoFieldList == null || daoFieldList.isEmpty()) {
			this.daoFieldList = List.of();
		} else {
			this.daoFieldList = List.copyOf(daoFieldList);
		}
	}
}
