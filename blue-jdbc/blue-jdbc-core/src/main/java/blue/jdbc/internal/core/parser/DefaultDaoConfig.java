package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.JavaBean;
import blue.base.core.util.AssertUtil;
import blue.jdbc.core.parser.DaoConfig;
import lombok.Setter;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-21
 */
@Setter
public class DefaultDaoConfig implements DaoConfig {
    private String name;
    private Class<?> clazz;
    private Object object;
    private JavaBean javaBean;

	public DefaultDaoConfig() {
	}

    @Override
    public void check() {
        AssertUtil.notEmpty(name, "DaoName");
        AssertUtil.notNull(object, "DaoObject");
        AssertUtil.notNull(javaBean, "JavaBean");
        this.clazz = object.getClass();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Class<?> getClazz() {
        return clazz;
    }

    @Override
    public <T> T getObject() {
        return (T) object;
    }

    @Override
    public JavaBean getJavaBean() {
        return javaBean;
    }
}
