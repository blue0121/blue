package blue.jdbc.internal.core.parser;

import blue.jdbc.core.parser.DaoConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-21
 */
public class DaoConfigCache {
	private final Map<String, DaoConfig> configMap = new HashMap<>();
	private final Map<Class<?>, DaoConfig> clazzConfigMap = new HashMap<>();

	public DaoConfigCache() {
	}

	public void put(DaoConfig config) {
		configMap.put(config.getName(), config);
		clazzConfigMap.put(config.getClazz(), config);
	}

	public boolean exist(Class<?> clazz) {
		return clazzConfigMap.containsKey(clazz);
	}

	public DaoConfig get(String name) {
		return configMap.get(name);
	}

	public DaoConfig get(Class<?> clazz) {
		return clazzConfigMap.get(clazz);
	}

	public Map<Class<?>, DaoConfig> all() {
		return Map.copyOf(clazzConfigMap);
	}

}
