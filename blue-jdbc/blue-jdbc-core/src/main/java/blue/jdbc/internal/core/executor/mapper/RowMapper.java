package blue.jdbc.internal.core.executor.mapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-02
 */
public interface RowMapper<T> {

	int ONE = 1;

	/**
	 * get mapper type
	 */
	Class<T> getType();

	/**
	 * ResultSet => Object instance
	 */
	T mapRow(ResultSetMetaData meta, ResultSet rs, int row) throws SQLException;
}
