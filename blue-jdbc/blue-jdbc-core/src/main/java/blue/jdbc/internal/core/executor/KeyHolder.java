package blue.jdbc.internal.core.executor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-08
 */
public interface KeyHolder {

	/**
	 * get number key
	 *
	 * @return
	 */
	Number getKey();

	/**
	 * get number key list
	 *
	 * @return
	 */
	List<Number> getKeyList();

	/**
	 * get number key from ResultSet
	 *
	 * @param rs
	 */
	void mapRow(ResultSet rs) throws SQLException;

}
