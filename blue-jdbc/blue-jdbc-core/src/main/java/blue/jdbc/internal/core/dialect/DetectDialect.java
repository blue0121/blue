package blue.jdbc.internal.core.dialect;

import blue.base.core.util.StringUtil;
import blue.jdbc.core.exception.JdbcException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

/**
 * @author Jin Zheng
 * @since 2020-01-04
 */
public class DetectDialect {
	private static Logger logger = LoggerFactory.getLogger(DetectDialect.class);

	private DetectDialect() {
	}

	public static Dialect dialect(DataSource dataSource) {
		try (Connection conn = dataSource.getConnection()) {
			DatabaseMetaData meta = conn.getMetaData();
			String productName = meta.getDatabaseProductName();
			String productVersion = meta.getDatabaseProductVersion();
			String driverName = meta.getDriverName();
			String driverVersion = meta.getDriverVersion();
			String url = meta.getURL();
			String user = meta.getUserName();
			logger.info("Database: {}[{}], driver: {}[{}], url: {}, user: {}", productName, productVersion,
					driverName, driverVersion, url, user);

			return dialect(productName, productVersion, url);
		} catch (SQLException e) {
			throw new JdbcException(e);
		}
	}

	private static Dialect dialect(String productName, String productVersion, String url) {
		String jdbcType = StringUtil.getJdbcType(url);
		Dialect dialect = switch (jdbcType) {
			case MySQLDialect.PROTOCOL -> new MySQLDialect();
			case PostgreSQLDialect.PROTOCOL -> new PostgreSQLDialect();
			case OracleDialect.PROTOCOL -> new OracleDialect();
			case HyperSQLDialect.PROTOCOL -> new HyperSQLDialect();
			default -> null;
		};

		if (dialect != null) {
			logger.info("Detect dialect: {}", dialect.getClass().getName());
		}

		return dialect;
	}

}
