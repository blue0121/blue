package blue.jdbc.internal.core.sql;

import blue.base.core.util.AssertUtil;
import blue.base.core.util.StringUtil;
import blue.jdbc.core.parser.SqlItem;
import blue.jdbc.internal.core.parser.DefaultSqlItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-15
 */
public class GetFieldSqlHandler implements SqlHandler {
	public GetFieldSqlHandler() {
	}

	@Override
	public SqlItem sql(SqlParam param) {
		var field = param.getField();
		AssertUtil.notEmpty(field, "Field");
		var map = param.getMap();
		var config = param.getEntityConfig();
		this.checkMap(map);

		var columnMap = config.getColumnMap();
		var idMap = config.getIdMap();
		var version = config.getVersionConfig();
		List<String> columnList = new ArrayList<>();
		List<String> fieldList = new ArrayList<>();

		String fieldColumn = this.getColumnString(field, idMap, columnMap, version);
		for (var entry : map.entrySet()) {
			String whereColumn = this.getColumnString(entry.getKey(), idMap, columnMap, version);
			columnList.add(whereColumn + EQUAL_PLACEHOLDER);
			fieldList.add(entry.getKey());
		}
		var sql = String.format(GET_TPL, fieldColumn, config.getEscapeTableName(), StringUtil.join(columnList, AND));
		return new DefaultSqlItem(sql, fieldList);
	}
}
