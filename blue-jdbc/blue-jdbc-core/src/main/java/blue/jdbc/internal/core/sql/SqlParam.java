package blue.jdbc.internal.core.sql;

import blue.jdbc.core.parser.EntityConfig;
import blue.jdbc.core.parser.MapperConfig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 2019-11-24
 */
@Getter
@Setter
@NoArgsConstructor
public class SqlParam {
	private EntityConfig entityConfig;
	private MapperConfig mapperConfig;
	private Class<?> clazz;
	private Object target;
	private String field;
	private Map<String, ?> map;
	private List<String> args;

}
