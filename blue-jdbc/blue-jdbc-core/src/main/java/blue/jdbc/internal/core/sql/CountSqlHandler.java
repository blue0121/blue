package blue.jdbc.internal.core.sql;

import blue.base.core.util.StringUtil;
import blue.jdbc.core.parser.SqlItem;
import blue.jdbc.internal.core.parser.DefaultSqlItem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-14
 */
public class CountSqlHandler implements SqlHandler {
	public CountSqlHandler() {
	}

	@Override
	public SqlItem sql(SqlParam param) {
		var config = param.getEntityConfig();
		var map = param.getMap();
		this.checkMap(map);

		List<String> columnList = new ArrayList<>();
		List<String> fieldList = new ArrayList<>();
		var columnMap = config.getColumnMap();
		var idMap = config.getIdMap();
		for (var entry : map.entrySet()) {
			String whereColumn = this.getColumnString(entry.getKey(), idMap, columnMap, null);
			columnList.add(whereColumn + EQUAL_PLACEHOLDER);
			fieldList.add(entry.getKey());
		}
		var sql = String.format(COUNT_TPL, config.getEscapeTableName(), StringUtil.join(columnList, AND));
		return new DefaultSqlItem(sql, fieldList);
	}
}
