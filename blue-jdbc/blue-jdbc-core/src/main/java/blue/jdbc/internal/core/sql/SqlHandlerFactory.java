package blue.jdbc.internal.core.sql;

import blue.base.core.util.AssertUtil;
import blue.jdbc.core.parser.EntityConfig;
import blue.jdbc.core.parser.SqlItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-15
 */
public class SqlHandlerFactory {
    private static Logger logger = LoggerFactory.getLogger(SqlHandlerFactory.class);

    private final EnumMap<SqlType, SqlHandler> handlerMap = new EnumMap<>(SqlType.class);

    public SqlHandlerFactory() {
        handlerMap.put(SqlType.COUNT, new CountSqlHandler());
        handlerMap.put(SqlType.DELETE, new DeleteSqlHandler());
        handlerMap.put(SqlType.EXIST, new ExistSqlHandler());
        handlerMap.put(SqlType.GET_FIELD, new GetFieldSqlHandler());
        handlerMap.put(SqlType.GET, new GetSqlHandler());
        handlerMap.put(SqlType.INC, new IncSqlHandler());
        handlerMap.put(SqlType.INSERT, new InsertSqlHandler());
        handlerMap.put(SqlType.UPDATE, new UpdateSqlHandler());
    }

    public SqlItem handle(SqlType type, EntityConfig config, Map<String, ?> map) {
        var handler = this.getHandler(type);
        var param = new SqlParam();
        param.setEntityConfig(config);
        param.setMap(map);
        return handler.sql(param);
    }

    public SqlItem handle(SqlType type, EntityConfig config, String field, Map<String, ?> map) {
        var handler = this.getHandler(type);
        var param = new SqlParam();
        param.setEntityConfig(config);
        param.setField(field);
        param.setMap(map);
        return handler.sql(param);
    }

    public SqlItem handle(SqlType type, EntityConfig config, Map<String, ?> map, List<String> args) {
        var handler = this.getHandler(type);
        var param = new SqlParam();
        param.setEntityConfig(config);
        param.setMap(map);
        param.setArgs(args);
        return handler.sql(param);
    }

    private SqlHandler getHandler(SqlType type) {
        AssertUtil.notNull(type, "SqlType");
        var handler = handlerMap.get(type);
        if (handler == null) {
            throw new UnsupportedOperationException("Unknown SqlType: " + type);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Found [{}] SqlHandler: {}", type.name(), handler.getClass().getSimpleName());
        }
        return handler;
    }

}
