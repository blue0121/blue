package blue.jdbc.internal.core.parser;

import blue.base.core.reflect.JavaBean;
import blue.jdbc.core.parser.ColumnConfig;
import blue.jdbc.internal.core.dialect.Dialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-26
 */
public class MapperParser extends AbstractParser {
	private static Logger logger = LoggerFactory.getLogger(MapperParser.class);

	private final MapperConfigCache cache;

	public MapperParser(Dialect dialect, MapperConfigCache cache) {
		super(dialect);
		this.cache = cache;
	}

	@Override
	protected void parseInternal(JavaBean bean) {
		logger.info("Mapped class: {}", bean.getTargetClass().getName());
		DefaultMapperConfig config = new DefaultMapperConfig();
		config.setJavaBean(bean);
		Map<String, ColumnConfig> columnMap = new LinkedHashMap<>();

		var fieldMap = bean.getAllFields();
		for (var entry : fieldMap.entrySet()) {
			DefaultColumnConfig column = new DefaultColumnConfig();
			this.setFieldConfig(entry.getValue(), column);
			columnMap.put(column.getFieldName(), column);
			logger.debug("Column: {} <==> {}", column.getFieldName(), column.getColumnName());
		}

		config.setColumnMap(columnMap);
		config.check();
		cache.put(config);
	}

}
