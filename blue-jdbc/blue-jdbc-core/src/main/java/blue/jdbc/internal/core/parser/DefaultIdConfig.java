package blue.jdbc.internal.core.parser;

import blue.base.core.util.AssertUtil;
import blue.jdbc.core.annotation.GeneratorType;
import blue.jdbc.core.parser.IdConfig;
import blue.jdbc.core.parser.IdType;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public class DefaultIdConfig extends DefaultFieldConfig implements IdConfig {
	private IdType idType;
	private GeneratorType generatorType;

	public DefaultIdConfig() {
	}

	@Override
	public void check() {
		super.check();
		AssertUtil.notNull(idType, "IdType");
		AssertUtil.notNull(generatorType, "GeneratorType");
	}

	@Override
	public IdType getIdType() {
		return idType;
	}

	@Override
	public GeneratorType getGeneratorType() {
		return generatorType;
	}

	public void setIdType(IdType idType) {
		this.idType = idType;
	}

	public void setGeneratorType(GeneratorType generatorType) {
		this.generatorType = generatorType;
	}
}
