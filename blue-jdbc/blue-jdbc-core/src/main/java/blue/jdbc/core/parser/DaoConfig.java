package blue.jdbc.core.parser;

import blue.base.core.reflect.JavaBean;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-21
 */
public interface DaoConfig {
	String FIELD_JDBC = "jdbcOperation";

	/**
	 * check dao
	 */
	void check();

	/**
	 * get dao name
	 */
	String getName();

	/**
	 * get class/type
	 */
	Class<?> getClazz();

	/**
	 * get dao instance
	 */
	<T> T getObject();

	/**
	 * get JavaBean
	 */
	JavaBean getJavaBean();

}
