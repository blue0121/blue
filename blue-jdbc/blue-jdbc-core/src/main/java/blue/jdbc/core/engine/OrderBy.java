package blue.jdbc.core.engine;

/**
 * @author Jin Zheng
 * @since 1.0 2020-06-24
 */
public interface OrderBy {
	/**
	 * add order sql: id desc
	 *
	 * @param sql
	 */
	void add(String sql);

}
