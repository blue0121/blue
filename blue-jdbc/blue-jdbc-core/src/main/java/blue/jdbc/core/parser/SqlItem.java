package blue.jdbc.core.parser;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-29
 */
public interface SqlItem {
	/**
	 * check config
	 */
	void check();

	/**
	 * sql
	 */
	String getSql();

	/**
	 * parameter name sequence in sql placeholder(?)
	 */
	List<String> getParamNameList();

}
