package blue.jdbc.core.parser;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-22
 */
public interface ColumnConfig extends FieldConfig {
	/**
	 * is must insert
	 */
	boolean isMustInsert();

	/**
	 * is must update
	 */
	boolean isMustUpdate();

}
