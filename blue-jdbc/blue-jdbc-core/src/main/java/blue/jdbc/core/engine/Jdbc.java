package blue.jdbc.core.engine;

import blue.jdbc.internal.core.engine.DefaultJdbcBuilder;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-02
 */
public interface Jdbc {

	static JdbcBuilder builder() {
		return new DefaultJdbcBuilder();
	}

	/**
	 * get JdbcOperation
	 */
	JdbcOperation getJdbcOperation();

	/**
	 * get @Dao instance
	 */
	<T> T getDao(Class<T> clazz);

	/**
	 * close resource, such as DataSource
	 */
	void close();

}
