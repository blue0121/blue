package blue.jdbc.core.parser;

import blue.base.core.reflect.BeanField;
import blue.base.core.reflect.JavaBean;

import java.util.List;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-22
 */
public interface ServiceConfig {

	/**
	 * check service
	 */
	void check();

	/**
	 * get service name
	 */
	String getName();

	/**
	 * get class/type
	 */
	Class<?> getClazz();

	/**
	 * get service instance
	 */
	<T> T getObject();

	/**
	 * get JavaBean
	 */
	JavaBean getJavaBean();

	/**
	 * get Dao field list
	 */
	List<BeanField> getDaoFieldList();

}
