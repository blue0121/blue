package blue.jdbc.core.options;

import blue.base.core.util.AssertUtil;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public class DataSourceOptions {
    private String url;
    private String username;
    private String password;

    private int initialSize = 1;
    private int minIdle = 1;
    private int maxActive = 20;
	private int maxWait = 6000;

	private int timeBetweenEvictionRunsMillis = 60000;
	private int minEvictableIdleTimeMillis = 600000;
	private int maxEvictableIdleTimeMillis = 900000;

	private String validationQuery = "select 1";
	private boolean testWhileIdle = true;
	private boolean testOnBorrow = false;
	private boolean testOnReturn = false;
	private boolean keepAlive = true;
	private boolean poolPreparedStatements = false;
	private boolean asyncInit = true;

	private String filters = "stat";

	public DataSourceOptions() {
	}

	public void check() {
		AssertUtil.notEmpty(url, "DataSource url");
	}

	public String getUrl() {
		return url;
	}

	public DataSourceOptions setUrl(String url) {
		this.url = url;
		return this;
	}

	public String getUsername() {
		return username;
	}

	public DataSourceOptions setUsername(String username) {
		this.username = username;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public DataSourceOptions setPassword(String password) {
		this.password = password;
		return this;
	}

	public int getInitialSize() {
		return initialSize;
	}

	public DataSourceOptions setInitialSize(int initialSize) {
		this.initialSize = initialSize;
		return this;
	}

	public int getMinIdle() {
		return minIdle;
	}

	public DataSourceOptions setMinIdle(int minIdle) {
		this.minIdle = minIdle;
		return this;
	}

	public int getMaxActive() {
		return maxActive;
	}

	public DataSourceOptions setMaxActive(int maxActive) {
		this.maxActive = maxActive;
		return this;
	}

	public int getMaxWait() {
		return maxWait;
	}

	public DataSourceOptions setMaxWait(int maxWait) {
		this.maxWait = maxWait;
		return this;
	}

	public int getTimeBetweenEvictionRunsMillis() {
		return timeBetweenEvictionRunsMillis;
	}

	public DataSourceOptions setTimeBetweenEvictionRunsMillis(int timeBetweenEvictionRunsMillis) {
		this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
		return this;
	}

	public int getMinEvictableIdleTimeMillis() {
		return minEvictableIdleTimeMillis;
	}

	public DataSourceOptions setMinEvictableIdleTimeMillis(int minEvictableIdleTimeMillis) {
		this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
		return this;
	}

	public int getMaxEvictableIdleTimeMillis() {
		return maxEvictableIdleTimeMillis;
	}

	public DataSourceOptions setMaxEvictableIdleTimeMillis(int maxEvictableIdleTimeMillis) {
		this.maxEvictableIdleTimeMillis = maxEvictableIdleTimeMillis;
		return this;
	}

	public String getValidationQuery() {
		return validationQuery;
	}

	public DataSourceOptions setValidationQuery(String validationQuery) {
		this.validationQuery = validationQuery;
		return this;
	}

	public boolean isTestWhileIdle() {
		return testWhileIdle;
	}

	public DataSourceOptions setTestWhileIdle(boolean testWhileIdle) {
		this.testWhileIdle = testWhileIdle;
		return this;
	}

	public boolean isTestOnBorrow() {
		return testOnBorrow;
	}

	public DataSourceOptions setTestOnBorrow(boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
		return this;
	}

	public boolean isTestOnReturn() {
		return testOnReturn;
	}

	public DataSourceOptions setTestOnReturn(boolean testOnReturn) {
		this.testOnReturn = testOnReturn;
		return this;
	}

	public boolean isKeepAlive() {
		return keepAlive;
	}

	public DataSourceOptions setKeepAlive(boolean keepAlive) {
		this.keepAlive = keepAlive;
		return this;
	}

	public boolean isPoolPreparedStatements() {
		return poolPreparedStatements;
	}

	public DataSourceOptions setPoolPreparedStatements(boolean poolPreparedStatements) {
		this.poolPreparedStatements = poolPreparedStatements;
		return this;
	}

	public boolean isAsyncInit() {
		return asyncInit;
	}

	public DataSourceOptions setAsyncInit(boolean asyncInit) {
		this.asyncInit = asyncInit;
		return this;
	}

	public String getFilters() {
		return filters;
	}

	public DataSourceOptions setFilters(String filters) {
		this.filters = filters;
		return this;
	}
}
