package blue.jdbc.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Jin Zheng
 * @since 1.0 2021-12-22
 */
@Documented
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {

	/**
	 * Service 类名称，默认第1个字母小写
	 * UserService => userService
	 */
	String name() default "";

	/**
	 * 数据源名称
	 */
	String dataSource() default "";

}
