package blue.jdbc.core.parser;

import blue.base.core.reflect.JavaBean;

import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public interface EntityConfig {

	/**
	 * check config
	 */
	void check();

	/**
	 * get class/type
	 */
	Class<?> getClazz();

	/**
	 * get JavaBean
	 */
	JavaBean getJavaBean();

	/**
	 * get table name
	 */
	String getTableName();

	/**
	 * get table name with escape
	 */
	String getEscapeTableName();

	/**
	 * get single IdConfig
	 */
	IdConfig getIdConfig();

	/**
	 * get IdConfig map
	 *
	 * @return key: fieldName, value: IdConfig
	 */
	Map<String, IdConfig> getIdMap();

	/**
	 * get VersionConfig
	 */
	VersionConfig getVersionConfig();

	/**
	 * get ColumnConfig map
	 *
	 * @return key: fieldName, value: ColumnConfig
	 */
	Map<String, ColumnConfig> getColumnMap();

	/**
	 * get extra ColumnConfig map
	 *
	 * @return key: fieldName, value: ColumnConfig
	 */
	Map<String, ColumnConfig> getExtraMap();

	/**
	 * get SqlConfig
	 */
	SqlConfig getSqlConfig();

}
