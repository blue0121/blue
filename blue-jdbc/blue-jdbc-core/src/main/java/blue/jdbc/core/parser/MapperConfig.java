package blue.jdbc.core.parser;

import blue.base.core.reflect.JavaBean;

import java.util.Map;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public interface MapperConfig {
	/**
	 * check config
	 */
	void check();

	/**
	 * get class/type
	 */
	Class<?> getClazz();

	/**
	 * get JavaBean
	 */
	JavaBean getJavaBean();

	/**
	 * get ColumnConfig map
	 *
	 * @return key: fieldName, value: ColumnConfig
	 */
	Map<String, ColumnConfig> getColumnMap();

}
