package blue.jdbc.core.parser;

import blue.base.core.reflect.BeanField;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public interface FieldConfig {
	/**
	 * check field
	 */
	void check();

	/**
	 * get field name
	 */
	String getFieldName();

	/**
	 * get column name of table
	 */
	String getColumnName();

	/**
	 * get column name with escape
	 */
	String getEscapeColumnName();

	/**
	 * get BeanField of JavaBean
	 */
	BeanField getBeanField();
}
