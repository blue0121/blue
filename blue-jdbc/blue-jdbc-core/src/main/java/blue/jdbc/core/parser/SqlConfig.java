package blue.jdbc.core.parser;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-25
 */
public interface SqlConfig {
	/**
	 * check config
	 */
	void check();

	/**
	 * select * from [table] where id=?
	 */
	SqlItem getSelectById();

	/**
	 * select * from [table] where id in (?,?,?)
	 */
	SqlItem getSelectByIdList();

	/**
	 * insert into [table] (id,x) values (?,?)
	 */
	SqlItem getInsert();

	/**
	 * update [table] set x=? where id=?
	 */
	SqlItem getUpdateById();

	/**
	 * update [table] set x=? where id=? and version=?
	 */
	SqlItem getUpdateByIdAndVersion();

	/**
	 * delete from [table] where id=?
	 */
	SqlItem getDeleteById();

	/**
	 * delete from [table] where id in (?,?,?)
	 */
	SqlItem getDeleteByIdList();

}
