package blue.jdbc.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * XxxDao 类注解
 *
 * @author Jin Zheng
 * @since 2021-12-20
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dao {
	/**
	 * Dao类名称，默认第1个字母小写
	 * UserDao => userDao
	 */
	String name() default "";
}
