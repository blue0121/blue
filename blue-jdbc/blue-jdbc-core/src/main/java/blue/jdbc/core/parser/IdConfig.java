package blue.jdbc.core.parser;

import blue.jdbc.core.annotation.GeneratorType;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-22
 */
public interface IdConfig extends FieldConfig {

	/**
	 * get IdType
	 */
	IdType getIdType();

	/**
	 * get GeneratorType
	 */
	GeneratorType getGeneratorType();

	/**
	 * is database generated
	 * @return
	 */
	default boolean isDbGenerated() {
		var type = this.getGeneratorType();
		if (type == GeneratorType.INCREMENT) {
			return true;
		}
		var idType = this.getIdType();
		var isNum = idType == IdType.INT || idType == IdType.LONG;
		if (type == GeneratorType.AUTO && isNum) {
			return true;
		}
		return false;
	}

}
