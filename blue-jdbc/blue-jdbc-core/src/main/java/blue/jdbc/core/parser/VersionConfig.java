package blue.jdbc.core.parser;

/**
 * @author Jin Zheng
 * @since 1.0 2021-10-22
 */
public interface VersionConfig extends FieldConfig {

	/**
	 * is force
	 */
	boolean isForce();

	/**
	 * default value
	 *
	 * @return
	 */
	int getDefaultValue();

}
