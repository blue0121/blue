package blue.jdbc.core.engine;

import blue.jdbc.core.options.DataSourceOptions;

/**
 * @author Jin Zheng
 * @since 1.0 2021-11-02
 */
public interface JdbcBuilder {

	/**
	 * build Jdbc instance
	 */
	Jdbc build();

	/**
	 * set DataSourceOptions
	 */
	JdbcBuilder setDataSourceOptions(DataSourceOptions options);

	/**
	 * add model, such as @Entity, @Mapper, @Dao
	 */
	JdbcBuilder addClazz(Class<?>...clazzes);

	/**
	 * add scan packages
	 */
	JdbcBuilder addScanPackages(String...pkgs);

}
