module blue.jdbc.core {
	requires java.sql;
	requires java.naming;
	requires java.management;
	requires blue.base.core;
	requires druid;
	requires lombok;

	exports blue.jdbc.core.annotation;
	exports blue.jdbc.core.engine;
	exports blue.jdbc.core.exception;
	exports blue.jdbc.core.options;
	exports blue.jdbc.core.parser;
}